// Gmsh project created on Wed May 26 17:45:18 2021
SetFactory("OpenCASCADE");
//+
Point(1) = {0, 0, 0, 1.0};
//+
Point(2) = {0, 0.01, 0, 1.0};
//+
Point(3) = {0.01, 0.01, 0, 1.0};
//+
Point(4) = {0.01, 0, 0, 1.0};
//+
Point(5) = {0, 0, 0.01, 1.0};
//+
Point(6) = {0.01, 0, 0.01, 1.0};
//+
Point(7) = {0.01, 0.01, 0.01, 1.0};
//+
Point(8) = {0, 0.01, 0.01, 1.0};
//+
Line(1) = {1, 4};
//+
Line(2) = {4, 3};
//+
Line(3) = {3, 2};
//+
Line(4) = {2, 1};
//+
Line(5) = {1, 5};
//+
Line(6) = {5, 6};
//+
Line(7) = {6, 4};
//+
Line(8) = {6, 7};
//+
Line(9) = {7, 3};
//+
Line(10) = {7, 8};
//+
Line(11) = {8, 2};
//+
Line(12) = {8, 5};
//+
Curve Loop(1) = {10, 11, -3, -9};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {4, 5, -12, 11};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {6, 7, -1, 5};
//+
Plane Surface(3) = {3};
//+
Curve Loop(4) = {8, 9, -2, -7};
//+
Plane Surface(4) = {4};
//+
Curve Loop(5) = {2, 3, 4, 1};
//+
Plane Surface(5) = {5};
//+
Curve Loop(6) = {6, 8, 10, 12};
//+
Plane Surface(6) = {6};
//+
Surface Loop(1) = {1, 6, 3, 4, 5, 2};
//+
Volume(1) = {1};
//+
Transfinite Curve {10, 3, 9, 11, 12, 4, 8, 2, 6, 5, 1, 7} = 10 Using Progression 1;

