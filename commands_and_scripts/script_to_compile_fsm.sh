#!/bin/bash
cd $HOME && \
sudo cp -r $HOME/shared/fenics_fsm_for_experimenting_history_transfer/fenics-solid-mechanics/ . && \
cd fenics-solid-mechanics/ && \
sudo python3 setup.py install  && \
sudo cp -r $HOME/fenics-solid-mechanics/fsm/src /usr/local/lib/python3.6/dist-packages/fsm-0.0.2-py3.6.egg/fsm && \
sudo cp -r $HOME/fenics-solid-mechanics/fsm/python/*.cpp /usr/local/lib/python3.6/dist-packages/fsm-0.0.2-py3.6.egg/fsm/python
