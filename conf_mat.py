# ref: recommended thermophysical properties for selected commercial alloys by Kenneth C Mills

# 4420-(69*T/500) ({{0,4420},{1000,4282}})
#rho = 4309         # Density of Ti6Al4V @ 800 deg C in kg/m3 
rho_coeff_a = -69/500
rho_coeff_b = 4420

# thermal parameters
#ts = 1873
#tl = 1923
#tm = (tl+ts)/2.0
#tr = 2*(tl-tm)

# 0.0157 * T + 7 ({{0,7},{1000,22.7}})
#tcd = 17.8          # thermal conductivity of Ti6Al4V @ 800 deg C in W/(m K)
#tdiff = tcd        # thermal conductivity !!!
tcd_coeff_a = 0.0157
tcd_coeff_b = 7
tdiff_coeff_a = 0.0157
tdiff_coeff_b = 7

# 0.207 * T + 546 ({{0,546},{1000,753}})
#cp = 714           # specific heat of solid,  J/(kg*K)
cp_coeff_a = 0.207
cp_coeff_b = 546

convh =  20#20        # conv. heat transfer coefficient, W/K

#lat = ?       # Latent heat of material, J/kg

eps = 0.14375         # thermal emissivity averaged for wavelengths 5-20 micro meter @ 877 degree C
		      # (ref: Infrared normal spectral emissivity of Ti–6Al–4V alloy in the 500–1150 K temperature range)
 		      # (the emissivity changes between 0.15 to 0.17 between 500K to 1200K (277 deg C - 927 deg C) for 15 micro meter wavelength Fig. 6.)





