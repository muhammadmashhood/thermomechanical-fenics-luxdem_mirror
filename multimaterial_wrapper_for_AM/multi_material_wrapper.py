import types
import os

# Import SWIG-generated extension module (DOLFIN C++)
import dolfin.cpp as cpp

# Local imports
from dolfin.fem.form import *
from dolfin import parameters
from dolfin.cpp.mesh import MeshFunctionSizet

from dolfin import compile_cpp_code

import fsm

pwd = os.path.dirname(os.path.abspath(__file__))
with open(pwd + "/multi_material_wrapper.cpp", "r") as f:
    multi_material_wrapper_code = f.read()
multi_material_wrapper = compile_cpp_code(multi_material_wrapper_code, include_dirs=["/usr/local/lib/python3.6/dist-packages/fsm-0.0.2-py3.6.egg/fsm/src"])
#/home/marco/Programmi/Dolphin/src_from_dorsal/fenics-solid-mechanics/src

import ufl

class MultiMaterialWrapper(ufl.Coefficient):

    def __init__(self, *args, **kwargs):
        """Initialize Function."""
        
        if not isinstance(args[0], list):
            raise RuntimeError("Expecting a 'list' of QuadratureFunctions as argument 1, got ", type(args[0]), ".")
        self.list_of_QuadratureFunctions = args[0]
        if not isinstance(args[1], MeshFunctionSizet):
            raise RuntimeError("Expecting a dolfin 'MeshFunction(\"size_t\",...)' 2, got ", type(args[1]), ".")
        self.materials = args[1]
        self._cpp_object = multi_material_wrapper.WrappedQuadratureFunction([i.cpp_object() for i in self.list_of_QuadratureFunctions], self.materials)

        ufl.Coefficient.__init__(self, args[0][0].ufl_function_space(), count=self._cpp_object.id())

        # Set name as given or automatic
        name = kwargs.get("name") or "f_%d" % self.count()
        self._cpp_object.rename(name, "a WrappedQuadratureFunction")

    def name(self):
        return self._cpp_object.name()
    
    def cpp_object(self):
        return self._cpp_object

    def value_rank(self):
        return self._cpp_object.value_rank()

    def value_dimension(self, i):
        return self._cpp_object.value_dimension(i)

    def value_shape(self):
        return self._cpp_object.value_shape


