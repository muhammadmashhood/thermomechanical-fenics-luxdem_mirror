Lx = Ly = Lz = 1 # dimension or length of each direction for AM domain

Nx = Ny = Nz = 12 # number of subdivisions or bounding boxes of domain, remember its size should be bigger than the distance between two consecutive points 
no_of_elements_each_dir = 12 # updated it along with the path of mesh
arg1 = 'input_mesh_files/unit_cube_3d_12_elem.xml'

g_code_file_path = 'g_code_csvs/unit_cube_filler.csv'

layer_thickness = Lx/no_of_elements_each_dir
no_of_layers = int(Lx/layer_thickness)
bounding_box_thickness = Lx/Nx

#G-code point to put manualy 
px = 0.49  
py = 0.34
pz = 0.0

