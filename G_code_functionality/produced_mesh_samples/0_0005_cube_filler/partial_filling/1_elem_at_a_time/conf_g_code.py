Lx = Ly = Lz = 0.5e-3 # dimension or length of each direction for AM domain

Nx = Ny = Nz = 20 # number of subdivisions or bounding boxes of domain, remember its size should be bigger than the distance between two consecutive points 
no_of_elements_each_dir = 20 # updated it along with the path of mesh
arg1 = 'input_mesh_files/small_cube_20_elem_0_0005_box_mesh.xml'

g_code_file_path = 'g_code_csvs/G_code_cube_0_5mm_filler_for_each_elem_mid_point.csv'

layer_thickness = Lx/Nx
#no_of_layers = int(Lx/layer_thickness)
no_of_layers = int(5)
bounding_box_thickness = Lx/Nx

#G-code point to put manualy 
px = 0.49  
py = 0.34
pz = 0.0

