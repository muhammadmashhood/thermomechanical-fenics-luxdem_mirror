#import dolfin as df
from fenics import *
import sys

#mesh #[0,10.]^3 # make some mesh before e.g. unit cube 

arg1 = sys.argv[1]
print("reading mesh from file", arg1)
#mesh = df.Mesh(arg1)
mesh = Mesh(arg1)
    
#mf = df.MeshFunction("size_t",mesh, mesh.topology().dim(),0)
mf = MeshFunction("size_t",mesh, mesh.topology().dim(),0)

from shapely.geometry import Polygon, Point

def funct_Domain(i,j,k,Lx,Ly,Lz,Nx,Ny,Nz):
    poly = Polygon([ [i*Lx/Nx,j*Ly/Ny,k*Lz/Nz],
              [(i+1)*Lx/Nx,j*Ly/Ny,k*Lz/Nz],
              [(i+1)*Lx/Nx,(j+1)*Ly/Ny,k*Lz/Nz],
              [i*Lx/Nx,(j+1)*Ly/Ny,k*Lz/Nz ],
              [i*Lx/Nx,j*Ly/Ny,(k+1)*Lz/Nz],
              [(i+1)*Lx/Nx,j*Ly/Ny,(k+1)*Lz/Nz],
              [(i+1)*Lx/Nx,(j+1)*Ly/Ny,(k+1)*Lz/Nz],
              [i*Lx/Nx,(j+1)*Ly/Ny,(k+1)*Lz/Nz ],
             ])  # making the 8 cornered polygon in each subdivided rectangular region

    '''subdomain = df.AutoSubDomain(lambda x,bndry:  df.between(x[0],i*Lx/Nx,(i+1)*Lx/Nx) and 
                                                               df.between(x[1],j*Ly/Ny,(j+1)*Ly/Ny)  and 
                                                               df.between(x[2],k*Lz/Nz,(k+1)*Lz/Nz))''' # making subdomain in each subdivided rectangular region
    subdomain = AutoSubDomain(lambda x,bndry:  between(x[0], ( i*Lx/Nx, (i+1)*Lx/Nx ) ) and 
                                                               between(x[1], ( j*Ly/Ny, (j+1)*Ly/Ny) )  and 
                                                               between(x[2], ( k*Lz/Nz, (k+1)*Lz/Nz) ) ) # making subdomain in each subdivided rectangular region
    #print(type(subdomain)) #<class 'dolfin.fem.dirichletbc.AutoSubDomain'>                                                         
    return subdomain,poly   # returning both the subdivided region submesh and the polygon of it

Nx=Ny=Nz=5  # number of subdivision required
Lx=Ly=Lz=1 # length of the cube

counter = 1  # serves as the labelling of subdivided region 
polys = []   # list for the all polygons in the subdivided regions
subdomains = []
for k in range(0,Nz):
        for j in range(0,Ny):
            if j%2: #module 
                ilist=list(range(0,Nx))
            else:
                ilist=list(range(0,Nx))
                ilist.reverse()
            for i in ilist: #zig-zag
                print("({0},{1},{2})".format(i,j,k))
                subdomain,poly = funct_Domain(i,j,k,Lx,Ly,Lz,Nx,Ny,Nz)
                subdomain.mark(mf,counter)  # labelling the subdivided region's mesh marker
                counter=counter+1
                #mesh.refine(mf,counter) # it refine.
                polys.append(poly)  # adding the polygon of each region into the collection list
                #subdomains.append(subdomain)

print(len(polys)) # 125 or 1 - 125

# GCode.
'''def activeElement(px,py,pz,polys):
    check=False
    import pdb
    
    for  (item,poly) in enumerate(polys): # Does it really return the mesh function or only the labels for involved regions?
        #pdb.set_trace()
        print (item)
        if poly.contains(Point(px,py,pz)):  # check if the input x Y coordinate from G code is on any polygon of subdivided region? And also which polygon has it? 
            check = True
            print("poly contains the point")
            if item == 0:
                return item, [1]  # if first one has it then return polygon with the label of region
            else:
                return item+1, list(range(1,item)) # if in polygon item more than one then return not only this item but also the labels from start till this polygon region's label so that whole domain is involved to be cut up till this state including prebious domain parts
'''

def activeElement(px,py,pz,polys):
    check=False
    import pdb
    for item in range( len(polys) ): # Does it really return the mesh function or only the labels for involved regions?
        #pdb.set_trace()
        print (item)
        print(polys[item].bounds)
        print( list( polys[item].exterior.coords ) )
        
        #if polys[item].contains(Point(px,py,pz)):
        if polys[item].within(Point(px,py,pz)):
            print("poly contains the point")
    return item, item 
    

    '''if polys[item].contains(Point(px,py,pz)):  # check if the input x Y coordinate from G code is on any polygon of subdivided region? And also which polygon has it? 
            check = True
            print("poly contains the point")
            if item == 0:
                return item+1, [1]  # if first one has it then return polygon with the label of region
            else:
                return item+1, list(range(1,item+1))''' # if in polygon item more than one then return not only this item but also the labels from start till this polygon region's label so that whole domain is involved to be cut up till this state including prebious domain parts


# poly test
i=j=k=0
poly_test = Polygon([ [i*Lx/Nx,j*Ly/Ny,k*Lz/Nz],
              [(i+1)*Lx/Nx,j*Ly/Ny,k*Lz/Nz],
              [(i+1)*Lx/Nx,(j+1)*Ly/Ny,k*Lz/Nz],
              [i*Lx/Nx,(j+1)*Ly/Ny,k*Lz/Nz ],
              [i*Lx/Nx,j*Ly/Ny,(k+1)*Lz/Nz],
              [(i+1)*Lx/Nx,j*Ly/Ny,(k+1)*Lz/Nz],
              [(i+1)*Lx/Nx,(j+1)*Ly/Ny,(k+1)*Lz/Nz],
              [i*Lx/Nx,(j+1)*Ly/Ny,(k+1)*Lz/Nz ]
             ])
# sequentially correct              
'''poly_test = Polygon([ [i*Lx/Nx,j*Ly/Ny,k*Lz/Nz],
              [(i+1)*Lx/Nx,j*Ly/Ny,k*Lz/Nz],
              [(i+1)*Lx/Nx,(j+1)*Ly/Ny,k*Lz/Nz],
              [i*Lx/Nx,(j+1)*Ly/Ny,k*Lz/Nz ],
              [i*Lx/Nx,(j+1)*Ly/Ny,(k+1)*Lz/Nz ],
              [(i+1)*Lx/Nx,(j+1)*Ly/Ny,(k+1)*Lz/Nz],
              [(i+1)*Lx/Nx,j*Ly/Ny,(k+1)*Lz/Nz],
              [i*Lx/Nx,j*Ly/Ny,(k+1)*Lz/Nz]
             ])'''
'''poly_test = Polygon([ [i*Lx/Nx,j*Ly/Ny,k*Lz/Nz],
              [(i+1)*Lx/Nx,j*Ly/Ny,k*Lz/Nz],
              [(i+1)*Lx/Nx,(j+1)*Ly/Ny,k*Lz/Nz],
              [i*Lx/Nx,(j+1)*Ly/Ny,k*Lz/Nz ]
             ])'''             
             
             
'''            
# visualizing the polygon
import geopandas as gpd
import matplotlib.pyplot as plt

p = gpd.GeoSeries(poly_test)
p.plot()
plt.show()
'''
#import pdb
#pdb.set_trace()
#print(poly_test.bounds)
poly_test_list = [poly_test,poly_test,poly_test,poly_test]
print(poly_test_list)
active_domain_label, active_domain_label_list = activeElement(0.1,0.1,0.1,[poly_test])

print(active_domain_label)
print(active_domain_label_list)
#mesh_cut = MeshView.create(mark_cd, 1)


#1. make unit cube
#2. we have labeled with an order (zig-zag)
#3. G-Code=> X,Y,Z => activeElement(px,py,pz,polys) it return u the label of activated maker and before ones

# ur job:  mf => MeshView.
# 


# X, Y  Z=0
# -> -> -> -> ->
# <- <- <- <- <-   
#-> -> -> -> ->
# <- <- <- <- <-   
# -> -> -> -> ->  X=0-5, Y=0

#10 |9 | 8 | 7 | 6
# 1 |2 | 3 | 4 | 5





