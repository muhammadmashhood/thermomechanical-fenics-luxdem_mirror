import dolfin as df
from conf_g_code import * # configuration and input parameters for G-code handling 
import numpy as np

# reading the mesh 
print("reading mesh from file", arg1)
mesh = df.Mesh(arg1)



# storing the mark of mesh as well as the mesh from G-Code points movement
meshinfo = df.HDF5File(mesh.mpi_comm(),'abc/G_code_layersgeom.h5', 'w') 

def funct_Domain(i,j,k,Lx,Ly,Lz,Nx,Ny,Nz,sub_dict): # makes dictionary to keep extreme x,y,z coordinates of each bounding box and makes bounding box subdomain to mark and label it in mesh function mf 
    sub_dict["{},{},{}".format(i,j,k)]=[[i*Lx/Nx,(i+1)*Lx/Nx],
												[j*Ly/Ny,(j+1)*Ly/Ny],
												[k*Lz/Nz,(k+1)*Lz/Nz]]  # contains the bounding box x, y, z coordinate ranges against coordinate indices i, j, k as keys 
	
    subdomain = df.AutoSubDomain( lambda x, bndry:  df.between( x[0], (i*Lx/Nx,(i+1)*Lx/Nx) ) and df.between(x[1], (j*Ly/Ny,(j+1)*Ly/Ny) )  and df.between(x[2],(k*Lz/Nz,(k+1)*Lz/Nz) )   )   # makes the bounding boxes sequentially which detect the G-code point in future to activate elements it contains
    return subdomain

indicisOfActiveCell = []
listofactive = []
counter = 1 # will be used as label of each bounding box subdomain's label 
number_dict = dict() # this dictionary will contain all points indices i,j,k for bounding boxes against their label of counter as a key of dictionary 
sub_dict = dict() # this dictionary will contain each bounding box's x,y,z coordinate ranges
refined = True

# raising error messages when invalid values combination passed
if Nx > no_of_elements_each_dir:
    raise ValueError("The size of bounding box should be greater than size of element. Try with Nx = Ny = Nz equal or discrete multiple times bigger than mesh element size.")

if ( Nx != no_of_elements_each_dir and no_of_elements_each_dir%Nx !=0 ):
    raise ValueError("Use either number of bounding box same as number of mesh element or use the number of bounding box smaller than the number of mesh elements but it should completley divide and give zero remainder when divided by number of mesh elements")

"""
sub_dictr=dict()
for k in range(0,1): # this looping iteration makes the bounding box subdomain and labels it with individual counters
    for j in range(0,Ny):

        for i in range(0,Nx): #not zig-zag
            print("({0},{1},{2})".format(i,j,k))
            subdomain = funct_Domain(i,j,k,Lx,Ly,Lz,Nx,Ny,Nz,sub_dictr)  # returns the bounding box subdomain and creates dictionary for its bounding x, y, z coordinates
            print ( mesh.num_cells() )
            if refined: 
                    mfbool=df.MeshFunction("bool",mesh, mesh.topology().dim() , False)
                    subdomain.mark(mfbool,True) 
                    print ( np.where (mfbool.array()==True) )
                    mesh = df.refine(mesh,mfbool) 
            print ( mesh.num_cells() )
            #import pdb
df.XDMFFile("abc/mesh.xdmf").write(mesh)     
"""
mf = df.MeshFunction("size_t",mesh, mesh.topology().dim(),0) # declaring the mesh function for lablling concerned subdomains of bounding box helping to activate concerned cells
mf_active = df.MeshFunction("size_t",mesh, mesh.topology().dim(),0) # the mesh function which will be used to cut mesh using mesh view 
       
            #pdb.set_trace()
for k in range(0,Nz): # this looping iteration makes the bounding box subdomain and labels it with individual counters
    for j in range(0,Ny):
        '''if (j%2): #module
            ilist=list(range(0,Nx))
        else:
            ilist=list(range(0,Nx))
            ilist.reverse()
        for i in ilist: #zig-zag'''
                
        for i in range(0,Nx): #not zig-zag
            print("({0},{1},{2})".format(i,j,k))
            number_dict["{0}".format(counter)] = "{0},{1},{2}".format(i,j,k) # stores the coordinates indices i, j, k with counter as a key in dictionary
            subdomain = funct_Domain(i,j,k,Lx,Ly,Lz,Nx,Ny,Nz,sub_dict)  # returns the bounding box subdomain and creates dictionary for its bounding x, y, z coordinates
                
                
            
            subdomain.mark(mf,counter) # marking the bounding box domain using mf as mesh function and counter as label
            #mesh.refine(mf,counter) # it refine mesh if wanted
            

            '''
            # saving the bounding box subdomain for sampling or checking
            bounding_box_mesh = df.MeshView.create(mf, counter)
            mxf = df.XDMFFile("bounding_box_mesh/bounding_box_mesh_" + format(counter, '04') + ".xdmf" )
            mxf.write(bounding_box_mesh)
            mxf.close()  '''           

            counter=counter+1 # updating counter for next bounding box subdomain lablling
    
# GCode.
def contains(sub_dict_values, px,py,pz):  # checks if bounding box in question contains the given G-code point 
    extrema_x=sub_dict_values[0]
    extrema_y=sub_dict_values[1]
    extrema_z=sub_dict_values[2]
    if ( (px>= extrema_x[0] and px<=extrema_x[1]) and 
        (py>= extrema_y[0] and py<=extrema_y[1]) and
        (pz>= extrema_z[0] and pz<=extrema_z[1]) ) :
        return True
    else:
        return False

def activeElement(px,py,pz,number_dict,sub_dict):  # this is the function which returns the label of currently last active bounding box as well as the list of all active bounding box for given G-code point in question
    N = len(number_dict)  # given the number of coordinate indices of i,j,k
    
    for item in range(1,N+1):  # iterating from 1 to N+1 because we want exactly the value of item same as the key in number_dict (which starts from 1) 
        key_subdomain_dict = number_dict["{0}".format(item)] # getting the coordinates indices against key value of counter or in other words the bounding box domain label
        sub_dict_values = sub_dict[key_subdomain_dict] # getting the range min and max of x,y,z coordinate values of bounding box or cube in this case 
        
        if ( contains(sub_dict_values, px,py,pz) ): # checking sequentially and passing the given G-code point coordinates along with the bounding box coordinate ranges to check if current G-code point is in the box or not
            
            if (item == 1):
                return item#,[1]
            else:
                return item#,list(range(1,item+1))  # return the list of labels of bounding boxes domains which contain the given G-code point and all zones which were previously activated

# reading the G-code file data of X, Y coordinates and Power P 
read_g_code_data = np.genfromtxt(g_code_file_path, delimiter = ',')

# assigning the G-code data to points coordinates values 
mesh_counter = 0
for h in range( no_of_layers + 1): 
    if layer_thickness > bounding_box_thickness:
        raise ValueError("The layer thickness should not be bigger than the bounding box thickness to keep whole geometry continous or uniform volume.")
        
    pz = h * layer_thickness # from conf_mat file so far 
    
    for i in range(len(read_g_code_data)):
        px = read_g_code_data[i][0]
        py = read_g_code_data[i][1]
            
        print("px_py_pz:    ", px,py,pz)          
        if ( px > Lx or py > Ly or pz > Lz ):
            raise ValueError("Given point is out of bound of allowed domain size of final geometry.")
          
        currently_last_active = activeElement(px,py,pz,number_dict,sub_dict) # two return values, one is the currently active bounding box label second is list of all active bounding box labels 
        listofactive.append(currently_last_active) # appending enables to keep record of the label of all previously activated bounding box labels 
        listofactive=list(set(listofactive )) # avoid repetion. 
        # cutting mesh based on the input point from G code 
        for value in  listofactive : # iterate through all of the active element zones
            for cell in df.SubsetIterator(mf, value):
                temp_cell_indices = cell.index() 
                #print(temp_cell_indices)
                indicisOfActiveCell.append( temp_cell_indices )# gets the indices of the cells which are in the zone or bounding box subdomains to be activated or active
                #print(indicisOfActiveCell)

        mf_active.array()[indicisOfActiveCell] = 1 # labelling all or the subdomain bounding box domains as 1 to facilitate the MeshView mesh cutting step
        mesh_cut = df.MeshView.create(mf_active, 1) # finally cutting the mesh from unit cube 

        # save cut mesh 
        mxf = df.XDMFFile("abc/mesh_files_from_g_code/mesh_after_G_code_point" + format(mesh_counter, '04') + ".xdmf" )
        mxf.write(mesh_cut)
        mxf.close()
        
        meshinfo.write(mesh_cut,"/submesh/{}_part".format(mesh_counter))
        meshinfo.write(mf_active,"/meshfunction/{}_part".format(mesh_counter)) # cell marker    
            
        mesh_counter = mesh_counter + 1

meshinfo.close()

# writing the total number of mesh as number of steps 
import csv
with open('number_of_steps.csv', 'w') as csvfile:
	no_of_steps_writer = csv.writer(csvfile, delimiter=',')
	no_of_steps_writer.writerow( [mesh_counter-1,mesh_counter-1] )
csvfile.close()

#1. make unit cube
#2. we have labeled with an order (zig-zag)
#3. G-Code=> X,Y,Z => activeElement(px,py,pz,polys) it return u the label of activated maker and before ones

# ur job:  mf => MeshView.
# 


# X, Y  Z=0
# -> -> -> -> ->
# <- <- <- <- <-   
#-> -> -> -> ->
# <- <- <- <- <-   
# -> -> -> -> ->  X=0-5, Y=0

#10 |9 | 8 | 7 | 6
# 1 |2 | 3 | 4 | 5
