from fenics import *
import logging
set_log_level(logging.WARNING)

class CurrentDomainInitial(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 0.05)) and between(x[1], (0, 0.05)) and between(x[2], (0, 0.05))

class top_boundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[2], 0.5e-3 - 1e-14, 0.5e-3)

u_D = Constant(400)
u_initial = Constant(800)

'''mesh_final = UnitCubeMesh(100,100,100)
mark_cd = MeshFunction("size_t", mesh_final, mesh_final.topology().dim(), 0)
mark1 = CurrentDomainInitial()
mark1.mark(mark_cd, 1)
mesh = MeshView.create(mark_cd, 1)'''
#mesh = UnitCubeMesh(10,10,10)
mesh = Mesh('input_mesh_files/small_cube_20_elem_0_0005.xml')

V = FunctionSpace(mesh, 'P', 1)

# Define initial value
expr = Expression(' x[2] >= 0.45e-3 ? 800: 400', degree = 1)
#u_n = interpolate(expr, V)
u_n = interpolate(u_initial, V)

v = TestFunction(V)
f = Constant(0)


# List
boundary_conditions = {2: {'Dirichlet':  u_D}}

class BoundaryZ0(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[2], 0, 1e-12)


# Mark boundaries
boundary_markers = MeshFunction("size_t", mesh, mesh.topology().dim()-1, 9999)
by0 = BoundaryZ0(); by0.mark(boundary_markers, 2)

# Collect Dirichlet conditions
bcs = []
for i in boundary_conditions:
    if 'Dirichlet' in boundary_conditions[i]:
        bc_cur = DirichletBC(V, boundary_conditions[i]['Dirichlet'],
                             boundary_markers, i)
        bcs.append(bc_cur)

## Define convection BC

# parameters
conv_coeff = 20
ambient_temp = 400
thermal_cond = 7.2
r = Constant(conv_coeff/thermal_cond)
s = Constant(ambient_temp)

# Mark boundaries for convection
boundary_marker = MeshFunction("size_t", mesh, mesh.topology().dim()-1, 9999)
boundary_class = top_boundary(); boundary_class.mark(boundary_marker, 1)


cp = 560
rho = 4420

u = Function(V)
utrial = TrialFunction(V)
u.interpolate(u_D)
dt = 1e-3 # 0.15e-3 may also give result for mesh size 30 elements 

# surface integral for convection    
ds = Measure('ds', domain = mesh, subdomain_data = boundary_marker)
local_factor = dt*thermal_cond*r*utrial*v*ds(1)
ambient_factor = dt*thermal_cond*r*s*v*ds(1)

F = cp*rho*utrial*v*dx + thermal_cond*dt*dot(grad(utrial), grad(v))*dx - cp*rho*(u_n + dt*f)*v*dx + local_factor - ambient_factor
a, L = lhs(F), rhs(F)



# Time-stepping
t = 0

for n in range(200):
    t += dt
    step = n + 1
    
    solve(a == L, u, bcs)

    if n == 0:
        # Solver parameters
        prm=parameters
        info(prm, True)

    # Update previous solution
    u_n.assign(u)
    minima=u.vector().get_local().min()
    maxima=u.vector().get_local().max()
    print('minima and maxima', minima, maxima, step, t)
    
    xdmff = XDMFFile("thermal_output_mwe/forttemp" + format(step, '04') + ".xdmf")
    xdmff.write(u)
    xdmff.close()

