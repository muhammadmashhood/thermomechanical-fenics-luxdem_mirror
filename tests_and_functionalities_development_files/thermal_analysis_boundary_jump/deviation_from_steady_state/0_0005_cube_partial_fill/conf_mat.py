rho = 4309         # Density of Ti6Al4V @ 800 deg C in kg/m3 

# thermal parameters
#ts = 1873
#tl = 1923
#tm = (tl+ts)/2.0
#tr = 2*(tl-tm)

tcd = 17.8          # thermal conductivity of Ti6Al4V @ 800 deg C in W/(m K)
tdiff = tcd        # thermal conductivity !!!
cp = 714           # specific heat of solid,  J/(kg*K)
convh =  20#20        # conv. heat transfer coefficient, W/K

#lat = ?       # Latent heat of material, J/kg

eps = 0.14375         # thermal emissivity averaged for wavelengths 5-20 micro meter @ 877 degree C
		      # (ref: Infrared normal spectral emissivity of Ti–6Al–4V alloy in the 500–1150 K temperature range)





