num_steps_file_path = 'G_code_functionality/number_of_steps.csv'
sdt = 1#3.0            # time step refinement
dt = 1e-2#1e-3/sdt#1e-1/sdt#1e-2/sdt        # time step size
oplot = 50*sdt       # plot every 30 steps or more

seg = 20             # number of segments for circular voids

sc = 1               # size scale
xhi = 1#4.0132e-2/sc   # 79*0.508 mm, or 790*grid_d
yhi_init = 0.001 + 48/50#27/30#3.4036e-2 # 67*0.508 mm

add_layers = 2       # number of deposited layers

layth_a = 1/50#0.508e-3   # thickness of deposited layers
xnsub_a = 1580//sc   # number of subdivisions in x-direction
ynsub_a = 20         # number of subdivisions in y-direction

xc1 = 0.032491748; yc1 = 0.032; rc1 = .00125
xc2 = 0.020;       yc2 = 0.032; rc2 = .00183333
xc3 = 0.007272078; yc3 = 0.032; rc3 = .0025
xyrs = [(xc1,yc1,rc1), (xc2,yc2,rc2), (xc3,yc3,rc3)]


daf_layers = 0       # ! if zero ignore below

layth_d = 0.508e-3   # thickness of deep affected layer
xnsub_d = 79//sc     # number of subdivisions in x-direction
ynsub_d = 1          # number of subdivisions in y-direction

haf_layers = 1       # ! if zero ignore below

layth_h = 0.508e-3   # thickness of heat affected layer
xnsub_h = 790//sc    # number of subdivisions in x-direction
ynsub_h = 10         # number of subdivisions in y-direction

grid_x = xnsub_a
grid_d = xhi/grid_x
print("xhi, grid_x, grid_d",xhi,grid_x,grid_d)

nx = round(xhi/grid_d)
ny_init = round(yhi_init/grid_d)
xhi = nx*grid_d
yhi_init = ny_init*grid_d

st_per_e_a = round(layth_a/ynsub_a/grid_d)
st_per_e_d = round(layth_d/ynsub_d/grid_d)
st_per_e_h = round(layth_h/ynsub_h/grid_d)

num_xsteps_a = xnsub_a
num_xsteps_d = xnsub_d
num_xsteps_h = xnsub_h

add_xstep = nx//xnsub_a
daf_xstep = nx//xnsub_d
haf_xstep = nx//xnsub_h # 4

aff_layers = [(haf_layers, layth_h, xnsub_h, ynsub_h),
              (daf_layers, layth_d, xnsub_d, ynsub_d)]
