import numpy as np
import matplotlib.pyplot as plt
import math as mth
#x0 = 2.0 / 3.0
#x1 = 1.5

#w = 0.08333

#D = np.linspace(0,2, 500)

#sigmaD = 1.0 / (1.0 + np.exp(-(1 - D) / w))

#x =  x0 + (x1 - x0)*(1 - sigmaD)

#plt.plot(D, x)
#plt.xlabel('D'); plt.ylabel('x')
#plt.savefig('smooth-transitions-constants.png')




a = 0.5
b = 10
x = np.linspace(0,1, 500)
s_x = 400 + (0.5 + 0.5 * np.tanh( (180/mth.pi) * ((x-a)/b)) ) * 400



plt.plot(x, s_x)
plt.xlabel('x'); plt.ylabel('y')
plt.savefig('smooth-transitions-constants_tanh.png')

