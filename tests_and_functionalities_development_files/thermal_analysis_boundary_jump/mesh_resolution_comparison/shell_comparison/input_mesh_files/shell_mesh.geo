// Gmsh project created on Thu May 27 15:49:28 2021
SetFactory("OpenCASCADE");
//+
Box(1) = {0, 0, 0, 1, 1, 1};
//+
Box(2) = {0.0833, 0.0833, 0, 1-2*0.0833, 1-2*0.0833, 1};
//+
BooleanDifference{ Volume{1}; Delete; }{ Volume{2}; Delete; }

//+
Transfinite Curve {31, 33, 32, 26, 36, 29, 28, 34} = 13 Using Progression 1;
//+
Transfinite Curve {21, 16, 23, 20, 14, 24, 18, 22} = 11 Using Progression 1;
