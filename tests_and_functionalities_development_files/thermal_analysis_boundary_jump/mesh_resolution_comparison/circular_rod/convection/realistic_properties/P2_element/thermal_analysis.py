"""
FEniCS tutorial demo program: Heat equation with Dirichlet conditions.
Test problem is chosen to give an exact solution at all nodes of the mesh.

  u'= Laplace(u) + f  in the unit square
  u = u_D             on the boundary
  u = u_0             at t = 0

"""

from __future__ import print_function
from fenics import *
import logging
set_log_level(logging.WARNING)
import numpy as np
#import matplotlib.pyplot as plt
import socket
print("Machine:", socket.gethostname())
print("2D-heat-DED version 1.0")
from G_code_functionality import conf_g_code as gcode_conf
#import G-code_functionality/conf_g_code as gcode_conf

# Build geometry
from conf_geom import *
from conf_proc import *


Td = xhi/velbeam      # time per layer, deposition only
Tl = Td + pause       # time per layer, adding pause

#num_steps = int(Tl/dt+.5)   # number of time steps

# reading the number of steps from file (same as the number of mesh files)
num_steps_data_row = np.genfromtxt(num_steps_file_path, delimiter = ',')
num_steps = int(num_steps_data_row[0])


from conf_mat import *
sigm = 5.67e-08    # SB constant
rad = eps*sigm/tcd # radiation parameter (eps*sig/k)

conv = convh/tcd   # convection parameter (h/k)


yhi = yhi_init

# Define boundary condition
# Dirichlet
u_D = Constant(tinit)
#u_DD = Constant(300)

# Neumann
upx = Constant(-10);upy = Constant(-10)

# Robin
# 1: left+right, 2: top
s1 = Constant(tglov);s2 = Constant(tglov);r1 = Constant(conv);r2 = Constant(conv)

# radiation on Top
s3 = Constant(tglov);r3 = Constant(rad)

# Gaussian
s4 = Expression(' -alpha*pow3d/(2*pi*pow(beamrad,2)) * exp(-0.5 * pow( (x[0]-velbeam*t-lah)/beamrad , 2)) ',
                degree=2, pow3d=pow3d, beamrad=beamrad, velbeam=velbeam, lah=lah, t=0, alpha=alpha)


print("reading mesh from file", gcode_conf.arg1)
mesh_final = Mesh(gcode_conf.arg1)

mxf = XDMFFile("thermal_output/mesh.xdmf")
mxf.write(mesh_final)
mxf.close()

Vf = FunctionSpace(mesh_final, 'P', 2)
uf = Function(Vf)
u_f_array = uf.vector().get_local()
u_f_array[:] = 800#1000#tinit

'''
class CurrentDomainInitial(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[1], (0, yhi))

mark_cd = MeshFunction("size_t", mesh_final, mesh_final.topology().dim(), 0)
mark1 = CurrentDomainInitial()
mark1.mark(mark_cd, 1)
#mesh = SubMesh(mesh_final, mark_cd, 1)
print(type(mesh)) # <class 'dolfin.cpp.mesh.SubMesh'>
mesh = MeshView.create(mark_cd, 1)'''



mesh = Mesh('input_mesh_files/circular_rod_0.2_size.xml')

V = FunctionSpace(mesh, 'P', 2)

# Define initial value
u_n = interpolate(u_D, V)
#u_n = project(u_D, V)

# Define variational problem
u = Function(V)
u.interpolate(u_D)
v = TestFunction(V)
f = Constant(0)




# List
boundary_conditions = {0: {'Robin':  (r1,s1)},
                       1: {'Radiation':  (r3,s3)},
                       2: {'Dirichlet':  u_D},
                       3: {'GaussRobRadiation': (r2,s2,r3,s3,s4)},
                       4: {'RRadiation':  (r1,s1,r3,s3)}}

# Define boundary subdomains
tol = 1e-14
class BoundaryX0(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0, tol)
class BoundaryX1(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], xhi, tol)
class BoundaryZ0(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[2], 0, tol)
class BoundaryZ1(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[2], 1, 1+tol)
class BoundaryY1(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], 10, tol)
class BoundaryY0(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], 0, tol)
class BoundaryAl(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


def collect_bnd_int_and_f(mesh):
    # Mark boundaries
    boundary_markers = MeshFunction("size_t", mesh, mesh.topology().dim()-1, 9999)
    bxa = BoundaryY1(); bxa.mark(boundary_markers, 4)
    #bx0 = BoundaryAl(); bx0.mark(boundary_markers, 0)
    #bx1 = BoundaryAl(); bx1.mark(boundary_markers, 1)# temporary stoping to verify convection first 
    by0 = BoundaryY0(); by0.mark(boundary_markers, 2)
    #by1 = BoundaryAl(); by1.mark(boundary_markers, 3)

    # Collect Dirichlet conditions
    bcs = []
    for i in boundary_conditions:
        if 'Dirichlet' in boundary_conditions[i]:
            bc_cur = DirichletBC(V, boundary_conditions[i]['Dirichlet'],
                                 boundary_markers, i)
            bcs.append(bc_cur)

    # redefine ds to allow multiple Neumann BC regions
    ds = Measure('ds', domain = mesh, subdomain_data = boundary_markers)

    integrals_N = []
    for i in boundary_conditions:
        if 'Neumann' in boundary_conditions[i]:
            if boundary_conditions[i]['Neumann'] != 0:
                g = boundary_conditions[i]['Neumann']
                integrals_N.append(dt*tdiff*g*v*ds(i))

    integrals_R_a = []
    integrals_R_L = []
    for i in boundary_conditions:
        if 'Robin' in boundary_conditions[i]:
            if boundary_conditions[i]['Robin'] != 0:
                r, s = boundary_conditions[i]['Robin']
                integrals_R_a.append(dt*tdiff*r*u*v*ds(i))
                integrals_R_L.append(dt*tdiff*r*s*v*ds(i))
        if 'Radiation' in boundary_conditions[i]:
            if boundary_conditions[i]['Radiation'] != 0:
                r, s = boundary_conditions[i]['Radiation']
                integrals_R_a.append(dt*tdiff*r*pow(u,4)*v*ds(i))
                integrals_R_L.append(dt*tdiff*r*pow(s,4)*v*ds(i))
        if 'RRadiation' in boundary_conditions[i]:
            if boundary_conditions[i]['RRadiation'] != 0:
                r, s, rp, sp = boundary_conditions[i]['RRadiation']
                integrals_R_a.append(dt*tdiff*r*u*v*ds(i))
                integrals_R_L.append(dt*tdiff*r*s*v*ds(i))
                integrals_R_a.append(dt*tdiff*rp*pow(u,4)*v*ds(i))
                integrals_R_L.append(dt*tdiff*rp*pow(sp,4)*v*ds(i))
        if 'GaussRobRadiation' in boundary_conditions[i]:
            if boundary_conditions[i]['GaussRobRadiation'] != 0:
                r, s, rp, sp, g = boundary_conditions[i]['GaussRobRadiation']
                integrals_N.append(dt*tdiff*g*v*ds(i))
                integrals_R_a.append(dt*tdiff*r*u*v*ds(i))
                integrals_R_L.append(dt*tdiff*r*s*v*ds(i))
                integrals_R_a.append(dt*tdiff*rp*pow(u,4)*v*ds(i))
                integrals_R_L.append(dt*tdiff*rp*pow(sp,4)*v*ds(i))

    # needs u
    #cpv = conditional(gt(abs(u-tm), tr), cp, cp + lat/tr * (1-(abs(u-tm)/tr)) )
    cpv = cp 
    cpvrho = cpv*rho

    F = cpvrho*u*v*dx + tdiff*dt*dot(grad(u), grad(v))*dx - cpvrho*(u_n + dt*f)*v*dx + sum(integrals_N)
    F += sum(integrals_R_a) - sum(integrals_R_L)
    return F, bcs

# Time-stepping
t = 0
istep = 0
ostep = 0
########################################################################
# First time step simulation
n=0
for n in range (300):
    t += dt
    u_D.t = t

    s1.t = t; s2.t = t; s3.t = t; s4.t = t

    istep += 1
    print("step:", istep, t)

    F, bcs = collect_bnd_int_and_f(mesh)

    # Compute solution
    solve(F == 0, u, bcs)

    if n == 0:
        # Solver parameters
        prm=parameters
        info(prm, True)

    cmin=u.vector().get_local().min()
    cmax=u.vector().get_local().max()
    print('minmax', cmin, cmax, istep, t)

    # Update previous solution
    u_n.assign(u)

    # Plot solution
    if (istep % oplot == 0):
        ostep += 1
        xdmff = XDMFFile("thermal_output/forttemp" + format(ostep, '04') + ".xdmf")
        u.rename("u","temp")
        xdmff.write(u)
        xdmff.close()

    

########################################################################



ostep += 1
xdmff = XDMFFile("thermal_output/forttemp" + format(ostep, '04') + ".xdmf")
u.rename("u","temp")
xdmff.write(u)
xdmff.close()

