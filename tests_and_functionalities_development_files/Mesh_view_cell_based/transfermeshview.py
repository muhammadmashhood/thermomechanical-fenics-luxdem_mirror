#!/usr/bin/env python
# coding: utf-8

# In[2]:


import dolfin as df
mesh = df.UnitSquareMesh(10,10)
mf = df.MeshFunction("size_t",mesh, mesh.topology().dim(),0)
df.AutoSubDomain(lambda x,_: x[0]<0.4).mark(mf,1)

meshp = df.MeshView.create(mf,1)
mfn = df.MeshFunction("size_t",mesh, mesh.topology().dim(),0)
df.AutoSubDomain(lambda x,_: x[0]<0.9).mark(mfn,2)

meshn = df.MeshView.create(mfn,2)
elem = df.FiniteElement("Lagrange",mesh.ufl_cell(),1)
Vp= df.FunctionSpace(meshp,elem)
up = df.Function(Vp)

Vn= df.FunctionSpace(meshn,elem)
un = df.Function(Vn)	
expr = df.Expression("(x[0]>0.4)*jump",jump=400.,degree=1)
up=df.interpolate(expr,Vp)
un_exact=df.interpolate(expr,Vn)

#meshn.topology().mapping()[mesh.id()].vertex_map()
cellsp=meshp.topology().mapping()[mesh.id()].cell_map()
cellsn=meshn.topology().mapping()[mesh.id()].cell_map()

print(len(cellsp))
print(len(cellsn))
print(mesh.num_cells())

import numpy as np

map_dict= dict()
print(cellsp)
print(cellsn)
cellsidxp= [c.index() for c in df.cells(meshp)]
cellsidxn= [c.index() for c in df.cells(meshn)]

maplist=list()
#index of big mesh 
for item, c in enumerate(cellsp):
	if c in enumerate(cellsn):
		j=np.where(c==np.array(cellsn, dtype=int))[0]
		maplist.append(j[0])
		print(j)

print(maplist)




df.XDMFFile("testTransfer/up.xdfm").write(up)
# Assing dofs to quarter
for c in cellsidxp:
	cellnext = maplist[c]
	un.vector()[Vn.dofmap().cell_dofs(cellnext)] = up.vector()[Vp.dofmap().cell_dofs(c)]



df.XDMFFile("testTransfer/un.xdfm").write(un)
        


# In[ ]:




