#!/usr/bin/env python
# coding: utf-8

# In[2]:


from dolfin import *

mesh = UnitSquareMesh(10,10)

mfp = MeshFunction("size_t",mesh, mesh.topology().dim(),0)
AutoSubDomain(lambda x,_: x[0]<=0.4).mark(mfp,1)
meshp = MeshView.create(mfp,1)
XDMFFile("testTransfer/meshp.xdmf").write(meshp)

mfn = MeshFunction("size_t",mesh, mesh.topology().dim(),0)
AutoSubDomain(lambda x,_: x[0]<=0.9).mark(mfn,2)
meshn = MeshView.create(mfn,2)
XDMFFile("testTransfer/meshn.xdmf").write(meshn)

V= FunctionSpace(mesh, 'P', 1)

Vp= FunctionSpace(meshp, 'P', 1)
up = Function(Vp)

Vn= FunctionSpace(meshn, 'P', 1)
un = Function(Vn)	

#cpv = conditional(gt(abs(u-tm), tr), cp, cp + lat/tr * (1-(abs(u-tm)/tr)) )

expr = Expression('x[0] > 0.4 ? 400: 0*1', degree = 1)
#expr = Expression("gt(x[0],0.4),jump",jump=400.,degree=1)
#up = interpolate(expr,Vp)
up.assign(interpolate(expr,Vp))
un.assign(interpolate(expr,Vn))

un_exact = interpolate(expr,Vn)
u_exact = interpolate(expr,V)
XDMFFile("testTransfer/u_exact.xdmf").write(u_exact)
        





def solution_transfer_function(mesh_Complete, half_cube_mesh, quarter_cube_mesh, u_half_cube, u_quarter_cube):
    # Builds cells mapping between parent meshes
    half = half_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()
    quarter = quarter_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()

    # Builds cells mapping betwen childs
    map = [j for i, c in enumerate(half) for j, d in enumerate(quarter) if c==d]
    
    # functional spaces (must be same as in the actual problem)
    
    V_half_cube = FunctionSpace(half_cube_mesh, 'P', 1)
    V_quarter_cube = FunctionSpace(quarter_cube_mesh, 'P', 1)
    
    # Get cell dofmaps
    half_dofmap = V_half_cube.dofmap()
    quarter_dofmap = V_quarter_cube.dofmap()

    # Assign values to functions for testint
    #u_half_cube.vector()[:] = np.random.rand(u_half_cube.vector()[:].size, )

    # Assing dofs to quarter
    for c in cells(half_cube_mesh):
      u_quarter_cube.vector()[quarter_dofmap.cell_dofs(map[c.index()])] = u_half_cube.vector()[half_dofmap.cell_dofs(c.index())]
    return u_quarter_cube



un = solution_transfer_function(mesh, meshp, meshn, up, un)

XDMFFile("testTransfer/up.xdmf").write(up)

XDMFFile("testTransfer/un.xdmf").write(un)
