// Gmsh project created on Fri Dec  4 18:18:56 2020
SetFactory("OpenCASCADE");
//+
Cylinder(1) = {0, 0, 0, 0, 10, 0, 1, 2*Pi};
//+
Characteristic Length {2, 1} = 0.2;
