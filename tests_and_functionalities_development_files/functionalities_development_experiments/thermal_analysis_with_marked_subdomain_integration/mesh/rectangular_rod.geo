// Gmsh project created on Fri Dec  4 17:40:54 2020
SetFactory("OpenCASCADE");
//+
Point(1) = {0, 0, 0, 1.0};
//+
Point(2) = {0, 0, 1, 1.0};
//+
Point(3) = {1, 0, 0, 1.0};
//+
Point(4) = {1, 0, 1, 1.0};
//+
Point(5) = {1, 10, 0, 1.0};
//+
Point(6) = {1, 10, 1, 1.0};
//+
Point(7) = {0, 10, 0, 1.0};
//+
Point(8) = {0, 10, 1, 1.0};
//+
Point(9) = {5.2, -2.2, 7.4, 1.0};
//+
Recursive Delete {
  Point{9}; 
}
//+
Line(1) = {8, 7};
//+
Line(2) = {7, 5};
//+
Line(3) = {5, 6};
//+
Line(4) = {6, 8};
//+
Line(5) = {2, 1};
//+
Line(6) = {1, 3};
//+
Line(7) = {3, 4};
//+
Line(8) = {2, 4};
//+
Line(9) = {2, 8};
//+
Line(10) = {7, 1};
//+
Line(11) = {3, 5};
//+
Line(12) = {6, 4};
//+
Characteristic Length {8, 7, 5, 6, 2, 1, 3, 4} = 0.218;
//+
Curve Loop(1) = {3, 12, -7, 11};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {2, -11, -6, -10};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {1, 10, -5, 9};
//+
Plane Surface(3) = {3};
//+
Curve Loop(4) = {8, -12, 4, -9};
//+
Plane Surface(4) = {4};
//+
Curve Loop(5) = {1, 2, 3, 4};
//+
Plane Surface(5) = {5};
//+
Curve Loop(6) = {8, -7, -6, -5};
//+
Plane Surface(6) = {6};
//+
Surface Loop(1) = {1, 5, 3, 2, 6, 4};
//+
Surface Loop(2) = {4, 6, 1, 5, 3, 2};
//+
Volume(1) = {2};
