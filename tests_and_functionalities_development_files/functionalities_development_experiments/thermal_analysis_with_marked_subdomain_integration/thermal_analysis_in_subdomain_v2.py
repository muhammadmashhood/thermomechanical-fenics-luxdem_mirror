
from fenics import *
import logging
set_log_level(logging.WARNING)
import sys


# Unit cube mesh making

mesh_final = UnitCubeMesh(12,12,12);



# Define boundary subdomains
tol_boundary = 1e-14

class BoundaryY0(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], 0, tol_boundary)
class BoundaryY1_diff(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], 1, tol_boundary)
class DiffusionHalfDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 0.5))
        

# Time-stepping
t = 0
istep = 0
dt = 1e-2
num_steps = 250

# Functional space
V = FunctionSpace(mesh_final, 'P', 1)

# Define initial value

u_n = interpolate(Constant(400), V)
u = Function(V)
u.interpolate(Constant(400))
v = TestFunction(V)
f = Constant(0)

# marking the subdomain 
mark_diff_domain = MeshFunction("size_t", mesh_final, mesh_final.topology().dim(), 0)
mark_domain = DiffusionHalfDomain()
mark_diff_domain.set_all(0)
mark_domain.mark(mark_diff_domain, 1)


for n in range(num_steps):

    # Update current time
    t += dt

    istep += 1
    print("step:", istep, t)

    # Mark boundaries 
    boundary_markers = MeshFunction("size_t", mesh_final, mesh_final.topology().dim()-1, 9999)
    by0 = BoundaryY0(); by0.mark(boundary_markers, 2)
    by1 = BoundaryY1_diff(); by1.mark(boundary_markers, 3)

    #Dirichlet conditions
    bcs = []

    bc_cur = DirichletBC(V, Constant(400), boundary_markers, 2)
    bcs.append(bc_cur)
    bc_cur = DirichletBC(V, Constant(500), boundary_markers, 3)
    bcs.append(bc_cur)
    
    # physical parameters
    cpv = 50
    rho = 4420
    cpvrho = cpv*rho
    tdiff = 1000

    dx = Measure('dx', domain = mesh_final, subdomain_data = mark_diff_domain, subdomain_id = 1)
    #dx = Measure("dx")
    #dxx = dx(subdomain_data=mark_diff_domain)
    
    #submesh = SubMesh(mesh_final, mark_diff_domain, 1)
    
    F = cpvrho*u*v*dx + tdiff*dt*dot(grad(u), grad(v))*dx - cpvrho*(u_n + dt*f)*v*dx #+ u*v*dxx(0)      

    # Compute solution
    solve(F == 0, u, bcs)

    # previous solution
    u_n.assign(u)

    # Plot solution
    xdmff = XDMFFile("forttemp" + format(istep, '04') + ".xdmf")
    u.rename("u","temp")
    xdmff.write(u)
    xdmff.close()
