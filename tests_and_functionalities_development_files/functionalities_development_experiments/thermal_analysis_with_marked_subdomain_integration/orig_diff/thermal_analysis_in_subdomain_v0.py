# Basic code to give the dx(i) idea but solution to new elements not assigned

"""
FEniCS tutorial demo program: Heat equation with Dirichlet conditions.
Test problem is chosen to give an exact solution at all nodes of the mesh.

  u'= Laplace(u) + f  in the unit square
  u = u_D             on the boundary
  u = u_0             at t = 0

"""

from __future__ import print_function
from fenics import *
import logging
set_log_level(logging.WARNING)
import numpy as np
#import matplotlib.pyplot as plt
import socket
print("Machine:", socket.gethostname())
print("2D-heat-DED version 1.0")
#from mshr import *

# Build geometry
from conf_geom import *

print("\nInit dims: ", xhi*1000, " x ", yhi_init*1000, " mm^2")
for xyr in xyrs:
    print("circle: ", xyr[0], xyr[1], xyr[2])

print("   grid_d: ", grid_d*1000, " mm")
print("nx ", nx)

from conf_proc import *
print("Number of heating passes: ", nhepa)
print("Beam radius: ", beamrad*1000, " mm")
print("Laser ahead: ", lah*1000, " mm")
print("Laser veloc: ", velbeam*1000, " mm/s")
print("Laser pause: ", pause, " s")

Td = xhi/velbeam      # time per layer, deposition only
Tl = Td + pause       # time per layer, adding pause

num_steps = int(Tl/dt+.5)   # number of time steps
#num_steps = 1000
print("Per layer time: ", Tl)
print("Per  depo time: ", Td)
print("            dt: ", dt)
print(" (time down sc):", sdt)
print("     num_steps: ", num_steps)
print("    plot every: ", oplot)
print("  per l. plots: ", float(num_steps)/oplot)

print("Added #layers x thickness:        ", add_layers, " x ",ynsub_a*st_per_e_a*grid_d*1000, " mm")
print(" els. per layer, el. y-thickness: ", ynsub_a,st_per_e_a*grid_d*1000, " mm")
print("                 el. x-thickness: ", add_xstep*grid_d*1000, " mm")

print("Daffe #layers x thickness:        ", daf_layers, " x ",ynsub_d*st_per_e_d*grid_d*1000, " mm")
print(" els. per layer, el. y-thickness: ", ynsub_d,st_per_e_d*grid_d*1000, " mm")
print("                 el. x-thickness: ", daf_xstep*grid_d*1000, " mm")

print("Affec #layers x thickness:        ", haf_layers, " x ",ynsub_h*st_per_e_h*grid_d*1000, " mm")
print(" els. per layer, el. y-thickness: ", ynsub_h,st_per_e_h*grid_d*1000, " mm")
print("                 el. x-thickness: ", haf_xstep*grid_d*1000, " mm")

print("Number of added layers x thickness: ", add_layers, " x ", layth_a*1000, " mm")
print("Number of x-subdiv. in added layer: ", xnsub_a)
print("Number of y-subdiv. in added layer: ", ynsub_a)

print("Segments: ", seg)

from conf_mat import *
sigm = 5.67e-08    # SB constant
rad = eps*sigm/tcd # radiation parameter (eps*sig/k)

conv = convh/tcd   # convection parameter (h/k)

layth_d = st_per_e_d*grid_d
layth_h = st_per_e_h*grid_d

yhi = yhi_init

print("ts tl", ts, tl)
print("GT tr From tm, cp, cp + lat/(tl-ts))", tr, tm, cp, cp + lat/tr)

# Define boundary condition
# Dirichlet
u_D = Constant(tinit)

# Neumann
upx = Constant(-10);upy = Constant(-10)

# Robin
# 1: left+right, 2: top
s1 = Constant(tglov);s2 = Constant(tglov);r1 = Constant(conv);r2 = Constant(conv)

# radiation on Top
s3 = Constant(tglov);r3 = Constant(rad)

# Gaussian
print("power3d",pow3d)
print("alpha",alpha)
s4 = Expression(' -alpha*pow3d/(2*pi*pow(beamrad,2)) * exp(-0.5 * pow( (x[0]-velbeam*t-lah)/beamrad , 2)) ',
                degree=2, pow3d=pow3d, beamrad=beamrad, velbeam=velbeam, lah=lah, t=0, alpha=alpha)

import sys
numarg = (len(sys.argv)-1)
arg1 = sys.argv[1]
print("reading mesh from file", arg1)
mesh_final = Mesh(arg1)


mxf = XDMFFile("mesh.xdmf")
mxf.write(mesh_final)
mxf.close()

Vf = FunctionSpace(mesh_final, 'P', 1)
uf = Function(Vf)
u_f_array = uf.vector().get_local()
u_f_array[:] = 420#1000#tinit

# Convert subdomains to mesh function for plotting
mf = MeshFunction("size_t", mesh_final, 2, mesh_final.domains())
plot(mf, "Subdomains")
#plt.savefig("voids_subdomains.png")

class CurrentDomainInitial(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[1], (0, yhi))

mark_cd = MeshFunction("size_t", mesh_final, mesh_final.topology().dim(), 0)
mark1 = CurrentDomainInitial()
mark1.mark(mark_cd, 1)
#mesh = SubMesh(mesh_final, mark_cd, 1)

#V = FunctionSpace(mesh, 'P', 1)
V = FunctionSpace(mesh_final, 'P', 1)

# Define initial value
u_n = interpolate(u_D, V)
#u_n = project(u_D, V)

# Define variational problem
u = Function(V)
u.interpolate(u_D)
v = TestFunction(V)
f = Constant(0)


####################### write checkpoint and mesh info for mesh and temperature solution (h5) #######################
TempuratureSolution = XDMFFile("tempProfile/temp.xdmf")
TempuratureSolution.parameters["functions_share_mesh"] = False # since mesh is changing

meshinfo= HDF5File(mesh_final.mpi_comm(),'tempProfile/layersgeom.h5', 'w')  





# List
boundary_conditions = {2: {'Dirichlet':  u_D}}

# Define boundary subdomains
tol = 1e-14
class BoundaryX0(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0, tol)
class BoundaryX1(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], xhi, tol)
class BoundaryY0(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], 0, tol)
class BoundaryY1(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], yhi, tol)
class BoundaryAl(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary
tol2=0.000001; tol3=0.00000001

class CurrentDomain2(SubDomain):
    def inside(self, x, on_boundary):
        return between(
            x[1], (0, yhi)) and not \
            (\
             (x[1] > yhi-layth_a+tol2) and x[0] > ((n+1)*dt*velbeam+tol3)\
            )
class CurrentDomain3(SubDomain):
    def inside(self, x, on_boundary):
        return between(
            x[1], (0, yhi)) and not \
            (\
             (x[1] > yhi-layth_a+tol2) and x[0] < xhi - ((n+1)*dt*velbeam+tol3)\
            )
'''
class CurrentDomain2(SubDomain):
    def inside(self, x, on_boundary):
        #print('the Domain2 is called')
        return between(
             x[1], (0, yhi)) 
             
class CurrentDomain3(SubDomain):
    def inside(self, x, on_boundary):
        print('the Domain3 is called')
        return between(
             x[1], (0, yhi))  
'''
#def collect_bnd_int_and_f(mesh):
def collect_bnd_int_and_f(mark_cd):
    # Mark boundaries
    boundary_markers = MeshFunction("size_t", mesh_final, mesh_final.topology().dim()-1, 9999)
    #boundary_markers = MeshFunction("size_t", mesh, mesh.topology().dim()-1, 9999)

    by0 = BoundaryY0(); by0.mark(boundary_markers, 2)
    

    # Collect Dirichlet conditions
    bcs = []
    for i in boundary_conditions:
        if 'Dirichlet' in boundary_conditions[i]:
            bc_cur = DirichletBC(V, boundary_conditions[i]['Dirichlet'],
                                 boundary_markers, i)
            bcs.append(bc_cur)

    # redefine ds to allow multiple Neumann BC regions
    

    integrals_N = []
    

    integrals_R_a = []
    integrals_R_L = []
    

    # needs u
    cpv = conditional(gt(abs(u-tm), tr), cp, cp + lat/tr * (1-(abs(u-tm)/tr)) )
    cpvrho = cpv*rho
    
    #dx = Measure('dx', domain = mesh_final, subdomain_data = mark_cd)
    dx = Measure("dx")
    dxx = dx(subdomain_data=mark_cd)
    
    F = cpvrho*u*v*dxx(1) + tdiff*dt*dot(grad(u), grad(v))*dxx(1) - cpvrho*(u_n + dt*f)*v*dxx(1) + sum(integrals_N)
    F += sum(integrals_R_a) - sum(integrals_R_L)
    return F, bcs

# Time-stepping
t = 0
istep = 0
ostep = 0


for ihepa in range(nhepa):

    if switch_dir and (ihepa % 2 == 0):
        even = True
    else:
        even = False

    if (even):
        s4.t = Td
        s4.lah = -lah
    else:
        s4.t = 0
        s4.lah =  lah

    for n in range(num_steps):

        # Update current time
        t += dt
        u_D.t = t
        if (even):
            s1.t = t; s2.t = t; s3.t = t; s4.t -= dt
        else:
            s1.t = t; s2.t = t; s3.t = t; s4.t = t

        istep += 1
        print("step:", istep, t)

        F, bcs = collect_bnd_int_and_f(mark_cd)

        # Compute solution
        solve(F == 0, u, bcs)

        if n == 0:
            # Solver parameters
            prm=parameters
            info(prm, True)

        cmin=u.vector().get_local().min()
        cmax=u.vector().get_local().max()
        print('minmax', cmin, cmax, istep, t)

        # Update previous solution
        u_n.assign(u)

        # Plot solution
        if (istep % oplot == 0):
            ostep += 1
            xdmff = XDMFFile("forttemp" + format(ostep, '04') + ".xdmf")
            u.rename("u","temp")
            xdmff.write(u)
            xdmff.close()
            
            
            meshinfo.write(mesh_final,"/submesh/{}_part".format(ostep))
            meshinfo.write(mark_cd,"/meshfunction/{}_part".format(ostep)) # cell marker
            meshinfo.write(u, "/solution/{}_part".format(ostep))
            
            
            xint = interpolate(Expression("x[0]", degree=1), V)
            yint = interpolate(Expression("x[1]", degree=1), V)
            xcoords = xint.vector()[yint.vector().get_local()>yhi-tol]
            yvalues = u.vector()[yint.vector().get_local()>yhi-tol]
            outstring = zip(xcoords, yvalues)
            xyf = open('xy' + format(ostep, '04') + '.dat', 'w')
            for line in outstring:
                xyf.write(" ".join(str(item) for item in line) + "\n")
            xyf.close()


nl = 0
for nl in range(1,add_layers+1):
    print("adding layer ", nl, istep, t)
    if switch_dir and (nl % 2 == 0):
        even = True
    else:
        even = False

    yhi = yhi_init + nl*layth_a

    if (even):
        s4.t = Td
        s4.lah = -lah
    else:
        s4.t = 0
        s4.lah =  lah

    for n in range(num_steps):

        mark_cd.set_all(0)
        if (even):
            mark1 = CurrentDomain3()
        else:
            mark1 = CurrentDomain2()
        mark1.mark(mark_cd, 1)
        #mesh = SubMesh(mesh_final, mark_cd, 1)
        
        #mark1.mark(mark_cd, ostep+1)
        #mesh = SubMesh(mesh_final, mark_cd, ostep+1)

        

        

        # Update current time
        t += dt
        u_D.t = t
        if (even):
            s1.t = t; s2.t = t; s3.t = t; s4.t -= dt
            s4.lah = -lah
        else:
            s1.t = t; s2.t = t; s3.t = t; s4.t += dt
            s4.lah =  lah
        istep += 1
        print("step:", istep, t)

        F, bcs = collect_bnd_int_and_f(mark_cd)

        # Compute solution
        solve(F == 0, u, bcs)

        cmin=u.vector().get_local().min()
        cmax=u.vector().get_local().max()
        print('minmax', cmin, cmax, istep, t)

        # Update previous solution
        u_n.assign(u)

        # Plot solution
        if (istep % oplot == 0):
            ostep += 1
            xdmff = XDMFFile("forttemp" + format(ostep, '04') + ".xdmf")
            u.rename("u","temp")
            xdmff.write(u)
            xdmff.close()

            TempuratureSolution.write_checkpoint(u,"temperature",float(ostep),append=True)

            meshinfo.write(mesh_final,"/submesh/{}_part".format(ostep))
            meshinfo.write(mark_cd,"/meshfunction/{}_part".format(ostep)) # cell marker
            meshinfo.write(u, "/solution/{}_part".format(ostep))


            
            
            
            xint = interpolate(Expression("x[0]", degree=1), V)
            yint = interpolate(Expression("x[1]", degree=1), V)
            xcoords = xint.vector()[yint.vector().get_local()>yhi-tol]
            yvalues = u.vector()[yint.vector().get_local()>yhi-tol]
            outstring = zip(xcoords, yvalues)
            xyf = open('xy' + format(ostep, '04') + '.dat', 'w')
            for line in outstring:
                xyf.write(" ".join(str(item) for item in line) + "\n")
            xyf.close()

            yhip = yhi_init + (nl-1)*layth_a
            xint = interpolate(Expression("x[0]", degree=1), V)
            yint = interpolate(Expression("x[1]", degree=1), V)
            xcoords = xint.vector()[yint.vector().get_local()==yhip]
            yvalues = u.vector()[yint.vector().get_local()==yhip]
            outstring = zip(xcoords, yvalues)
            xyf = open('xyp' + format(ostep, '04') + '.dat', 'w')
            for line in outstring:
                xyf.write(" ".join(str(item) for item in line) + "\n")
            xyf.close()

TempuratureSolution.close()
meshinfo.close()

ostep += 1
xdmff = XDMFFile("forttemp" + format(ostep, '04') + ".xdmf")
u.rename("u","temp")
xdmff.write(u)
xdmff.close()

xint = interpolate(Expression("x[0]", degree=1), V)
yint = interpolate(Expression("x[1]", degree=1), V)
xcoords = xint.vector()[yint.vector().get_local()==yhi]
yvalues = u.vector()[yint.vector().get_local()==yhi]
outstring = zip(xcoords, yvalues)
xyf = open('xy' + format(ostep, '04') + '.dat', 'w')
for line in outstring:
    xyf.write(" ".join(str(item) for item in line) + "\n")
xyf.close()

if (nl>0):
    yhip = yhi_init + (nl-1)*layth_a
    xint = interpolate(Expression("x[0]", degree=1), V)
    yint = interpolate(Expression("x[1]", degree=1), V)
    xcoords = xint.vector()[yint.vector().get_local()==yhip]
    yvalues = u.vector()[yint.vector().get_local()==yhip]
    outstring = zip(xcoords, yvalues)
    xyf = open('xyp' + format(ostep, '04') + '.dat', 'w')
    for line in outstring:
        xyf.write(" ".join(str(item) for item in line) + "\n")
    xyf.close()
