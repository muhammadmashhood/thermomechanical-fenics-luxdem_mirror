# modified to work with the docker image's fsm
from dolfin import *
import fsm

#set_log_level(10)

class DirichletBoundaryX(SubDomain):
    def inside(self, x, on_boundary):
        return (x[0] < DOLFIN_EPS)
class DirichletBoundaryY(SubDomain):
    def inside(self, x, on_boundary):
        return (x[1] < DOLFIN_EPS) 
class DirichletBoundaryZ(SubDomain):
    def inside(self, x, on_boundary):
        return (x[2] < DOLFIN_EPS)

class HalfDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 0.5))
class QuarterDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 0.75))
class WholeDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 1))

        
def solution_transfer_function(mesh_Complete, half_cube_mesh, quarter_cube_mesh, u_half_cube, u_quarter_cube):
    # Builds cells mapping between parent meshes
    half = half_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()
    quarter = quarter_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()

    # Builds cells mapping betwen childs
    map = [j for i, c in enumerate(half) for j, d in enumerate(quarter) if c==d]
    
    # functional spaces (must be same as in the actual problem)
    V_half_cube = VectorFunctionSpace(half_cube_mesh, "Lagrange", 2)
    V_quarter_cube = VectorFunctionSpace(quarter_cube_mesh, "Lagrange", 2)
    
    # Get cell dofmaps
    half_dofmap = V_half_cube.dofmap()
    quarter_dofmap = V_quarter_cube.dofmap()

    # Assign values to functions for testint
    #u_half_cube.vector()[:] = np.random.rand(u_half_cube.vector()[:].size, )

    # Assing dofs to quarter
    for c in cells(half_cube_mesh):
      u_quarter_cube.vector()[quarter_dofmap.cell_dofs(map[c.index()])] = u_half_cube.vector()[half_dofmap.cell_dofs(c.index())]
    return u_quarter_cube

      

# Making complete mesh
mesh_Complete = UnitCubeMesh(8,8,8);
mark_sub_domain = MeshFunction("size_t", mesh_Complete, mesh_Complete.topology().dim(), 0)
V_Complete  = VectorFunctionSpace(mesh_Complete, "Lagrange", 2)

number_of_submesh = 2#3
for submesh_number in range(number_of_submesh): 
    
    if (submesh_number == 0) :
        mark_sub_domain.set_all(0)
        mark_domain = WholeDomain()#HalfDomain()
        mark_domain.mark(mark_sub_domain, 1)
        sub_mesh = MeshView.create(mark_sub_domain, 1)
        print('HalfDomain')
        
    elif (submesh_number == 1) :
        mark_sub_domain.set_all(0)
        mark_domain = WholeDomain()#QuarterDomain()
        mark_domain.mark(mark_sub_domain, 1)
        sub_mesh = MeshView.create(mark_sub_domain, 1)
        print('QuarterDomain')
        
        
    elif (submesh_number == 2) :
        mark_sub_domain.set_all(0)
        mark_domain = WholeDomain()
        mark_domain.mark(mark_sub_domain, 1)
        sub_mesh = MeshView.create(mark_sub_domain, 1)
        print('WholeDomain')
        

    mesh = sub_mesh
    
    E = 20000.0;
    nu = 0.3;

    scheme = "default"
    degree = 3
    dx = Measure("dx")
    dx = dx(degree=degree, scheme=scheme)

    V  = VectorFunctionSpace(mesh, "Lagrange", 2)
    element_t = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=36, quad_scheme=scheme)
    Vt = FunctionSpace(mesh, element_t)
    element_s = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
    Vs = FunctionSpace(mesh, element_s)

    T  = FunctionSpace(mesh, "Lagrange", 1)
    zero = Constant(0.0)

    bc0 = DirichletBC(V.sub(0), zero, DirichletBoundaryY(), method="pointwise")
    bc1 = DirichletBC(V.sub(1), zero, DirichletBoundaryY(), method="pointwise")
    bc2 = DirichletBC(V.sub(2), zero, DirichletBoundaryY(), method="pointwise")

    bcs = [bc0, bc1, bc2]

    E_t = 0.3*E
    hardening_parameter = E_t/(1.0 - E_t/E)
    yield_stress = 9.0

    u = Function(V, name="u")
    t = Function(T, name="T")

    temp_exp = Expression(" 1000*x[1]*x[1]*x[1]*t1",t1=0,degree=2)

    if (submesh_number > 0):  # because first subdomain gets everything from the start as default
         
        u = solution_transfer_function(mesh_Complete, mesh_old, mesh, u_old, u)
        #u.interpolate(zero)  
        u_to_plot = u
        # Write output to files from last submesh
        xdmff = XDMFFile("u_to_plot/u_to_plot" + format(submesh_number, '04') + ".xdmf")    
        xdmff.write(u_to_plot);
        xdmff.close()



    def mech_eps(u):
        return as_vector([u[i].dx(i) for i in range(3)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1), (0, 2), (1, 2)]])

    def thermal_eps(t):
        expansion_coeff = Constant(10.E-6)
        return expansion_coeff * as_vector([1., 1., 1., 0., 0., 0.]) * t

    def eps(u, t):
        return mech_eps(u) - thermal_eps(t)

    def sigma(s):
        #s = ss.function_space()
        return as_matrix([[s[0], s[3], s[4]], [s[3], s[1], s[5]], [s[4], s[5], s[2]]])

    def tangent(t):
        #t = tt.function_space()
        return as_matrix([[t[i*6 + j] for j in range(6)] for i in range(6)])

    J2 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter)
    Qdef = fsm.UFLQuadratureFunction(eps(u, t), element_s, mesh)
    fsm_constitutive_update = fsm.ConstitutiveUpdate(Qdef, J2)
    #fsm_tangent = QuadratureFunction(mesh, Vt.element(), fsm_constitutive_update, fsm_constitutive_update.w_tangent())
    #fsm_stress = QuadratureFunction(mesh, Vs.element(), fsm_constitutive_update.w_stress())
    fsm_tangent = fsm.QuadratureFunction(Vt, fsm_constitutive_update.w_tangent(), fsm_constitutive_update)
    fsm_stress  = fsm.QuadratureFunction(Vs, fsm_constitutive_update.w_stress())


    v = TestFunction(V)
    uTrial = TrialFunction(V)

    a = inner(mech_eps(v), dot(tangent(fsm_tangent), mech_eps(uTrial)) )*dx
    L = inner(grad(v), sigma(fsm_stress))*dx

    nonlinear_problem = fsm.PlasticityProblem(a, L, u, fsm_tangent, fsm_stress, bcs)

    nonlinear_solver = NewtonSolver()
    nonlinear_solver.parameters["convergence_criterion"] = "incremental";
    nonlinear_solver.parameters["maximum_iterations"]    = 50;
    nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
    nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;

    # File names for output

    eps_p_eq = fsm_constitutive_update.eps_p_eq()
    #fsm_constitutive_update.eps_p_eq().compute_mean(eps_eq);

    element_eps_p_eq_project = FiniteElement("CG", mesh.ufl_cell(), degree=1)
    V_eps_p_eq_project = FunctionSpace(mesh, element_eps_p_eq_project)
    eps_p_eq_project = Function(V_eps_p_eq_project, name="eps_p_eq")
    
#for submesh_number in range(2): 
    if (submesh_number == 0):
        temp_exp.t1 = 1
    else:
        temp_exp.t1 = -1

    
    t.assign(interpolate(temp_exp,T))

    # Solve non-linear problem
    nonlinear_solver.solve(nonlinear_problem.cpp_object(), u.vector());

    # Update variables
    fsm_constitutive_update.update_history();

    # Write output to files
    
    file1_u = XDMFFile("output/output_u" + format(submesh_number, '04') + ".xdmf")
    file1_u.write(u, t=float(submesh_number));
    file1_u.close()
    
    eps_p_eq_project.vector()[:] = project(eps_p_eq, V_eps_p_eq_project, solver_type='gmres',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ).vector()
    #previous subdomain solution:
    eps_p_eq_old = eps_p_eq
    u_old = u
    V_old = V
    mesh_old = mesh
    
    file1_eqv_eps_p = XDMFFile("output/output_eqv_eps_p" + format(submesh_number, '04') + ".xdmf")
    file1_eqv_eps_p.write(eps_p_eq_project, t=float(submesh_number));
    file1_eqv_eps_p.close()    
