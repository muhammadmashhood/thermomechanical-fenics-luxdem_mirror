Newton iteration 0: r (abs) = 1.365e-01 (tol = 1.000e-15) r (rel) = 1.000e+00 (tol = 1.000e-06)
Newton iteration 1: r (abs) = 3.881e-03 (tol = 1.000e-15) r (rel) = 2.843e-02 (tol = 1.000e-06)
Newton iteration 2: r (abs) = 1.376e-04 (tol = 1.000e-15) r (rel) = 1.008e-03 (tol = 1.000e-06)
Newton iteration 3: r (abs) = 3.216e-06 (tol = 1.000e-15) r (rel) = 2.356e-05 (tol = 1.000e-06)
Newton iteration 4: r (abs) = 4.920e-08 (tol = 1.000e-15) r (rel) = 3.605e-07 (tol = 1.000e-06)
Newton solver finished in 5 iterations and 5 linear solver iterations.


Newton iteration 0: r (abs) = 1.346e-01 (tol = 1.000e-15) r (rel) = 1.000e+00 (tol = 1.000e-06)
Newton iteration 1: r (abs) = 5.234e-03 (tol = 1.000e-15) r (rel) = 3.889e-02 (tol = 1.000e-06)
Newton iteration 2: r (abs) = 9.974e-04 (tol = 1.000e-15) r (rel) = 7.412e-03 (tol = 1.000e-06)
Newton iteration 3: r (abs) = 3.241e-05 (tol = 1.000e-15) r (rel) = 2.408e-04 (tol = 1.000e-06)
Newton iteration 4: r (abs) = 5.093e-07 (tol = 1.000e-15) r (rel) = 3.784e-06 (tol = 1.000e-06)
Newton iteration 5: r (abs) = 1.313e-10 (tol = 1.000e-15) r (rel) = 9.754e-10 (tol = 1.000e-06)
Newton solver finished in 6 iterations and 6 linear solver iterations.

