//include pybdin...
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>

#include <cmath>
#include <vector>
#include <memory>

#include <Eigen/Core>
/*#include <dolfin/function/GenericFunction.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>*/
#include <dolfin.h>

#include "/usr/local/lib/python3.6/dist-packages/fsm-0.0.2-py3.6.egg/FenicsSolidMechanics.h"
//#include <QuadratureFunction.h>
//#include <VonMises.h>
//#include <PlasticityModel.h>
//#include <ConstitutiveUpdate.h>
//#include <HistoryData.h>
//fsm might be written as fenicssolid (but not sure)
void transferHistory (const std::shared_ptr<fsm::ConstitutiveUpdate> & constituiveupdate1,
	const std::shared_ptr<fsm::ConstitutiveUpdate> & constitutiveupdate2,
	std::vector<std::size_t> cell_map1,
	std::vector<std::size_t> cell_map2)  // may be MeshFunction<std::size_t, .... other arguments also needed ? 
{ 
std::cout<<"#############################"<<std::endl;
//std::cout<<J2->hardening_parameter(0.1)<<std::endl;
std::cout<<"Hi there I am in the function"<<std::endl;
std::cout<<"constituiveupdate1()	"<< typeid ( constituiveupdate1 ).name() <<std::endl;
std::cout<<"constitutiveupdate2()	"<< typeid ( constitutiveupdate2 ).name() <<std::endl;
//std::cout<<"meshfunction1()"<< typeid ( meshfunction1 ).name() <<std::endl;
//std::cout<<"*meshfunction1()"<< typeid ( *meshfunction1 ).name() <<std::endl;
//std::cout<<"meshfunction2()"<< typeid ( meshfunction2 ).name() <<std::endl;
//std::cout<<"meshfull()"<< typeid ( meshfull ).name() <<std::endl;
std::cout<<"cell_map1	"<< typeid ( cell_map1 ).name() <<std::endl;
std::cout<<"cell_map2	"<< typeid ( cell_map2 ).name() <<std::endl;
//std::cout<<"cell_map1[500]		"<< cell_map1[500] <<std::endl;
//std::cout<<"cell_map1.size()	"<< cell_map1 <<std::endl;
std::cout<<"cell_map1.size()	"<< cell_map1.size() <<std::endl;
std::cout<<"cell_map2.size()	"<< cell_map2.size() <<std::endl;
std::cout<<"#############################"<<std::endl;


/*
// Mapping between 2 submesh
std::vector<std::size_t> vertex_map1,cell_map1;
auto mapping1 = std::make_shared<dolfin::MeshView>(meshfull,vertex_map1,cell_map1);
auto submesh1 = std::make_shared<dolfin::Mesh>(mapping1->create(*meshfunction1, 1));

int num_cells1 = submesh1->num_cells();
std::cout<<"num_cells1	"<< num_cells1 <<std::endl;
//std::vector<std::size_t> cell_map1;  // alternative of original idea


std::vector<std::size_t> vertex_map2,cell_map2;
auto mapping2 = std::make_shared<dolfin::MeshView>(meshfull,vertex_map2,cell_map2);
auto submesh2 = std::make_shared<dolfin::Mesh>(mapping2->create(*meshfunction2, 1));

int num_cells2 = submesh2->num_cells();
std::cout<<"num_cells2	"<< num_cells2 <<std::endl;
//std::vector<std::size_t> cell_map2; // alternative of original idea



cell_map1=mapping1->cell_map();   // check if dolfin::Mesh can support this function otherwise MeshView explore
cell_map2=mapping2->cell_map();    //cell_map2St6vectorImSaImEE
*/



std::vector<int> mapping(cell_map1.size());
std::size_t counter=0;
for (auto i : cell_map1) {
   //std::cout <<"i:	"<< i <<std::endl;
   auto itr = std::find(cell_map2.begin(),cell_map2.end(),i); // value of the iterator is returned as itr at location where cell_map1 is present in squence of cell_map2 
	if ( itr != cell_map2.end() ){
		//std::cout <<"itr:	"<< *itr <<std::endl;
		}
    else{
		std::cout << "#################################################################\n";
		std::cout << "Element from previous subdomain is not found in current subdomain\n";
		std::cout << "#################################################################\n";}
		
   int index = std::distance(cell_map2.begin(), itr); // the index gets the number of elements between cell_map2 and the itr position of cell_map2 where cell_map1 is located 
   //std::cout <<"index:	"<<index<<std::endl;
   mapping[counter]=index; // the counter position of the mapping array stores the number of elements present between start of cell_map2 and place where cell_map1 (i) is present
   //std::cout<<"mapping:	"<<mapping[counter]<<std::endl;
   ++counter;
   
   
} // mapping between 2 global ID, submesh=> global
// mapping 1stmesh(smaller one)=> Index of cell  (not the ID) in Bigger one.


std::size_t counterr=0;  //unsigned int
std::size_t cell_counter = 0;
std::size_t eqv_strain_counter = 0;
std::size_t eqv_strain_counter_ip = 0;

for(auto i:mapping){ // kind of iterating through each element

	// is cell_index1 and 2 int type?
	
	//const std::size_t cell_index1 = cell_map1[counterr]; //1st mesh.   // index of the first mesh cell at every mapping entry of length cell_map1
	const std::size_t cell_index1 = counterr;
	const std::size_t cell_index2 = i; // index of the cell in mesh 2 which corresponds to the physical location cell of mesh 1 in this way the system knows that the current cell from mesh 1 in question has which location in mesh 2 to assign correctly the solution
	//std::cout <<"cell_index2:	"<<cell_index2<<std::endl;
	
	
	Eigen::Matrix<double, 6, 1> oldvalueeps;
	//strain_p.setZero(); // set the plastic strain first zero for constitutiveupdate2 as zero	
	Eigen::Matrix<double, 1, 1> oldvalue_equiv;	
	
	//strain_func1 = constituiveupdate1->strain_func();  // strain_function with or without () or _strain_function ? 
	//int num_ip_dofs = strain_func1->element()->value_dimension(0); 
	//int num_ip_per_cell = strain_func1->element()->space_dimension()/num_ip_dofs; // any alternative for num_ip_per_cell
	
	std::size_t num_ip_per_cell = constituiveupdate1->num_ip_per_cell();
	//std::cout <<"num_ip_per_cell	"<<num_ip_per_cell<<std::endl;
	//std::size_t eqv_strain_counter_ip = 0;
		for (std::size_t ip = 0; ip < num_ip_per_cell; ip++) //151 line from Constitive update cpp
		{
		    /*constituiveupdate1->eps_p()->get_old_values(
			cell_index1, ip, oldvalueeps);   // Will it access the plastic strain using _eps_p() ?
			
			constitutiveupdate2->update_eps_p(cell_index2, ip, oldvalueeps);		*/	
			//std::cout<<"oldvalueeps		"<<oldvalueeps;
		    
			
			
			//constitutiveupdate2->eps_p()->set_new_values(
			//cell_index2, ip, oldvalueeps);  
		    
			
			//if (oldvalue_equiv[0] > 0.001)
			//{
			//	std::cout<<"	oldvalueeps		"<<oldvalue_equiv;
			//}
			//std::size_t cell = 2;
			//unsigned int integ_p = 2;
			//std::cout<<"	ip		"<<ip;
			
			/*const std::shared_ptr<const fsm::HistoryData>  eps_p_eqstate1 = constituiveupdate1->eps_p_eq();
			Eigen::Matrix<double, 1, 1> oldvalue_equiv;
			eps_p_eqstate1->get_old_values(cell_index1, ip, oldvalue_equiv);
			const std::shared_ptr<const fsm::HistoryData>  eps_p_eqstate2 = constitutiveupdate2->eps_p_eq();
			eps_p_eqstate2->set_new_values(cell_index2, ip, oldvalue_equiv);
			std::cout<<"	alive		"<<ip;*/
			
			//constituiveupdate1->eps_p_eq()->get_old_values(cell_index1, ip, oldvalue_equiv);
			constituiveupdate1->eps_p_eq()->get_old_values(cell_index1, ip, oldvalue_equiv);
			
			
			
			//constituiveupdate1->eps_p_eq()->set_new_values(cell_index1, ip, oldvalue_equiv);
			//constitutiveupdate2->eps_p_eq()->set_new_values(cell_index2, ip, oldvalue_equiv);
			
			constitutiveupdate2->update_eps_p_eq(cell_index2, ip, oldvalue_equiv);
			
			/*if (oldvalue_equiv[0] > 0.1 && oldvalue_equiv[0] < -0.1)
			{	std::cout<<std::endl;
				std::cout<<"oldvalueeps	in CU1	"<<oldvalue_equiv<<std::endl;
				std::cout <<"counterr	"<<counterr<<std::endl;
				std::cout <<"i	"<<i<<std::endl;
				std::cout<<"cell_index1		"<<cell_index1<<std::endl;
				std::cout<<"ip	"<<ip<<std::endl;
				std::cout<<std::endl;
				eqv_strain_counter_ip++;
			}*/
			
			/*if (	cell_index1 > cell_map1.size()	)
			{	
				std::cout<<"cell_index1		"<<cell_index1<<std::endl;
			}
			std::cout<<"cell_index1		"<<cell_index1<<std::endl;*/
			//std::cout<<"cell_index2		"<<cell_index2<<std::endl;
			//std::cout<<"counterr		"<<counterr<<std::endl;
			
			
			//if (oldvalue_equiv[0] > 1.0)
			//{
				//std::cout<<"	oldvalueeps	after	"<<oldvalue_equiv;
			//}
		   
		   //constitutiveupdate2->eps_p_eq()->set_new_values(cell_index2, ip, oldvalue_equiv);   
		   
		   //std::cout<<"Plastic last 1 : 	"<<constituiveupdate1->plastic_last(cell_index1, ip)<<std::endl;
		   //const bool act = constituiveupdate1->get_plastic_last(cell_index1, ip);
		   //const bool act = constitutiveupdate2->plastic_last(cell_index2, ip);
/*
		ConstitutiveUpdate2->plastic_last()[cell_index2][ip] = constituiveupdate1->_plastic_last()[cell_index_1][ip];  // Will it access the plastic last status using _plastic_last() ?
		*/
		
		}
		//std::cout<<"cell_index1		"<<cell_index1<<std::endl;
		eqv_strain_counter = eqv_strain_counter + (eqv_strain_counter_ip/6);
		
		if (	cell_index1 > cell_map1.size()	)
			{	
				//std::cout<<"cell_index1		"<<cell_index1<<std::endl;
				cell_counter++;
			}
	if (cell_map1[cell_index1] != cell_map2[cell_index2])	
	{
		std::cout << "###########################################################################################################\n";
		std::cout << "The identity of the cells in previous and current domain does not match, please check the mapping algorithm\n";
		std::cout << "###########################################################################################################\n";
		
		std::cout<<"cell_map_id_1		"<<cell_map1[cell_index1]<<std::endl;
		std::cout<<"cell_map_id_2		"<<cell_map2[cell_index2]<<std::endl;
	}		
		
	eqv_strain_counter_ip = 0;
	++counterr;
 }
 std::cout<<"	eqv_strains greater than 1535	"<<eqv_strain_counter<<std::endl;
 std::cout<<"	cell_counter greater than 1535	"<<cell_counter<<std::endl;
 std::cout<<std::endl;
 
}


PYBIND11_MODULE(SIGNATURE,m)
{
m.def("transferHistory",&transferHistory);
}

//__all__ = ["transferHistory"]

//Should any sort of namespace used ? 
   
	
    
