__all__ = ["transferHistory"]

from dolfin import compile_cpp_code


cppcode = """
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>

#include <cmath>
#include <vector>
#include <memory>

#include <Eigen/Core>
#include <dolfin/function/GenericFunction.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>

#include <QuadratureFunction.h>

void transferHistory(const std::shared_ptr<fsm::ConstitutiveUpdate> & constituiveupdate1, // ConstitutiveUpdate for previous mesh
    const std::shared_ptr<fsm::ConstitutiveUpdate> & constitutiveupdate2, // ConstitutiveUpdate for new or evolved mesh
    const std::shared_ptr<const dolfin::MeshFunction<std::size_t>> & meshfunction1,  // marking for previous mesh
    const std::shared_ptr<const dolfin::MeshFunction<std::size_t>> & meshfunction2,  // marking for current evolved mesh
    const std::shared_ptr<const dolfin::Mesh> & meshfull) // full mesh which will be the end result of problem at last step
{

// performing the maping of history variables e.g. eps_p, eps_p_equiv and plastic_last then assigning to current step's constituiveupdate  
// using functionalities e.g. constituiveupdate1->_eps_p()->get_old_value(cell_index1, ip, oldvalueeps)
// and constitutiveupdate2->_eps_p()->set_new_values(cell_index2, ip,oldvalueeps)

}

PYBIND11_MODULE(SIGNATURE,m)
{
m.def("transferHistory",&transferHistory);
}
"""
import fsm

module_cpp=compile_cpp_code(cppcode,include_dirs=['/home/fenics/shared/fenics-solid-mechanics/fsm/src'])  #using path to fsm/src

def transferHistory(CU1, CU2, mf_1,mf_2, mesh):
   module_cpp.transferHistory(CU1,CU2,mf_1,mf_2,mesh)
