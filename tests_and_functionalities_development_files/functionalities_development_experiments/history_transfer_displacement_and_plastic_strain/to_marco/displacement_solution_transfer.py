def solution_transfer_function(mesh_Complete, half_cube_mesh, third_quarter_cube_mesh, u_half_cube, u_third_quarter_cube):
    # Builds cells mapping between parent meshes
    half = half_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()
    quarter = third_quarter_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()

    # Builds cells mapping betwen childs
    map = [j for i, c in enumerate(half) for j, d in enumerate(quarter) if c==d]
    
    # functional spaces (must be same as in the actual problem)
    V_half_cube = VectorFunctionSpace(half_cube_mesh, "Lagrange", 2)
    V_quarter_cube = VectorFunctionSpace(third_quarter_cube_mesh, "Lagrange", 2)
    
    # Get cell dofmaps
    half_dofmap = V_half_cube.dofmap()
    quarter_dofmap = V_quarter_cube.dofmap()

    # Assign values to functions for testint
    #u_half_cube.vector()[:] = np.random.rand(u_half_cube.vector()[:].size, )

    # Assing dofs to quarter
    for c in cells(half_cube_mesh):
      u_third_quarter_cube.vector()[quarter_dofmap.cell_dofs(map[c.index()])] = u_half_cube.vector()[half_dofmap.cell_dofs(c.index())]
    return u_third_quarter_cube
