from dolfin import compile_cpp_code

cppcode = """
#include <dolfin/function/Expression.h>
#include <pybind11/pybind11.h>
namespace py = pybind11;

class SupgStab : public dolfin::Expression {
public:
SupgStab(): dolfin::Expression()
{}
};

PYBIND11_MODULE(SIGNATURE, m)
{
    py::class_<SupgStab, std::shared_ptr<SupgStab>, dolfin::Expression>
    (m, "SupgStab")
    .def(py::init<>());
}
"""

compiled_module = compile_cpp_code(cppcode)
print("compiled")
