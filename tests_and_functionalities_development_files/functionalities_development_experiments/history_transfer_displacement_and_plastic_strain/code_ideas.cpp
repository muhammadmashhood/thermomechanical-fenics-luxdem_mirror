__all__ = ["tranferhist"]

cpp_code_transfer= """

#include pybdin...

void transferHistory(const ...share fsm::ConstituveUpdate & constituiveupdate1,
 constitutiveupdate2, meshfunction1, meshfunction2, meshfull){

// Mapping between 2 submesh
std::vector<std::size_t> vertex_map1,cell_map1;
  auto mapping = std::make_shared<MeshView>(meshfull,vertex_map1,cell_map1);
  auto submesh1 = std::make_shared<Mesh>(mapping->create(meshfunction1, 1));

std::vector<std::size_t> vertex_map2,cell_map2;
  auto mapping = std::make_shared<MeshView>(meshfull,vertex_map2,cell_map2);
  auto submesh2 = std::make_shared<Mesh>(mapping->create(meshfunction2, 1));
cell_map1=submesh1->cell_map();
cell_map2=submesh2->cell_map();

std::vector<int> mapping(cell_map1.size());
std::size_t counter=0
for (auto i : cell_map1) {
   //std::cout << i << " ";
   auto itr = std::find(cell_map2.begin(),cell_map2.end(),i);
   int index = std::distance(cell_map2.begin(), itr);
   std::cout <<index<<" "; 
   mapping[counter]=index;
   ++counter;
} // mapping between 2 global ID, submesh=> global
// mapping 1stmesh(smaller one)=> Index of cell  (not the ID) in Bigger one.


std::size_t counter=0;  //unsigned int
for(auto i:mapping){
	cell_index1=cell_map1[counter]; //1st mesh.
	cell_index2= i; 
	   
	Eigen::Matrix<double, 6, 1> oldvalueeps;
	Eigen::Matrix<double, 1, 1> oldvalue_equiv;

	strain_func1=ConstitutuveUpdate1->strain_func();
	num_ip_dof= strain_func1->element()->value_dimesiont(0);
	num_ip_per_cell = strain_func1->element()->space_dimension()/_num_ip_dofs;

	  
		for (std: ip) //86 line from Constitive update
		    ConstitutuveUpdate1->eps_p()->get_old_value(
			cell_index1, ip, oldvalueeps);
		   ConstitutuveUpdate2->eps_p()->set_new_values(
			cell_index2, ip,oldvalueeps);  
		    ConstitutuveUpdate1->eps_p_equiv()->get_old_value(
			cell_index1, ip, oldvalue_equiv);

		   ConstitutuveUpdate2->eps_p_equiv()->set_new_values(cell_index2, ip,oldvalue_equiv)   

		ConstitutiveUpdate2->plastic_last()[cell_index2][ip] = ConstitutuveUpdate1->plastic_last()[cell_index_1][ip];
	}
PYBIND11_MODULE(SIGNATURE,m)
{
m.def("transferHistory",&transferHistory);
}
"""

path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
module_cpp=compile_cpp_code(cpp_code_transfer,include_dir=[path,INCLUDEofFSM]) fsm.get_include()

def tranferhist(CU1, CU2, mf_1,mf_2, mesh):
   module_cpp.transferHistory(CU1,CU2,mf_1,mf_2,mesh)

	

when u instatiate the second constitutive update
1) call Update()
2) call history_update()
3) from pincopallo import tranferhist 

COnstitutive UpdateList=[]

COnstitutive UpdateList.append(ConstitutiveUpdateCurrent)
ConstitutiveUpdateCurrent

		
 
   
	
    
