
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>

#include <cmath>
#include <vector>
#include <memory>

#include <Eigen/Core>
#include <dolfin/function/GenericFunction.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>

#include <QuadratureFunction.h>
//#include <dolfin/function/Expression.h>
//#include <dolfin/mesh/MeshFunction.h>

namespace anba {

class WrappedQuadratureFunction : public dolfin::GenericFunction
{
private:
    const std::vector<std::shared_ptr<const dolfin::GenericFunction>> matsLibrary;
    const std::shared_ptr<const dolfin::MeshFunction<std::size_t>> material_id;

public:
    WrappedQuadratureFunction& operator=(WrappedQuadratureFunction&) = delete;  // Disallow copying
    WrappedQuadratureFunction(const WrappedQuadratureFunction&) = delete;

    // Constructor.
    WrappedQuadratureFunction(
    	const std::vector<std::shared_ptr<const dolfin::GenericFunction>> _matsLibrary, 
    	const std::shared_ptr<const dolfin::MeshFunction<std::size_t>> _material_id) : 
		matsLibrary(_matsLibrary),
		material_id(_material_id)
    {
        for (size_t i = 1; i < matsLibrary.size(); i++) {
	    if (matsLibrary[i-1]->value_rank() != matsLibrary[i]->value_rank()) 
                dolfin::error("WrappedQuadratureFunction value_rank inconsitency between material ", i-1, " and ", i);
	    auto vs1 = matsLibrary[i-1]->value_shape();
	    auto vs2 = matsLibrary[i-1]->value_shape();
            for (size_t j = 0; j < vs1.size(); j++) {
                if (vs1[j] != vs2[j]) 
                    dolfin::error("WrappedQuadratureFunction value_shape inconsitency between material ", i-1, " and ", i);
            }
	}
    }

    std::shared_ptr<const dolfin::FunctionSpace> function_space() const
    {
      return std::shared_ptr<const dolfin::FunctionSpace>(NULL);
    }

    std::size_t value_rank() const
    { return matsLibrary[0]->value_rank(); }

    std::size_t value_dimension(std::size_t i) const
    { return matsLibrary[0]->value_dimension(i); }

    std::vector<std::size_t> value_shape() const
    {
      return matsLibrary[0]->value_shape();
    }

    // Eval at every cell.
    void restrict(double* w,
		const dolfin::FiniteElement& element,
		const dolfin::Cell& cell,
		const double* vertex_coordinates,
		const ufc::cell& ufc_cell) const
    {
        size_t mat_id = (*material_id)[ufc_cell.index];
	return matsLibrary[mat_id]->restrict(w, element, cell, vertex_coordinates, ufc_cell);
    }
    
    void compute_vertex_values(std::vector<double>&,
                                               const dolfin::Mesh&) const
    {
        dolfin::error("QuadratureFunction::compute_vertex_values not implemented");
    }

//     std::shared_ptr<const dolfin::FiniteElement> element() const
//     { return matsLibrary[0]->element(); }

}; // class

} // namespace anba

//

PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);

PYBIND11_MODULE(SIGNATURE, m)
{
    pybind11::class_<anba::WrappedQuadratureFunction, std::shared_ptr<anba::WrappedQuadratureFunction>, dolfin::GenericFunction>
      (m, "WrappedQuadratureFunction", "Class to wrap different QuadratureFunctions", pybind11::multiple_inheritance())
      .def(pybind11::init<const std::vector<std::shared_ptr<const dolfin::GenericFunction>>, 
    	const std::shared_ptr<const dolfin::MeshFunction<std::size_t>>>(), "Create a WrappedQuadratureFunction")
      .def("function_space", &anba::WrappedQuadratureFunction::function_space)
      .def("value_rank", &anba::WrappedQuadratureFunction::value_rank)
      .def("value_dimension", &anba::WrappedQuadratureFunction::value_dimension)
      .def_property_readonly("value_shape", &anba::WrappedQuadratureFunction::value_shape)
      .def("restrict", &anba::WrappedQuadratureFunction::restrict)
      .def("compute_vertex_values", &anba::WrappedQuadratureFunction::compute_vertex_values);
//      .def("element", &anba::WrappedQuadratureFunction::element);
}
