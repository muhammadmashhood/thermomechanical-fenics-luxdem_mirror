from dolfin import *
from fenicstools.Probe import *

# Test the probe functions:
#set_log_level(20)

mesh = UnitCubeMesh(16, 16, 16)



W = FunctionSpace(mesh, 'CG', 1)

# Just create some random data to be used for probing
w0 = interpolate(Expression(('x[2]'), degree=2), W)

x = array([[1, 0.5, 0.5], [0.2, 0.3, 0.4], [0.8, 0.9, 1.0]])  # coordinates of the 3 points of probe location
p = Probes(x.flatten(), W)   # here the probe location or positions are given to the probes constructor
#x = x*0.9 
#p.add_positions(x.flatten(), W)  # adding 3 more points 

p(w0)		# assigning w0 field values to each given probe location in constructor 

print(p.array())
print ( p.dump("testarray") )
#print ( p.array(2, "testarray") )         # dump snapshot 2    # name the file with testarray_snapshot_2.npy
#print ( p.array(filename="testarray") )   # dump all snapshots
#print ( p.dump("testarray") )

