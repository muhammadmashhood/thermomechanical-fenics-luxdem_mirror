from dolfin import *
from fenicstools.Probe import *

# Test the probe functions:
set_log_level(20)

mesh = UnitCubeMesh(16, 16, 16)
#mesh = UnitSquareMesh(10, 10)

V = FiniteElement("CG", mesh.ufl_cell(), 1)
Vv  = VectorElement("CG", mesh.ufl_cell(), 1)
W = FunctionSpace(mesh, V * Vv)
'''
BDM = FunctionSpace(mesh, "BDM", 1)
DG = FunctionSpace(mesh, "DG", 0)
W = BDM * DG

V = FunctionSpace(mesh, 'CG', 1)
Vv = VectorFunctionSpace(mesh, 'CG', 1)
W = V*Vv
'''
# Just create some random data to be used for probing
w0 = interpolate(Expression(('x[0]', 'x[1]', 'x[2]', 'x[1]*x[2]'), degree=2), W)

x = array([[1.5, 0.5, 0.5], [0.2, 0.3, 0.4], [0.8, 0.9, 1.0]])  # coordinates of the 3 points of probe location
p = Probes(x.flatten(), W)   # here the probe location or positions are given to the probes constructor
#x = x*0.9 
#p.add_positions(x.flatten(), W)  # adding 3 more points 
for i in range(6):
    p(w0)		# assigning w0 field values to each given probe location in constructor 

print(p.array(100))
#print ( p.array(2, "testarray") )         # dump snapshot 2    # name the file with testarray_snapshot_2.npy
#print ( p.array(filename="testarray") )   # dump all snapshots
#print ( p.dump("testarray") )

