# using the maping functions for dipsplacement transfer
# checking if solver accepts initial solution without problem


import fsm

################ things for transfer history function #########################
import types
import os
import dolfin.cpp as cpp
from dolfin import compile_cpp_code
from dolfin import *

pwd = os.path.dirname(os.path.abspath(__file__))
with open(pwd + "/history_transfer.cpp", "r") as f:
    cpp_code_transfer_history = f.read()


path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
module_cpp = compile_cpp_code(cpp_code_transfer_history, include_dirs=['/usr/local/lib/python3.6/dist-packages/fsm-0.0.2-py3.6.egg/']) 

def transferHistory(J, CU1):
   module_cpp.transferHistory(J, CU1)
################################################################################


# Making complete mesh
mesh_Complete = UnitCubeMesh(8,8,8);
# defining the parameters for experiment of function
mark_sub_domain_1 = MeshFunction("size_t", mesh_Complete, mesh_Complete.topology().dim(), 0)
mark_sub_domain_2 = MeshFunction("size_t", mesh_Complete, mesh_Complete.topology().dim(), 0)

E = 20000.0;
nu = 0.3;
yield_stress = 9.0
E_t = 0.3*E
hardening_parameter = E_t/(1.0 - E_t/E)
print(hardening_parameter)
J2 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter)

V  = VectorFunctionSpace(mesh_Complete, "Lagrange", 2)
u = Function(V, name="u")
def eps(u):
        return as_vector([u[i].dx(i) for i in range(3)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1), (0, 2), (1, 2)]])

scheme = "default"
element_s = VectorElement("Quadrature", mesh_Complete.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
Qdef = fsm.UFLQuadratureFunction(eps(u), element_s, mesh_Complete)
fsm_constitutive_update = fsm.ConstitutiveUpdate(Qdef, J2)

print(fsm_constitutive_update)
transferHistory(J2, fsm_constitutive_update)
