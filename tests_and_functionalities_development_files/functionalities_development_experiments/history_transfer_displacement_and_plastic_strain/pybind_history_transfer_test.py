__all__ = ["transferHistory"]

from dolfin import compile_cpp_code


cppcode = """
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>

#include <cmath>
#include <vector>
#include <memory>

#include <Eigen/Core>
#include <dolfin/function/GenericFunction.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>

#include <QuadratureFunction.h>

void transferHistory(const std::shared_ptr<fsm::ConstitutiveUpdate> & constituiveupdate1,
	const std::shared_ptr<fsm::ConstitutiveUpdate> & constitutiveupdate2,
	const std::shared_ptr<const dolfin::MeshFunction<std::size_t>> & meshfunction1,
	const std::shared_ptr<const dolfin::MeshFunction<std::size_t>> & meshfunction2, // may be MeshFunction<std::size_t, .... other arguments also needed ? 
	const std::shared_ptr<const dolfin::Mesh> & meshfull) {

// Mapping between 2 submesh
std::vector<std::size_t> vertex_map1,cell_map1;
  auto mapping = std::make_shared<MeshView>(meshfull,vertex_map1,cell_map1);
  auto submesh1 = std::make_shared<Mesh>(mapping->create(meshfunction1, 1));

std::vector<std::size_t> vertex_map2,cell_map2;
  auto mapping = std::make_shared<MeshView>(meshfull,vertex_map2,cell_map2);
  auto submesh2 = std::make_shared<Mesh>(mapping->create(meshfunction2, 1));
cell_map1=submesh1->cell_map();
cell_map2=submesh2->cell_map();

std::vector<int> mapping(cell_map1.size());
std::size_t counter=0
for (auto i : cell_map1) {
   //std::cout << i << " ";
   auto itr = std::find(cell_map2.begin(),cell_map2.end(),i); // value of the iterator is returned as itr at location where cell_map1 is present in squence of cell_map2 
   int index = std::distance(cell_map2.begin(), itr); // the index gets the number of elements between cell_map2 and the itr position of cell_map2 where cell_map1 is located 
   std::cout <<index<<" "; 
   mapping[counter]=index; // the counter position of the mapping array stores the number of elements present between start of cell_map2 and place where cell_map1 (i) is present
   ++counter;
} // mapping between 2 global ID, submesh=> global
// mapping 1stmesh(smaller one)=> Index of cell  (not the ID) in Bigger one.


std::size_t counter=0;  //unsigned int
for(auto i:mapping){ // kind of iterating through each element

	// is cell_index1 and 2 int type?
	
	int cell_index1=cell_map1[counter]; //1st mesh.   // index of the first mesh cell at every mapping entry of length cell_map1
	int cell_index2= i; // index of the cell in mesh 2 which corresponds to the physical location cell of mesh 1 in this way the system knows that the current cell from mesh 1 in question has which location in mesh 2 to assign correctly the solution
	   
	Eigen::Matrix<double, 6, 1> oldvalueeps;  
	strain_p.setZero();	
	Eigen::Matrix<double, 1, 1> oldvalue_equiv;

	std::shared_ptr<const UFLQuadratureFunction> strain_func1=constituiveupdate1->_strain_func();  // strain_function with or without () or _strain_function ? 
	int num_ip_dofs= strain_func1->element()->value_dimension(0); 
	int num_ip_per_cell = strain_func1->element()->space_dimension()/num_ip_dofs; // any alternative for num_ip_per_cell

	  
		for (std::size_t ip = 0; ip < num_ip_per_cell; ip++) //151 line from Constitive update cpp
		{
		    constituiveupdate1->_eps_p()->get_old_value(
			cell_index1, ip, oldvalueeps);   // Will it access the plastic strain using _eps_p() ?
		   constitutiveupdate2->_eps_p()->set_new_values(
			cell_index2, ip,oldvalueeps);  
		    
			constituiveupdate1->_eps_p_equiv()->get_old_value(
			cell_index1, ip, oldvalue_equiv);
		   constitutiveupdate2->_eps_p_equiv()->set_new_values(cell_index2, ip,oldvalue_equiv)   

		ConstitutiveUpdate2->_plastic_last()[cell_index2][ip] = constituiveupdate1->_plastic_last()[cell_index_1][ip];  // Will it access the plastic last status using _plastic_last() ?
	}
	
	++counter;
 }
}

PYBIND11_MODULE(SIGNATURE,m)
{
m.def("transferHistory",&transferHistory);
}
"""
import fsm
#path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
#fsm.get_include()
module_cpp=compile_cpp_code(cppcode,include_dirs=['/home/fenics/shared/fenics-solid-mechanics/fsm/src']) 

def transferHistory(CU1, CU2, mf_1,mf_2, mesh):
   module_cpp.transferHistory(CU1,CU2,mf_1,mf_2,mesh)



#print(fsm.get_include())
#compiled_module = compile_cpp_code(cppcode, include_dirs=['/home/fenics/shared/fenics-solid-mechanics/fsm/src'])
#print("compiled")
