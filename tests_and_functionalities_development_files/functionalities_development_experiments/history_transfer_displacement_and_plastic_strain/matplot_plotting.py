from fenics import *
mesh = UnitSquareMesh(2, 2)

coordinates = mesh.coordinates()
V = FunctionSpace(mesh, 'P', 1)
u = interpolate(Expression('x[0] + x[1]', degree=1), V)

plot(mesh)
plot(u)

import matplotlib.pyplot as plt

#nodal_values = u.vector()
#plt.plot(nodal_values)   # for graph

plt.savefig("figure.png")
plt.show()

