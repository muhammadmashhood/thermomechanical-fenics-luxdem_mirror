from dolfin import *
mesh = UnitCubeMesh(4,4,4)
V0 = FunctionSpace(mesh, "DG", 0)
V1 = FunctionSpace(mesh, "DG", 1)
u0 = Function(V0)
u1 = Function(V1)
f = Expression("x[0]>0.5?1:0", degree=1)
u0.vector()[:] = project(f, V0).vector()
u1.vector()[:] = project(f, V1).vector()
file_res0 = XDMFFile('pippo0.xdmf')
file_res0.parameters['functions_share_mesh'] = True
file_res0.parameters['rewrite_function_mesh'] = False
file_res0.parameters["flush_output"] = True
file_res0.write(u0, t=0.)
file_res1 = XDMFFile('pippo1.xdmf')
file_res1.parameters['functions_share_mesh'] = True
file_res1.parameters['rewrite_function_mesh'] = False
file_res1.parameters["flush_output"] = True
file_res1.write(u1, t=0.)
