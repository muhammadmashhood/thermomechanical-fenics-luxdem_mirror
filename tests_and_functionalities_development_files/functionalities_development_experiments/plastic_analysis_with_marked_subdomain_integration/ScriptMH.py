
def map_function_from_submesh_to_mesh( V_l,submesh,  V):
    mesh = V.mesh()
    #submesh = V_l.mesh() the feeling internally it does Mesh(submesh)
    # map dof from submesh back to original mesh for a CG1 vector function
    map = vertex_to_dof_map(V)
    map_l = vertex_to_dof_map(V_l)
    mesh_to_mesh = submesh.data().array('parent_vertex_indices', 0).reshape(\
        int(map_l.shape[0] / 2), 1)
    mesh_to_mesh = concatenate((mesh_to_mesh * 2, \
        mesh_to_mesh * 2 + 1), axis = 1)
    mesh_to_mesh = mesh_to_mesh.flatten()
    return  map[mesh_to_mesh],map_l

def transferMap(submesh_old,submesh_new, uold, unew, Vcomplete):
    utemp = Function(Vcomplete)
    Vold  = uold.function_space()
    V_new  = unew.function_space()
    map0, map1 = map_function_from_submesh_to_mesh( V_old,submesh_old,  Vcomplete)
    utemp.vector()[map0]= uold.vector()[map1]
    map2, map3 = map_function_from_submesh_to_mesh( V_new,submesh_new,  Vcomplete)
    unew.vector()[map3]=  utemp.vector()[map2]
    return unew 


#    f.vector()[map[mesh_to_mesh]] = f_l.vector()[map_l]    
    

# before for loop
meshList=[]
marker_domains = [ HalfDomain(), QuartierDomain(), WholeDomain()]
for submesh_number in range(number_of_submesh):
    mark_domain= marker_domains[submesh_number]
    mark_sub_domain.set_all(0)
    mark_domain.mark(mark_sub_domain,1)
    mesh = SubMesh(mesh_Complete,mark_sub_domain,1)
    meshList.append(mesh)

us=[] # collect my displacement list 

Vcomplete =  VectorFunctionSpace(mesh_Complete , "Lagrange",2)

#In your loop in place of instatiate just u 
for submesh_number in range(number_of_submesh):
    if submesh_number==0:
        u = Function(V,name="displacement")
        us.list(u)
    else:
        u = Function(V,name="displacement")
        previousSpace = VectorFunctionSpace(meshList[submesh_numer-1],"Lagrange",2)
        uprev = us[submesh_numer-1]
        transferMap(meshList[submesh_numer-1],meshList[submesh_numer], uprev, u, Vcomplete)
        us.append(u)



