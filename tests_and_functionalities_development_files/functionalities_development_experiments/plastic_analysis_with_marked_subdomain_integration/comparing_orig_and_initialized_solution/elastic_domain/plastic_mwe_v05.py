# using the maping functions for dipsplacement transfer
# checking if solver accepts initial solution without problem

from dolfin import *
import fsm
#import pdb

#set_log_level(10)

class DirichletBoundaryX(SubDomain):
    def inside(self, x, on_boundary):
        return (x[0] < DOLFIN_EPS)
class DirichletBoundaryY(SubDomain):
    def inside(self, x, on_boundary):
        return (x[1] < DOLFIN_EPS) 
class DirichletBoundaryZ(SubDomain):
    def inside(self, x, on_boundary):
        return (x[2] < DOLFIN_EPS)

class PrescribedDisplacementY(SubDomain):
    def inside(self, x, on_boundary):
        return between( x[1], (1-DOLFIN_EPS, 1) )   

class HalfDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 0.5))
class QuarterDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 0.75))
class WholeDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 1))
        
def map_function_from_submesh_to_mesh( V_l,submesh,  V):
    mesh = V.mesh()
    submesh = V_l.mesh() #the feeling internally it does Mesh(submesh) (confirm)
    # map dof from submesh back to original mesh for a CG1 vector function
    #map = vertex_to_dof_map(V) #Return a map between vertex indices and dofs indices
    map = V.dofmap().dofs()
    map_l = vertex_to_dof_map(V_l) # std::vector<dolfin::la_index> vertex to dof map 
    mesh_to_mesh = submesh.data().array('parent_vertex_indices', 0).reshape(\
        int(map_l.shape[0] / 2), 1)
    mesh_to_mesh = concatenate((mesh_to_mesh * 2, \
        mesh_to_mesh * 2 + 1), axis = 1)
    mesh_to_mesh = mesh_to_mesh.flatten()
    return  map[mesh_to_mesh],map_l

def transferMap(submesh_old,submesh_new, uold, unew, Vcomplete):
    utemp = Function(Vcomplete)
    Vold  = uold.function_space()
    Vnew  = unew.function_space()
    map0, map1 = map_function_from_submesh_to_mesh( V_old,submesh_old,  Vcomplete)
    utemp.vector()[map0]= uold.vector()[map1]
    map2, map3 = map_function_from_submesh_to_mesh( V_new,submesh_new,  Vcomplete)
    unew.vector()[map3]=  utemp.vector()[map2]
    return unew 

def solution_transfer_function(mesh_Complete, half_cube_mesh, quarter_cube_mesh, u_half_cube, u_quarter_cube):
    # Builds cells mapping between parent meshes
    half = half_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()
    quarter = quarter_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()

    # Builds cells mapping betwen childs
    map = [j for i, c in enumerate(half) for j, d in enumerate(quarter) if c==d]
    
    # functional spaces (must be same as in the actual problem)
    V_half_cube = VectorFunctionSpace(half_cube_mesh, "Lagrange", 2)
    V_quarter_cube = VectorFunctionSpace(quarter_cube_mesh, "Lagrange", 2)
    
    # Get cell dofmaps
    half_dofmap = V_half_cube.dofmap()
    quarter_dofmap = V_quarter_cube.dofmap()

    # Assign values to functions for testint
    #u_half_cube.vector()[:] = np.random.rand(u_half_cube.vector()[:].size, )

    # Assing dofs to quarter
    for c in cells(half_cube_mesh):
      u_quarter_cube.vector()[quarter_dofmap.cell_dofs(map[c.index()])] = u_half_cube.vector()[half_dofmap.cell_dofs(c.index())]
    return u_quarter_cube

# making output file
'''file1 = XDMFFile('output.xdmf')      
file1.parameters['functions_share_mesh'] = True
file1.parameters['rewrite_function_mesh'] = False
file1.parameters["flush_output"] = True'''

# Making complete mesh
mesh_Complete = UnitCubeMesh(8,8,8);
V_Complete  = VectorFunctionSpace(mesh_Complete, "Lagrange", 2)

mark_sub_domain = MeshFunction("size_t", mesh_Complete, mesh_Complete.topology().dim(), 0)

number_of_submesh = 2#3

# marking the subdomain
for submesh_number in range(number_of_submesh):    
    if (submesh_number == 0) :
        mark_sub_domain.set_all(0)
        mark_domain = WholeDomain()#HalfDomain()
        mark_domain.mark(mark_sub_domain, 1)
        sub_mesh = MeshView.create(mark_sub_domain, 1)
        print('HalfDomain')
        
    elif (submesh_number == 1) :
        mark_sub_domain.set_all(0)
        mark_domain = WholeDomain()#QuarterDomain()
        mark_domain.mark(mark_sub_domain, 1)
        sub_mesh = MeshView.create(mark_sub_domain, 1)
        print('QuarterDomain')
        
        
    elif (submesh_number == 2) :
        mark_sub_domain.set_all(0)
        mark_domain = WholeDomain()
        mark_domain.mark(mark_sub_domain, 1)
        sub_mesh = MeshView.create(mark_sub_domain, 1)
        print('WholeDomain')
        
        
    mesh = sub_mesh
    #mesh = mesh_Complete
    
    E = 20000.0;
    nu = 0.3;

    scheme = "default"
    degree = 3
   
    #dx = Measure(("dx"),domain=mesh, subdomain_data=mark_sub_domain) # not sure if domain = mesh or domain = meshComplete
    #dx = dx(degree=degree, scheme=scheme)
    
    dx = Measure("dx")
    dx = dx(degree=degree, scheme=scheme)
    
    V  = VectorFunctionSpace(mesh, "Lagrange", 2)
    element_t = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=36, quad_scheme=scheme)
    Vt = FunctionSpace(mesh, element_t)
    element_s = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
    Vs = FunctionSpace(mesh, element_s)

    zero = Constant(0.0)
    prescribed_displacement = Expression(" 5e-3*t1",t1=0,degree=2)

    bc0 = DirichletBC(V.sub(0), zero, DirichletBoundaryY(), method="pointwise")
    bc1 = DirichletBC(V.sub(1), zero, DirichletBoundaryY(), method="pointwise")
    bc2 = DirichletBC(V.sub(2), zero, DirichletBoundaryY(), method="pointwise")
    bc3 = DirichletBC(V.sub(1), prescribed_displacement, PrescribedDisplacementY(), method="pointwise")

    bcs = [bc0, bc1, bc2, bc3]

    E_t = 0.3*E
    hardening_parameter = E_t/(1.0 - E_t/E)
    yield_stress = 1000#9.0

    u = Function(V, name="u")
    u_to_plot = Function(V, name="u_to_plot")

    def eps(u):
        return as_vector([u[i].dx(i) for i in range(3)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1), (0, 2), (1, 2)]])

    def sigma(s):
        #s = ss.function_space()
        return as_matrix([[s[0], s[3], s[4]], [s[3], s[1], s[5]], [s[4], s[5], s[2]]])

    def tangent(t):
        #t = tt.function_space()
        return as_matrix([[t[i*6 + j] for j in range(6)] for i in range(6)])

    J2 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter)
    Qdef = fsm.UFLQuadratureFunction(eps(u), element_s, mesh)
    fsm_constitutive_update = fsm.ConstitutiveUpdate(Qdef, J2)
        
    #fsm_tangent = QuadratureFunction(mesh, Vt.element(), fsm_constitutive_update, fsm_constitutive_update.w_tangent())
    #fsm_stress = QuadratureFunction(mesh, Vs.element(), fsm_constitutive_update.w_stress())
    fsm_tangent = fsm.QuadratureFunction(Vt, fsm_constitutive_update.w_tangent(), fsm_constitutive_update)
    fsm_stress  = fsm.QuadratureFunction(Vs, fsm_constitutive_update.w_stress())


    v = TestFunction(V)
    uTrial = TrialFunction(V)

    a = inner(eps(v), dot(tangent(fsm_tangent), eps(uTrial)) )*dx#(1) + (1e-90)*inner(eps(v), dot(tangent(fsm_tangent), eps(uTrial)) )*dx(0)
    L = inner(grad(v), sigma(fsm_stress))*dx#(1) + (1e-90)*inner(grad(v), sigma(fsm_stress))*dx(0)

    nonlinear_problem = fsm.PlasticityProblem(a, L, u, fsm_tangent, fsm_stress, bcs)

    nonlinear_solver = NewtonSolver()
    nonlinear_solver.parameters["convergence_criterion"] = "incremental";
    nonlinear_solver.parameters["maximum_iterations"]    = 50;
    nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
    nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;

    # File names for output

    eps_p_eq = fsm_constitutive_update.eps_p_eq()
    #fsm_constitutive_update.eps_p_eq().compute_mean(eps_eq);

    element_eps_p_eq_project = FiniteElement("CG", mesh.ufl_cell(), degree=1)
    V_eps_p_eq_project = FunctionSpace(mesh, element_eps_p_eq_project)
    eps_p_eq_project = Function(V_eps_p_eq_project, name="eps_p_eq")
    
    if (submesh_number > 0):  # because first subdomain gets everything from the start as default
         
        u = solution_transfer_function(mesh_Complete, mesh_old, mesh, u_old, u)
        #u = u_to_plot
        # Write output to files from last submesh
        #xdmff = XDMFFile("u_to_plot/u_to_plot" + format(submesh_number, '04') + ".xdmf")    
        #xdmff.write(u_to_plot);
        #xdmff.close()
        
    if (submesh_number == 0):
        prescribed_displacement.t1 = 0.5
    else:
        prescribed_displacement.t1 = 1
    
        
    # Solve non-linear problem
    nonlinear_solver.solve(nonlinear_problem.cpp_object(), u.vector());
    
    #pdb.set_trace()
    
    # Update variables
    fsm_constitutive_update.update_history();

    #print(fsm_constitutive_update.eps_p_eq().old_data())

    # Write output to files from last submesh 
    
    file1_u = XDMFFile("output/output_u" + format(submesh_number, '04') + ".xdmf")
    
    '''file1.parameters['functions_share_mesh'] = False
    file1.parameters['rewrite_function_mesh'] = False
    file1.parameters["flush_output"] = False'''
    
    file1_u.write(u, t=float(submesh_number));
    file1_u.close()
    
    assign( eps_p_eq_project, project(eps_p_eq, V_eps_p_eq_project, solver_type='gmres',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )
    #previous subdomain solution:
    eps_p_eq_old = eps_p_eq
    u_old = u
    V_old = V
    mesh_old = mesh
    
    file1_eqv_eps_p = XDMFFile("output/output_eqv_eps_p" + format(submesh_number, '04') + ".xdmf")
    file1_eqv_eps_p.write(eps_p_eq_project, t=float(submesh_number));
    file1_eqv_eps_p.close()