import numpy as np
from dolfin import *

class HalfDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 0.5))
class QuarterDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 0.75))

mesh_Complete = UnitCubeMesh(18,18,18);
mark_sub_domain = MeshFunction("size_t", mesh_Complete, mesh_Complete.topology().dim(), 0)

mark_sub_domain.set_all(0)
mark_domain = HalfDomain()
mark_domain.mark(mark_sub_domain, 1)
half_cube_mesh = MeshView.create(mark_sub_domain, 1)

mark_sub_domain.set_all(0)
mark_domain = QuarterDomain()
mark_domain.mark(mark_sub_domain, 1)
quarter_cube_mesh = MeshView.create(mark_sub_domain, 1)

V_half_cube  = VectorFunctionSpace(half_cube_mesh, "Lagrange", 2)
u_half_cube = Function(V_half_cube, name="u_half")

##############################################################################
# here solver performs the solution on the half cube and returns the solution vector u_half_cube #
##############################################################################

V_quarter_cube =  VectorFunctionSpace(quarter_cube_mesh, "Lagrange", 2)
u_quarter_cube = Function(V_quarter_cube, name="u_quarter")

# Builds cells mapping between parent meshes
half = half_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()
quarter = quarter_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()

# Builds cells mapping betwen childs
map = [j for i, c in enumerate(half) for j, d in enumerate(quarter) if c==d]

# Get cell dofmaps
half_dofmap = V_half_cube.dofmap()
quarter_dofmap = V_quarter_cube.dofmap()

# Assign values to functions for testint
u_half_cube.vector()[:] = np.random.rand(u_half_cube.vector()[:].size, )

# Assing dofs to quarter
for c in cells(half_cube_mesh):
  u_quarter_cube.vector()[quarter_dofmap.cell_dofs(map[c.index()])] = u_half_cube.vector()[half_dofmap.cell_dofs(c.index())]

File('u_half.pvd') << u_half_cube
File('u_quarter.pvd') << u_quarter_cube

########################################################
# now the u_half_cube has to be transferred to the u_quarter_cube #
# before starting the solver for quarter_cube_mesh #
#########################################################