from dolfin import *
import fsm
import sys
class DirichletBoundaryY(SubDomain):
    def inside(self, x, on_boundary):
        return (x[1] < DOLFIN_EPS)

mesh = UnitCubeMesh(20, 20, 20)#Mesh(sys.argv[1])#
print(type(mesh))

mxf = XDMFFile("mesh1.xdmf")
#mxf.write(mesh)
mxf.close()

scheme = "default"
degree = 3
dx = Measure("dx")
dx = dx(degree=degree, scheme=scheme)
V  = VectorFunctionSpace(mesh, "Lagrange", 2)
element_t = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=36, quad_scheme=scheme)
Vt = FunctionSpace(mesh, element_t)
element_s = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
Vs = FunctionSpace(mesh, element_s)
zero = Constant(0.0)
bc0 = DirichletBC(V.sub(0), zero, DirichletBoundaryY(), method="pointwise")
bc1 = DirichletBC(V.sub(1), zero, DirichletBoundaryY(), method="pointwise")
bc2 = DirichletBC(V.sub(2), zero, DirichletBoundaryY(), method="pointwise")
bcs = [bc0, bc1, bc2]
u = Function(V, name="u")
def eps(u):
    return as_vector([u[i].dx(i) for i in range(3)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1), (0, 2), (1, 2)]])
Qdef = fsm.UFLQuadratureFunction(eps(u), element_s, mesh) # representation?
# lists 
fsm_constitutive_update_list = []
eps_p_eq_list = []
for cell in cells(mesh):
    nu = 0.3
    yield_stress = 9e6
    E = 5e11 
    E_t = 0.3*E
    hardening_parameter = E_t/(1.0 - E_t/E) 
    #J2_1 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter) # .self ?
    J2_1 = fsm.fsm_cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter)
    fsm_constitutive_update_1 = fsm.ConstitutiveUpdate(Qdef, J2_1)
    fsm_constitutive_update_list.append(fsm_constitutive_update_1)
    print("appending eps_p_eq():", cell, mesh.num_cells())
    eps_p_eq_list.append( fsm_constitutive_update_1.eps_p_eq() )
