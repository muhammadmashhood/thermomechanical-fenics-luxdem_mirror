# material properties ( chosen around 800 deg C for temp range of 25 - 1644 deg C )

#THERMO-MECHANICAL MODEL DEVELOPMENT AND EXPERIMENTAL VALIDATION FOR METALLIC PARTS IN ADDITIVE MANUFACTURING
E = 20000#65.25E9; 
E_t = 0.3*E
hardening_parameter = E_t/(1.0 - E_t/E)
yield_stress = 9#438E6 
expansion_coeff_value = 10e-6#9.7E-6  

#Process Modeling and Validation of Powder Bed Metal Additive Manufacturing
nu = 0.3#0.41;  

reference_temperature = 0#1644.5946044921875#681.0755615234375
n_initial = 15#35 # initial geometry to read from G_code mesh


