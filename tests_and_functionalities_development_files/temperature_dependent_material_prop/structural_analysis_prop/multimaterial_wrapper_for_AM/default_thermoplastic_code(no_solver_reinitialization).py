# modified to work with the docker image's fsm
from dolfin import *
import fsm
import sys
# multi-material wrapper
import multi_material_wrapper as mmw
#set_log_level(10)

Time = 0.


class DirichletBoundaryXY_Top(SubDomain):
    def inside(self, x, on_boundary):
        return abs(x[2] - 1) < DOLFIN_EPS
class DirichletBoundaryYZ_Left(SubDomain):
    def inside(self, x, on_boundary):
        return abs(x[0] - 1) < DOLFIN_EPS
class DirichletBoundaryZX_front(SubDomain):
    def inside(self, x, on_boundary):
        return abs(x[1] - 1) < DOLFIN_EPS

class DirichletBoundaryX(SubDomain):
    def inside(self, x, on_boundary):
        return (x[0] < DOLFIN_EPS)
class DirichletBoundaryY(SubDomain):
    def inside(self, x, on_boundary):
        return (x[1] < DOLFIN_EPS)
class DirichletBoundaryZ(SubDomain):
    def inside(self, x, on_boundary):
        return (x[2] < DOLFIN_EPS)
      


class WholeDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 1))
        
mesh = BoxMesh(Point(0.0, 0.0, 0.0), Point(1, 1, 1), 4, 4, 4)#UnitCubeMesh(7,7,7);
#mesh = Mesh(sys.argv[1])
E = 20000.0;
nu = 0.3;


####################### write mesh info for mesh and temperature solution (h5) #######################
#meshinfo= HDF5File(mesh.mpi_comm(),'tempProfile/non_evol_unit_cube.h5', 'w')
#mark_sub_domain = MeshFunction("size_t", mesh, mesh.topology().dim(), 0)


nTimeSteps = 2

scheme = "default"
degree = 3
dx = Measure("dx")
dx = dx(degree=degree, scheme=scheme)

V  = VectorFunctionSpace(mesh, "Lagrange", 2)
element_t = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=36, quad_scheme=scheme)
Vt = FunctionSpace(mesh, element_t)
element_s = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
Vs = FunctionSpace(mesh, element_s)

T  = FunctionSpace(mesh, "Lagrange", 1)
zero = Constant(0.0)


bc0 = DirichletBC(V.sub(0), zero, DirichletBoundaryY(), method="pointwise")
bc1 = DirichletBC(V.sub(1), zero, DirichletBoundaryY(), method="pointwise")
bc2 = DirichletBC(V.sub(2), zero, DirichletBoundaryY(), method="pointwise")
bc3 = DirichletBC(V.sub(0), zero, DirichletBoundaryZX_front(), method="pointwise")
bc4 = DirichletBC(V.sub(1), zero, DirichletBoundaryZX_front(), method="pointwise")
bc5 = DirichletBC(V.sub(2), zero, DirichletBoundaryZX_front(), method="pointwise")

bcs = [bc0, bc1, bc2]
#bcs = [bc0, bc1, bc2, bc3, bc4, bc5]

E_t = 0.3*E
hardening_parameter = E_t/(1.0 - E_t/E)
yield_stress = 9.0

u = Function(V, name="u")
t = Function(T, name="T")

temp_exp = Expression(" 500*x[1]*x[1]*t1",t1=0,degree=2)
#temp_exp = Expression(" t1*500 ",t1=0,degree=2)
#t = temp_exp

# assigning the temperature profile based on expression
temp_exp.t1 = 1
t.assign(interpolate(temp_exp,T))
#t = project(temp_exp, T)

def mech_eps(u):
    return as_vector([u[i].dx(i) for i in range(3)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1), (0, 2), (1, 2)]])

def thermal_eps(t):
    expansion_coeff = Constant(10.E-6)
    #expansion_coeff = Expression(" 0.0019*temperature + 8.6 ",temperature=0,degree=2) #Constant(10.E-6)   interpolation for {{0,8.6},{500,9.55}}
    #expansion_coeff.temperature = 1
    
    #print(expansion_coeff)
    return expansion_coeff * as_vector([1., 1., 1., 0., 0., 0.]) * t  

def eps(u, t):
    return mech_eps(u) - thermal_eps(t)

def sigma(s):
    #s = ss.function_space()
    return as_matrix([[s[0], s[3], s[4]], [s[3], s[1], s[5]], [s[4], s[5], s[2]]])

def tangent(t):
    #t = tt.function_space()
    return as_matrix([[t[i*6 + j] for j in range(6)] for i in range(6)])


Qdef = fsm.UFLQuadratureFunction(eps(u, t), element_s, mesh) # representation?
# multi-material wrapper

# description of marker based upon the temperature or temperature range
materials = MeshFunction("size_t", mesh, mesh.topology().dim())
materials.set_all(0)
#subdomain_1 = CompiledSubDomain("x[1] > 0.5")
#subdomain_1.mark(materials, 1)


# assigning the markers 

# writting the DG temperature field
file_t = XDMFFile('output/t.xdmf')
file_t.parameters['functions_share_mesh'] = True
file_t.parameters['rewrite_function_mesh'] = False
file_t.parameters["flush_output"] = True
file_t.write(t, t=float(0));

T_dg = FunctionSpace(mesh, "DG", 0)
t_dg = project(t, T_dg)   # to make averaging on the cell and to keep basically no. temp field entities = no. mesh cell, projecting the CG temperature field upon the DG 0 and each cell suppose to have mark based on own temperature value 

# writting the DG temperature field
file_t_dg = XDMFFile('output/t_dg.xdmf')
file_t_dg.parameters['functions_share_mesh'] = True
file_t_dg.parameters['rewrite_function_mesh'] = False
file_t_dg.parameters["flush_output"] = True
file_t_dg.write(t_dg, t=float(0));


# lists to be assigned to multi-material wrapper
fsm_tangent_list = [] 
fsm_stress_list = []
fsm_constitutive_update_list = []
eps_p_eq_list = []

if len(t_dg.vector()) != mesh.num_cells():
    raise ValueError("make sure that the number of entities for cell temperature field projected on DG0 is equal to number of cells in current mesh")

cell_counter = 0    
for cell in cells(mesh):
    index = cell.index()
    #print(index)
    #print(len(t_dg.vector()))
    #print(t_dg.vector()[index])
    #if t_dg.vector()[index] > 49.8:   # is the location of value of concerned cell temperature same as the location of t[cell_index]?
    materials[index] = index          # giving label to the particular cell
    
    # temperature dependent material properties expression
    nu = 0.33 + 0.0001 * t_dg.vector()[index] #0.3 interpolation {{0,0.33},{500,0.38}} x-axis is temperature
    yield_stress = 9 - 0.0162 * t_dg.vector()[index] #9   interpolation {{0,50000},{500,25000}} x-axis is temperature
    E = 50000 - 50 * t_dg.vector()[index] #E = 20000   interpolation {{0,9},{500,0.9}} x-axis is temperature
    
    E_t = 0.3*E
    hardening_parameter = E_t/(1.0 - E_t/E) 
    
    # making the fsm components for each mesh cell or other words each temp cell region
    J2_1 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter) # .self ?
    fsm_constitutive_update_1 = fsm.ConstitutiveUpdate(Qdef, J2_1)
    
    fsm_tangent_1 = fsm.QuadratureFunction(Vt, fsm_constitutive_update_1.w_tangent(), fsm_constitutive_update_1)
    fsm_stress_1 = fsm.QuadratureFunction(Vs, fsm_constitutive_update_1.w_stress()) #, fsm_constitutive_update_1)?
    # updating the list for each cell based upon its DG averaged temperature value of material property 
    fsm_constitutive_update_list.append(fsm_constitutive_update_1)
    print("appending eps_p_eq():", cell, mesh.num_cells())
    eps_p_eq_list.append( fsm_constitutive_update_1.eps_p_eq() )
    fsm_tangent_list.append(fsm_tangent_1)
    fsm_stress_list.append(fsm_stress_1)
    
# writing the markers
material_marks_file = File("output/material_marks.pvd")
material_marks_file << materials


#J2_1 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter) # .self ?
#J2 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter)
#fsm_constitutive_update_1 = fsm.ConstitutiveUpdate(Qdef, J2_1)
#fsm_constitutive_update = fsm.ConstitutiveUpdate(Qdef, J2)
#fsm_tangent_1 = fsm.QuadratureFunction(Vt, fsm_constitutive_update_1.w_tangent(), fsm_constitutive_update_1)
#fsm_stress_1 = fsm.QuadratureFunction(Vs, fsm_constitutive_update_1.w_stress()) #, fsm_constitutive_update_1)?


#J2_2 = fsm.python.cpp.plasticity_model.VonMises(E/10, nu/1, yield_stress/1, hardening_parameter/1) # .self ?
#fsm_constitutive_update_2 = fsm.ConstitutiveUpdate(Qdef, J2_2)
#fsm_tangent_2 = fsm.QuadratureFunction(Vt, fsm_constitutive_update_2.w_tangent(), fsm_constitutive_update_2)
#fsm_stress_2 = fsm.QuadratureFunction(Vs, fsm_constitutive_update_2.w_stress())#, fsm_constitutive_update_2)?

#fsm_tangent = QuadratureFunction(mesh, Vt.element(), fsm_constitutive_update, fsm_constitutive_update.w_tangent())
#fsm_stress = QuadratureFunction(mesh, Vs.element(), fsm_constitutive_update.w_stress())

#fsm_tangent = mmw.MultiMaterialWrapper([fsm_tangent_1, fsm_tangent_2], materials)
#fsm_stress = mmw.MultiMaterialWrapper([fsm_stress_1, fsm_stress_2], materials)

fsm_tangent = mmw.MultiMaterialWrapper(fsm_tangent_list, materials)
fsm_stress = mmw.MultiMaterialWrapper(fsm_stress_list, materials)

#fsm_tangent = fsm.QuadratureFunction(Vt, fsm_constitutive_update.w_tangent(), fsm_constitutive_update)
#fsm_stress  = fsm.QuadratureFunction(Vs, fsm_constitutive_update.w_stress())

#fsm_constitutive_update = [fsm_constitutive_update_1, fsm_constitutive_update_2]
#eps_p_eq = mmw.MultiMaterialWrapper([fsm_constitutive_update_1.eps_p_eq(), fsm_constitutive_update_2.eps_p_eq()], materials)
#eps_p = mmw.MultiMaterialWrapper([fsm_constitutive_update_1.eps_p(), fsm_constitutive_update_2.eps_p()], materials)
eps_p_eq = mmw.MultiMaterialWrapper(eps_p_eq_list, materials)
     
v = TestFunction(V)
uTrial = TrialFunction(V)

a = inner(mech_eps(v), dot(tangent(fsm_tangent), mech_eps(uTrial)) )*dx
L = inner(grad(v), sigma(fsm_stress))*dx

#nonlinear_problem = fsm.PlasticityProblem(a, L, u, fsm_tangent_1, fsm_stress_1, bcs)
nonlinear_problem = fsm.PlasticityProblem(a, L, u, fsm_tangent_list[100], fsm_stress_list[100], bcs)


nonlinear_solver = NewtonSolver()
nonlinear_solver.parameters["convergence_criterion"] = "incremental";
nonlinear_solver.parameters["maximum_iterations"]    = 50;
nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;

# File names for output

#eps_p_eq = fsm_constitutive_update.eps_p_eq()
#fsm_constitutive_update.eps_p_eq().compute_mean(eps_eq);

file1 = XDMFFile('output/output.xdmf')
file1.parameters['functions_share_mesh'] = True
file1.parameters['rewrite_function_mesh'] = False
file1.parameters["flush_output"] = True
#File("variable_temp_250_x_4_elem/my_mesh.xml") << mesh

element_eps_p_eq_project = FiniteElement("DG", mesh.ufl_cell(), degree=1)
V_eps_p_eq_project = FunctionSpace(mesh, element_eps_p_eq_project)
eps_p_eq_project = Function(V_eps_p_eq_project, name="eps_p_eq")



step = 0
for step in range(nTimeSteps):
    print("step begin: ", step)
    
    #temp_exp.T=step
    #t.vector()[:] = 10. * step
    if step == 1:
        temp_exp.t1 = 0
        t.assign(interpolate(temp_exp,T))
    
    #t = interpolate(temp_exp,T)
    
 

    # Solve non-linear problem
    nonlinear_solver.solve(nonlinear_problem.cpp_object(), u.vector());

    # Update variables
    # As the update of fsm_constitutive_update makes all current plastified quadrature points, eps_p_eqv and eps_p as the old one for next load step so it is necessary to update it for each constitutive update for correct and realist next loading or inloading step
    
    for i in range(len(fsm_constitutive_update_list)):
        fsm_constitutive_update_list[i].update_history();
    

    # Write output to files
    file1.write(u, t=float(step));
    eps_p_eq_project.vector()[:] = project(eps_p_eq, V_eps_p_eq_project, solver_type='gmres',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ).vector()
    file1.write(eps_p_eq_project, t=float(step));
    file1.write(t, t=float(step));
    
    # marking the domain to export to h5 file
    #mark_sub_domain.set_all(0)
    #mark_domain = WholeDomain()
    #mark_domain.mark(mark_sub_domain, 1)

    ### exporting marking and subdomain mesh ###
    #meshinfo.write(mesh,"/submesh/{}_part".format(step))
    #meshinfo.write(mark_sub_domain,"/meshfunction/{}_part".format(step)) # cell marker
    
#meshinfo.close()

