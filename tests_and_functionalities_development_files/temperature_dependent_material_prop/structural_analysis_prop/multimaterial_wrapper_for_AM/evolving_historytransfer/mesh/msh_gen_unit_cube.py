

from dolfin import *




#mesh = BoxMesh(0.0, 0.0, 0.0, 10.0, 4.0, 2.0, 10, 10, 10)
#mesh = BoxMesh( Point(0.0, 0.0, 0.0), Point(0.5e-3, 0.5e-3, 0.5e-3), 20, 20, 20)
mesh = UnitCubeMesh(6,6,6)
print ("Plotting a BoxMesh")
plot(mesh, title="Box")

#mesh_file = File("small_cube_20_elem_0_0005_box_mesh.xml")
mesh_file = File("unit_cube_6_elem.xml")
mesh_file << mesh

#mxf = XDMFFile("small_cube_20_elem_0_0005_box_mesh.xdmf")
mxf = XDMFFile("unit_cube_6_elem.xdmf")
mxf.write(mesh)
mxf.close()

'''

domain_total = Rectangle(Point(0., 0.), Point(1, 1))

meshres = 1
print("mesh resolution: ", meshres)
mesh_final = generate_mesh(domain_total, meshres)

mesh_file = File("mesh.xml")
mesh_file << mesh_final'''
