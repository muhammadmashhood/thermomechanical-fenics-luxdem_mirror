//include pybdin...
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>

#include <cmath>
#include <vector>
#include <memory>

#include <algorithm>
#include <iterator>
#include <string_view>
#include <iostream>

#include <Eigen/Core>
/*#include <dolfin/function/GenericFunction.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/mesh/Cell.h>*/
#include <dolfin.h>

#include "/usr/local/lib/python3.6/dist-packages/fsm-0.0.2-py3.6.egg/FenicsSolidMechanics.h"

void transferHistory (const std::shared_ptr<fsm::ConstitutiveUpdate> & constituiveupdate1,
	const std::shared_ptr<fsm::ConstitutiveUpdate> & constitutiveupdate2,
	std::vector<std::size_t> cell_map1,
	std::vector<std::size_t> cell_map2)  // may be MeshFunction<std::size_t, .... other arguments also needed ? 
{ 
std::cout<<"#############################"<<std::endl;
//std::cout<<J2->hardening_parameter(0.1)<<std::endl;
std::cout<<"Hi there I am in the function"<<std::endl;
std::cout<<"constituiveupdate1()	"<< typeid ( constituiveupdate1 ).name() <<std::endl;
std::cout<<"constitutiveupdate2()	"<< typeid ( constitutiveupdate2 ).name() <<std::endl;
//std::cout<<"meshfunction1()"<< typeid ( meshfunction1 ).name() <<std::endl;
//std::cout<<"*meshfunction1()"<< typeid ( *meshfunction1 ).name() <<std::endl;
//std::cout<<"meshfunction2()"<< typeid ( meshfunction2 ).name() <<std::endl;
//std::cout<<"meshfull()"<< typeid ( meshfull ).name() <<std::endl;
std::cout<<"cell_map1	"<< typeid ( cell_map1 ).name() <<std::endl;
std::cout<<"cell_map2	"<< typeid ( cell_map2 ).name() <<std::endl;
//std::cout<<"cell_map1[500]		"<< cell_map1[500] <<std::endl;
//std::cout<<"cell_map1.size()	"<< cell_map1 <<std::endl;
std::cout<<"cell_map1.size()	"<< cell_map1.size() <<std::endl;
std::cout<<"cell_map2.size()	"<< cell_map2.size() <<std::endl;
std::cout<<"#############################"<<std::endl;


/*
// Mapping between 2 submesh
std::vector<std::size_t> vertex_map1,cell_map1;
auto mapping1 = std::make_shared<dolfin::MeshView>(meshfull,vertex_map1,cell_map1);
auto submesh1 = std::make_shared<dolfin::Mesh>(mapping1->create(*meshfunction1, 1));

int num_cells1 = submesh1->num_cells();
std::cout<<"num_cells1	"<< num_cells1 <<std::endl;
//std::vector<std::size_t> cell_map1;  // alternative of original idea


std::vector<std::size_t> vertex_map2,cell_map2;
auto mapping2 = std::make_shared<dolfin::MeshView>(meshfull,vertex_map2,cell_map2);
auto submesh2 = std::make_shared<dolfin::Mesh>(mapping2->create(*meshfunction2, 1));

int num_cells2 = submesh2->num_cells();
std::cout<<"num_cells2	"<< num_cells2 <<std::endl;
//std::vector<std::size_t> cell_map2; // alternative of original idea



cell_map1=mapping1->cell_map();   // check if dolfin::Mesh can support this function otherwise MeshView explore
cell_map2=mapping2->cell_map();    //cell_map2St6vectorImSaImEE
*/



std::vector<int> mapping(cell_map1.size());
std::size_t counter=0;
for (auto i : cell_map1) {
   auto itr = std::find(cell_map2.begin(),cell_map2.end(),i); // value of the iterator is returned as itr at location where cell_map1 is present in squence of cell_map2 
	if ( itr != cell_map2.end() ){
		//std::cout <<"itr:	"<< *itr <<std::endl;
		}
    else{
		std::cout << "#################################################################\n";
		std::cout << "Element from previous subdomain is not found in current subdomain\n";
		std::cout << "#################################################################\n";
		}
		
   int index = std::distance(cell_map2.begin(), itr); // the index gets the number of elements between cell_map2 and the itr position of cell_map2 where cell_map1 is located 
   mapping[counter]=index; // the counter position of the mapping array stores the number of elements present between start of cell_map2 and place where cell_map1 (i) is present
   //std::cout <<"		"<<mapping[counter];
   //std::cout <<"counter	"<<counter<<std::endl;
   ++counter;
   
   
}
// mapping between 2 global ID, submesh=> global
// mapping 1stmesh(smaller one)=> Index of cell  (not the ID) in Bigger one.

std::cout <<std::endl;



//////////////////////////// set subtraction for cell index of evolved domain zone //////////////////

std::vector<int> remain_from_mapping(cell_map2.size() - cell_map1.size());
std::vector<int> cell_map2_array (cell_map2.size());
std::cout <<"differ"<<cell_map2.size() - cell_map1.size()<<std::endl;
std::size_t counterr_remain = 0;

for (int i=0; i<cell_map2.size(); i++)
	{
		cell_map2_array[i] = i;
		//std::cout<<"	"<<cell_map2_array[i];
	}
 
for (int j=0; j<cell_map2.size(); j++)
	{
		if ( not (std::any_of(std::begin(mapping), std::end(mapping), [=](int n){return n == j;})) )
			{
				remain_from_mapping[counterr_remain] = j;  // check why values not being assigned? logic seems correct
				//std::cout <<"		"<<remain_from_mapping[counterr_remain];
				counterr_remain++;
			}
	}
std::cout <<std::endl;
std::cout<<"	remain_from_mapping.size()	"<<remain_from_mapping.size()<<std::endl;
std::cout<<"	counterr_remain	"<<counterr_remain<<std::endl;
std::cout <<std::endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//std::cout <<std::endl;
//std::cout<<"	mapping	"<<mapping<<std::endl;
print(mapping);

std::size_t counterr=0;  //unsigned int

for(auto i:mapping){ // kind of iterating through each element

	// is cell_index1 and 2 int type?
	
	//const std::size_t cell_index1 = cell_map1[counterr]; //1st mesh.   // index of the first mesh cell at every mapping entry of length cell_map1
	const std::size_t cell_index1 = counterr;
	const std::size_t cell_index2 = i; // index of the cell in mesh 2 which corresponds to the physical location cell of mesh 1 in this way the system knows that the current cell from mesh 1 in question has which location in mesh 2 to assign correctly the solution
	
	
	
	Eigen::Matrix<double, 6, 1> oldvalueeps;
	//strain_p.setZero(); // set the plastic strain first zero for constitutiveupdate2 as zero	
	Eigen::Matrix<double, 1, 1> oldvalue_equiv;	
	
	//strain_func1 = constituiveupdate1->strain_func();  // strain_function with or without () or _strain_function ? 
	//int num_ip_dofs = strain_func1->element()->value_dimension(0); 
	//int num_ip_per_cell = strain_func1->element()->space_dimension()/num_ip_dofs; // any alternative for num_ip_per_cell
	
	std::size_t num_ip_per_cell = constituiveupdate1->num_ip_per_cell();
	//std::cout <<"num_ip_per_cell	"<<num_ip_per_cell<<std::endl;
	
		for (std::size_t ip = 0; ip < num_ip_per_cell; ip++) //151 line from Constitive update cpp
		{
			// getting and updating the equivalent plastic strain values
		
		    constituiveupdate1->eps_p()->get_old_values(
			cell_index1, ip, oldvalueeps);   // Will it access the plastic strain using _eps_p() ?
			
			constitutiveupdate2->update_eps_p(cell_index2, ip, oldvalueeps);   
			
			// getting and updating the plastic strain values
			
			constituiveupdate1->eps_p_eq()->get_old_values(cell_index1, ip, oldvalue_equiv);
			
//std::cout <<"cell_index	"<<cell_index1<<"	ip	"<<ip<<"	eps_p_eq in CU1:		"<<oldvalue_equiv<<std::endl;
			
			//oldvalue_equiv = 2*oldvalue_equiv;
			constitutiveupdate2->update_eps_p_eq(cell_index2, ip, oldvalue_equiv);  
		    /*
			Eigen::Matrix<double, 1, 1> oldvalue_equiv_to_verify;	
			constitutiveupdate2->eps_p_eq()->get_old_values(cell_index2, ip, oldvalue_equiv_to_verify);
			std::cout <<"Updated eps_p_eq in CU2:	"<<oldvalue_equiv_to_verify;*/
			
			/*
			//check of eqv_plastic_strain if they are same or not in both constitutive updates but should be invoked after the update_history() is applied after transfer of history
			if(oldvalue_equiv != oldvalue_equiv_to_verify)
		   {   std::cout <<counterr<<std::endl;
			   std::cout << "###########################################################################################################\n";
			   std::cout << "Oooops issue there\n";
			   std::cout << "###########################################################################################################\n";
		   }*/
			
			
			/*
			//check of plastic last falg if they are same or not in both constitutive updates but should be invoked after the update_history() is applied after transfer of history
			if(constituiveupdate1->get_plastic_last(cell_index1, ip)!= constitutiveupdate2->get_plastic_last(cell_index2, ip))
		   {   std::cout <<counterr<<std::endl;
			   std::cout << "###########################################################################################################\n";
			   std::cout << "Oooops issue there\n";
			   std::cout << "###########################################################################################################\n";
		   }*/
			// Not needed because when update_history() is applied on constitutive update, plastic_last flag updates too 
		   /*
     		// getting and updating the plastified ip status

		   
		   const bool act = constituiveupdate1->get_plastic_last(cell_index1, ip);
		   //std::cout<<"Plastic last CU1 : 	"<<act<<std::endl;
		   constitutiveupdate2->update_plastic_last(cell_index2, ip, act);
		   
		   const bool act2 = constitutiveupdate2->get_plastic_last(cell_index2, ip);
		   //std::cout<<"Plastic last CU2 : 	"<<act2<<std::endl;
		   if(act!=act2)
		   {
			   std::cout << "###########################################################################################################\n";
			   std::cout << "Oooops issue there\n";
			   std::cout << "###########################################################################################################\n";
		   }*/
			   
/*
		ConstitutiveUpdate2->plastic_last()[cell_index2][ip] = constituiveupdate1->_plastic_last()[cell_index_1][ip];  // Will it access the plastic last status using _plastic_last() ?
		*/
		
		}
		
		
	if (cell_map1[cell_index1] != cell_map2[cell_index2])	
		{
			std::cout << "###########################################################################################################\n";
			std::cout << "The identity of the cells in previous and current domain does not match, please check the mapping algorithm\n";
			std::cout << "###########################################################################################################\n";
		
			std::cout<<"cell_map_id_1		"<<cell_map1[cell_index1]<<std::endl;
			std::cout<<"cell_map_id_2		"<<cell_map2[cell_index2]<<std::endl;
		}		
		
	
	++counterr;
 }
std::cout << "###########################################################################################################\n";
// the looping to check what are the values of isplastic, eps_p and eps_p_eq for rest of the domain ?

/*
//////////////////////////////////////// check the values of the evolved domain elements /////////////////////////////////

for(auto i:remain_from_mapping){ // kind of iterating through each element

	
	const std::size_t remain_cell_index = i;
	Eigen::Matrix<double, 6, 1> oldvalueeps;
	//strain_p.setZero(); // set the plastic strain first zero for constitutiveupdate2 as zero	
	Eigen::Matrix<double, 1, 1> oldvalue_equiv;	
	
	std::size_t num_ip_per_cell = constitutiveupdate2->num_ip_per_cell();
		
		for (std::size_t ip = 0; ip < num_ip_per_cell; ip++) //151 line from Constitive update cpp
		{
		    constitutiveupdate2->eps_p()->get_old_values(
			remain_cell_index, ip, oldvalueeps);   // Will it access the plastic strain using _eps_p() ?
			
			// getting the plastic strain values
			
			constitutiveupdate2->eps_p_eq()->get_old_values(remain_cell_index, ip, oldvalue_equiv);
			std::cout<<"	"<<oldvalue_equiv;
		}
 }
std::cout<<std::endl;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
}


PYBIND11_MODULE(dolfin_cpp_module_114c47ad22ef7af5b8a965869a324004,m)
{
m.def("transferHistory",&transferHistory);
}

//__all__ = ["transferHistory"]

//Should any sort of namespace used ? 
   
	
    
