



import subprocess
subprocess.call("dijisto clean",shell=True)
marking_cpp = """
#include <pybind11/pybind11.h>
#include <dolfin/function/Expression.h>
#include <dolfin/function/FunctionSpace.h>
using namespace dolfin;
class MarkerTemperature2 : public Expression
{
public:
std::shared_ptr<GenericFunction> temperature;
MarkerTemperature2() : Expression() { }
void eval(Array<double>& values, const Array<double>& x,
const ufc::cell& c) const
{
// Get dolfin cell and its diameter
// FIXME: Avoid dynamic allocation
dolfin_assert(temperature->function_space());
const std::shared_ptr<const Mesh> mesh = temperature->function_space()->mesh();
const Cell cell(*mesh, c.index);
//double h = cell.h();
Array<double> temp(temperature->value_size());
temperature->eval(temp, x, c);
double wind_average =0.0;
for (uint i = 0; i < temp.size(); ++i)
wind_average+= temp[i]/temp.size();
values[0]= exp(wind_average); //cell wise relation nu (T) = exp(avaraging(T))
}
};
PYBIND11_MODULE(SIGNATURE, m)
{
pybind11::class_<MarkerTemperature2,
std::shared_ptr<MarkerTemperature2>,
Expression>
(m, "MarkerTemperature")
.def(pybind11::init<>())
.def_readwrite("temperature", &MarkerTemperature2::temperature);
}
"""
_expr = df.compile_cpp_code(marking_cpp).MarkerTemperature
def TempDG(temperature):
mesh=temperature.function_space().mesh()
dgElem = df.FiniteElement("DG",mesh.ufl_cell(),0)
marker_temp = df.CompiledExpression(_expr(), element=dgElem, domain=mesh)
marker_temp.temperature = temperature._cpp_object
return marker_temp
mf = df.MeshFunction("size_t",mesh, mesh.topology().dim(),0)
expr= TempDG(temperature)
values_=df.project(expr,DGT)
print(DGT.dofmap().cell_dofs(0))



expr= TempDG(temperature)
values_=df.project(expr,DGT)
fsmtangent_list=[] #1st <-> cells mark by 0
#2nd <-> cells mark by 1...
fsmstress_list=[]
#eps..
materials = df.MeshFunction("size_t",mesh, mesh.topology().dim(),0)
for cell in df.cells(mesh):
    index=cell.index()
    dof= DGT.dofmap().cell_dofs(index)
    materials.array()[index]=index
    nu_= values_.vector()[dof]# avaraging value of Temperature for cell.. index.
    E_ = analytic fucntion depent on T.
    yield_stress= ...
    hardening_parameter = ...
    J2_1 = fsm.VonMises(E_, nu_, yield_stress_, hardening_parameter_) ##cellwise. E, nu
    fsm_constitutive_update_1 = fsm.ConstitutiveUpdate(Qdef, J2_1)
    fsm_tangent_1 = fsm.QuadratureFunction(Vt, fsm_constitutive_update_1.w_tangent(), f
    fsm_stress_1 = fsm.QuadratureFunction(Vs, fsm_constitutive_update_1.w_stress(), fsm
    fsmtangent_list.append(fsm_tangent_1)
    fsmstress_list.append(fsm_stress_1)
# here u define a list here .. fsm
#One MultiWrapper : 1st arg fsmList , 2nd arg is mf
fsm_tangent = MultiMaterialWrapper(fsmtangent_list, materials)
fsm_stress = MultiMaterialWrapper(fsmstress_list, materials)




mf = df.MeshFunction("size_t",mesh, mesh.topology().dim(),0)
expr= TempDG(temperature)
values_=df.project(expr,DGT)
mf.array()[:]= values_.vector()[:]
df.plot(mf)



values_=df.project(temperature,DGT)
print(set(values_.vector()[:]))




#expr= TempDG(temperature)
mf = df.MeshFunction("size_t",mesh, mesh.topology().dim(),0)
values_=df.project(temperature,DGT)
print(values_.vector()[:])
for cell in df.cells(mesh):
    index=cell.index()
    if values_.vector()[index]>0.2:
        mf[index]=1
df.plot(mf)
#Tempurature (treushold )=> mark cell
# for different cell marker => E(t) nu(t) hardening(t).
#nu = exp(t) ...
#nu GenericFunction.
