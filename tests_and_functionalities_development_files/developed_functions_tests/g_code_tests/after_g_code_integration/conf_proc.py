case = 1

if (case == 1):
    tinit = 400          # Initial temperature, and bottom Dirichlet
elif (case == 2):
    tinit = 303
elif (case == 3):
    tinit = 700
tglov = 400#303          # Glove comp. temp for Robin

nhepa = 1
beamrad = 0.000025#0.00025

if case == 1 or case == 2:
    pow3d = 0#1000
elif case == 3:
    pow3d = 750

alpha = 0.0028826225158256505
lah = -2.0*beamrad
velbeam = 2#50*0.016933333

if case == 1:
    switch_dir = False
    pause = 0.0
elif case == 2 or case == 3:
    switch_dir = True
    pause = 240.0e-3
