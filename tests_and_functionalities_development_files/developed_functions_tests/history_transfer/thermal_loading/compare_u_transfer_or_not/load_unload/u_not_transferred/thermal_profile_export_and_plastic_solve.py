# using the maping functions for dipsplacement transfer
# checking if solver accepts initial solution without problem

#from fenicstools.Probe import *
import fsm
import sys
#import pdb

#set_log_level(10)

################ things for transfer history function #########################
import types
import os
import dolfin.cpp as cpp
from dolfin import compile_cpp_code
from dolfin import *

pwd = os.path.dirname(os.path.abspath(__file__))
with open(pwd + "/history_transfer/history_transfer.cpp", "r") as f:
    cpp_code_transfer_history = f.read()

#path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
#module_cpp = compile_cpp_code(cpp_code_transfer_history,include_dir=[path,INCLUDEofFSM]) #fsm.get_include()

path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
#module_cpp = compile_cpp_code(cpp_code_transfer_history,include_dirs=['/home/fenics/shared/fenics-solid-mechanics/fsm/src/']) 
module_cpp = compile_cpp_code(cpp_code_transfer_history, include_dirs=['/usr/local/lib/python3.6/dist-packages/fsm-0.0.2-py3.6.egg/']) 

#fsm.get_include()

def transferHistory(CU1, CU2, map1, map2):
#def transferHistory(J,CU1):
   module_cpp.transferHistory(CU1, CU2, map1, map2)   # can it be called now as cpp_object etc.?
   #module_cpp.transferHistory(J,CU1)
################################################################################

from dolfin import *

class DirichletBoundaryX(SubDomain):
    def inside(self, x, on_boundary):
        return (x[0] < DOLFIN_EPS)
class DirichletBoundaryY(SubDomain):
    def inside(self, x, on_boundary):
        return (x[1] < DOLFIN_EPS) 
class DirichletBoundaryZ(SubDomain):
    def inside(self, x, on_boundary):
        return (x[2] < DOLFIN_EPS)

class PrescribedDisplacementY(SubDomain):
    def inside(self, x, on_boundary):
        return between( x[1], (1-DOLFIN_EPS, 1) )

class HalfDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 0.5))
class QuarterDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 0.75))
class WholeDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 1))
        


def solution_transfer_function(mesh_Complete, half_cube_mesh, quarter_cube_mesh, u_half_cube, u_quarter_cube):
    # Builds cells mapping between parent meshes
    half = half_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()
    quarter = quarter_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()

    # Builds cells mapping betwen childs
    map = [j for i, c in enumerate(half) for j, d in enumerate(quarter) if c==d]
    
    # functional spaces (must be same as in the actual problem)
    V_half_cube = VectorFunctionSpace(half_cube_mesh, "Lagrange", 2)
    V_quarter_cube = VectorFunctionSpace(quarter_cube_mesh, "Lagrange", 2)
    
    # Get cell dofmaps
    half_dofmap = V_half_cube.dofmap()
    quarter_dofmap = V_quarter_cube.dofmap()

    # Assign values to functions for testint
    #u_half_cube.vector()[:] = np.random.rand(u_half_cube.vector()[:].size, )

    # Assing dofs to quarter
    for c in cells(half_cube_mesh):
      u_quarter_cube.vector()[quarter_dofmap.cell_dofs(map[c.index()])] = u_half_cube.vector()[half_dofmap.cell_dofs(c.index())]
    return u_quarter_cube

# making output file
'''file1 = XDMFFile('output.xdmf')      
file1.parameters['functions_share_mesh'] = True
file1.parameters['rewrite_function_mesh'] = False
file1.parameters["flush_output"] = True'''



# Making complete mesh
mesh_Complete = Mesh(sys.argv[1]) # in future it can be read via mesh_list[len(mesh_list)]
V_Complete  = VectorFunctionSpace(mesh_Complete, "Lagrange", 2)

####################### write mesh info for mesh and temperature solution (h5) #######################
#meshinfo= HDF5File(mesh_Complete.mpi_comm(),'tempProfile/x_dir_evolving_with_thermal.h5', 'w')

mark_sub_domain = MeshFunction("size_t", mesh_Complete, mesh_Complete.topology().dim(), 0)

number_of_submesh = 2

# defining the arrays of previous solution record
u_old = []
mesh_old = []
fsm_constitutive_update_old = []
cell_map_old = []
    
# marking the subdomain
for submesh_number in range(number_of_submesh):    
    if (submesh_number == 0) :
        mark_sub_domain.set_all(0)
        mark_domain = HalfDomain()
        print(type(mark_domain))
        mark_domain.mark(mark_sub_domain, 1)
        sub_mesh = MeshView.create(mark_sub_domain, 1)
        print('HalfDomain')
        
    elif (submesh_number == 1) :
        mark_sub_domain.set_all(0)
        mark_domain = HalfDomain()#QuarterDomain()
        mark_domain.mark(mark_sub_domain, 1)
        sub_mesh = MeshView.create(mark_sub_domain, 1)
        print('QuarterDomain')
        
        
    elif (submesh_number == 2) :
        mark_sub_domain.set_all(0)
        mark_domain = HalfDomain()#WholeDomain()
        mark_domain.mark(mark_sub_domain, 1)
        sub_mesh = MeshView.create(mark_sub_domain, 1)
        print('WholeDomain')
        
        
    cell_map = sub_mesh.topology().mapping()[mesh_Complete.id()].cell_map()
    #print (cell_map)
    #print (len(cell_map))
    
    mesh = sub_mesh # <class 'dolfin.cpp.mesh.Mesh'>
    print (type (mesh) ) 
    print (type (mesh_Complete) )
    #mesh = mesh_Complete
    
    ### exporting marking and subdomain mesh ###
    #meshinfo.write(mesh,"/submesh/{}_part".format(submesh_number))
    #meshinfo.write(mark_sub_domain,"/meshfunction/{}_part".format(submesh_number)) # cell marker
    
    E = 200.0;
    nu = 0.3;

    scheme = "default"
    degree = 3
   
    #dx = Measure(("dx"),domain=mesh, subdomain_data=mark_sub_domain) # not sure if domain = mesh or domain = meshComplete
    #dx = dx(degree=degree, scheme=scheme)
    
    dx = Measure("dx")
    dx = dx(degree=degree, scheme=scheme)
    
    V  = VectorFunctionSpace(mesh, "Lagrange", 2)
    element_t = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=36, quad_scheme=scheme)
    Vt = FunctionSpace(mesh, element_t)
    element_s = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
    Vs = FunctionSpace(mesh, element_s)
    
    T  = FunctionSpace(mesh, "Lagrange", 1)
    
    #print ( type(element_s) )  # <class 'ufl.finiteelement.mixedelement.VectorElement'>

    zero = Constant(0.0)
    
    bc0 = DirichletBC(V.sub(0), zero, DirichletBoundaryY(), method="pointwise")
    bc1 = DirichletBC(V.sub(1), zero, DirichletBoundaryY(), method="pointwise")
    bc2 = DirichletBC(V.sub(2), zero, DirichletBoundaryY(), method="pointwise")
    
    bcs = [bc0, bc1, bc2]

    E_t = 0.3*E
    hardening_parameter = E_t/(1.0 - E_t/E)
    yield_stress = 9.0

    u = Function(V, name="u")
    #u_to_plot = Function(V, name="u_to_plot")
    
    # temperature defination and importing temperature solution
    t = Function(T, name="T")
    temp_exp = Expression(" x[1]*x[1]*x[1]*t2",t2=0,degree=2)
    #temp_exp = Expression(" x[1]*t2",t2=0,degree=2)
    
    
    def mech_eps(u):
        return as_vector([u[i].dx(i) for i in range(3)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1), (0, 2), (1, 2)]])

    def thermal_eps(t):
        expansion_coeff = Constant(10.E-4)#Constant(10.E-6)
        return expansion_coeff * as_vector([1., 1., 1., 0., 0., 0.]) * t

    def eps(u, t):
        return mech_eps(u) - thermal_eps(t)

    def sigma(s):
        #s = ss.function_space()
        return as_matrix([[s[0], s[3], s[4]], [s[3], s[1], s[5]], [s[4], s[5], s[2]]])

    def tangent(t):
        #t = tt.function_space()
        return as_matrix([[t[i*6 + j] for j in range(6)] for i in range(6)])

    J2 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter)
    
    Qdef = fsm.UFLQuadratureFunction(eps(u, t), element_s, mesh)
    
    fsm_constitutive_update = fsm.ConstitutiveUpdate(Qdef, J2)
    
    # declaring history data
    #import pdb
    #pdb.set_trace()
    #print(fsm.HistoryData)
    #fsm_history_data = fsm.HistoryData.cpp_object(mesh, Vs.element(), "size_t")
    
    #transferHistory( J2, fsm_constitutive_update.cpp_object() )
    '''    
    ################################## plotting the default eps_eq on current CU ################
        
    eps_p_eq_default = fsm_constitutive_update.eps_p_eq()
    element_eps_p_eq_project_default = FiniteElement("CG", mesh.ufl_cell(), degree=1)
    V_eps_p_eq_project_default = FunctionSpace(mesh, element_eps_p_eq_project_default)
    eps_p_eq_project_default = Function(V_eps_p_eq_project_default, name="eps_p_eq_default")
    
    assign( eps_p_eq_project_default, project(eps_p_eq_default, V_eps_p_eq_project_default, solver_type='gmres',
                                    form_compiler_parameters={
                                        "representation": parameters["form_compiler"]["representation"],
                                        "quadrature_scheme": scheme,
                                        "quadrature_degree": degree
                                    }
                                ) )
    file1_eqv_eps_p_default = XDMFFile("output/output_eqv_eps_p_default" + format(submesh_number, '04') + ".xdmf")
    file1_eqv_eps_p_default.write(eps_p_eq_project_default, t=float(submesh_number));
    file1_eqv_eps_p_default.close()
    
    #transferHistory(fsm_constitutive_update.cpp_object(),
    #                    fsm_constitutive_update.cpp_object(),
    #                    cell_map,
    #                    cell_map)
    
    ###############################################################################################
    '''    
    if (submesh_number > 0):  # because first subdomain gets everything from the start as default
        
        #fsm_constitutive_update = fsm_constitutive_update_old
        #u = solution_transfer_function(mesh_Complete, mesh_old[submesh_number-1], mesh, u_old[submesh_number-1], u)
        #u.interpolate(zero)  
        '''u_to_plot = u
        # Write output to files from last submesh
        xdmff = XDMFFile("u_to_plot/u_to_plot" + format(submesh_number, '04') + ".xdmf")    
        xdmff.write(u_to_plot);
        xdmff.close()'''
        
        
        # importing history transfer
        #import pdb
        #pdb.set_trace()
        #print ( cell_map_old[500] )
        #print ( len(cell_map_old) )
        '''
        ################################## plotting the previous CU's eps_eq ##########################
        
        eps_p_eq_old = fsm_constitutive_update_old.eps_p_eq()
        print(eps_p_eq_old)
        element_eps_p_eq_project_old = FiniteElement("CG", mesh_old.ufl_cell(), degree=1)
        V_eps_p_eq_project_old = FunctionSpace(mesh_old, element_eps_p_eq_project_old)
        eps_p_eq_project_old = Function(V_eps_p_eq_project_old, name="eps_p_eq_old")
        
        assign( eps_p_eq_project_old, project(eps_p_eq_old, V_eps_p_eq_project_old, solver_type='gmres',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )
        file1_eqv_eps_p_old = XDMFFile("output/output_eqv_eps_p_old" + format(submesh_number-1, '04') + ".xdmf")
        file1_eqv_eps_p_old.write(eps_p_eq_project_old, t=float(submesh_number-1));
        file1_eqv_eps_p_old.close()
        
        ###############################################################################################
        '''
        transferHistory(fsm_constitutive_update_old[submesh_number-1].cpp_object(),
                        fsm_constitutive_update.cpp_object(),
                        cell_map_old[submesh_number-1],
                        cell_map)
        fsm_constitutive_update.update_history(); # so that all currently updated one become the old variables
        #if (submesh_number>0):
        '''transferHistory(fsm_constitutive_update.cpp_object(),
                            fsm_constitutive_update.cpp_object(),
                            cell_map,
                            cell_map)'''
        
        
        '''cell_map_old_exp = [11, 13, 16]
        cell_map_exp = [11, 22, 36, 16, 32, 13]
        transferHistory(fsm_constitutive_update_old.cpp_object(),
                        fsm_constitutive_update.cpp_object(),
                        cell_map_old_exp,
                        cell_map_exp)'''
        '''
        ################################## plotting the transformed eps_eq on current CU ################
        
        #import pdb
        #pdb.set_trace()
        eps_p_eq_transferred = fsm_constitutive_update.eps_p_eq()
        
        element_eps_p_eq_project_transferred = FiniteElement("CG", mesh.ufl_cell(), degree=1)
        V_eps_p_eq_project_transferred = FunctionSpace(mesh, element_eps_p_eq_project_transferred)
        eps_p_eq_project_transferred = Function(V_eps_p_eq_project_transferred, name="eps_p_eq_transferred")
        
        
        
        assign( eps_p_eq_project_transferred, project(eps_p_eq_transferred, V_eps_p_eq_project_transferred, solver_type='gmres',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )
        
        
        file1_eqv_eps_p_transferred = XDMFFile("output/output_eqv_eps_p_transferred" + format(submesh_number, '04') + ".xdmf")
        file1_eqv_eps_p_transferred.write(eps_p_eq_project_transferred, t=float(submesh_number));
        file1_eqv_eps_p_transferred.close()
        
        ###############################################################################################
        '''
    
    #fsm_tangent = QuadratureFunction(mesh, Vt.element(), fsm_constitutive_update, fsm_constitutive_update.w_tangent())
    #fsm_stress = QuadratureFunction(mesh, Vs.element(), fsm_constitutive_update.w_stress())
    fsm_tangent = fsm.QuadratureFunction(Vt, fsm_constitutive_update.w_tangent(), fsm_constitutive_update)
    fsm_stress  = fsm.QuadratureFunction(Vs, fsm_constitutive_update.w_stress())

    v = TestFunction(V)
    uTrial = TrialFunction(V)

    a = inner(mech_eps(v), dot(tangent(fsm_tangent), mech_eps(uTrial)) )*dx#(1) + (1e-90)*inner(eps(v), dot(tangent(fsm_tangent), eps(uTrial)) )*dx(0)
    L = inner(grad(v), sigma(fsm_stress))*dx#(1) + (1e-90)*inner(grad(v), sigma(fsm_stress))*dx(0)

    nonlinear_problem = fsm.PlasticityProblem(a, L, u, fsm_tangent, fsm_stress, bcs) 
       
    nonlinear_solver = NewtonSolver()
    nonlinear_solver.parameters["convergence_criterion"] = "incremental";
    nonlinear_solver.parameters["maximum_iterations"]    = 50;
    nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
    nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;

    # File names for output

    eps_p_eq = fsm_constitutive_update.eps_p_eq()
    #fsm_constitutive_update.eps_p_eq().compute_mean(eps_eq);

    element_eps_p_eq_project = FiniteElement("DG", mesh.ufl_cell(), degree=1)
    V_eps_p_eq_project = FunctionSpace(mesh, element_eps_p_eq_project)
    eps_p_eq_project = Function(V_eps_p_eq_project, name="eps_p_eq")
    
    #################################################################################
    # try to check if eps_eq is still same as that of the transferred from last state
    assign( eps_p_eq_project, project(eps_p_eq, V_eps_p_eq_project, solver_type='gmres',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )
    file1_eqv_eps_p = XDMFFile("output/output_eqv_eps_p_check" + format(submesh_number, '04') + ".xdmf")
    file1_eqv_eps_p.write(eps_p_eq_project, t=float(submesh_number));
    file1_eqv_eps_p.close()
    
    '''
    # using probe tool
    #from fenicstools.Probe import *
    print("Probe result for submesh number :    ",submesh_number)
    if(submesh_number < 2):
        x = array([[0.5, 0, 0.5], [0.5, 0.5, 0.5], [0.5, 1, 0.5]])  # coordinates of the 3 points of probe location
    else:
        x = array([[0.75, 0, 0.5], [0.75, 0.5, 0.5], [0.75, 1, 0.5]])  # coordinates of the 3 points of probe location
    p = Probes(x.flatten(), V_eps_p_eq_project)   # here the probe location or positions are given to the probes constructor
    p(eps_p_eq_project)
    print(p.array())
    print ( p.dump("testarray") )
    print("####################################################")
	'''
    '''
    # using plot tool
    plot(mesh)
    if(submesh_number > 1):
        plot(eps_p_eq_project)
    import matplotlib.pyplot as plt
    plt.savefig("figure.png")
    plt.show()'''
    
    
    #################################################################################
    
    #################################################################################
    # try to check if eps_eq is still same as that of the transferred from last state
    u_to_check = u
    # Write output to files from last submesh
    xdmff = XDMFFile("output/u_to_check" + format(submesh_number, '04') + ".xdmf")    
    xdmff.write(u_to_check);
    xdmff.close()
    #################################################################################
    
    
    # temperature profile    
    if (submesh_number == 0):
        temp_exp.t2 = 1000*(submesh_number + 1)
    else:
        temp_exp.t2 = 0        
    #print((submesh_number + 1) * 84)
    t.assign(interpolate(temp_exp,T))
    
    # output the temperature solution
    #meshinfo.write(t,"/temperature_solution/{}_part".format(submesh_number)) # temperature solution of current step
    
    file1_t = XDMFFile("output/output_t" + format(submesh_number, '04') + ".xdmf")
    file1_t.write(t, t=float(submesh_number));
    file1_t.close()
    
    '''if (submesh_number == 1):
        print("file closed")
        meshinfo.close()'''
    
    # Solve non-linear problem
    nonlinear_solver.solve(nonlinear_problem.cpp_object(), u.vector());
    
    #pdb.set_trace()
    
    # Update variables
    fsm_constitutive_update.update_history();
    
    #transferHistory(fsm_constitutive_update.cpp_object(),
    #                    fsm_constitutive_update.cpp_object(),
    #                    cell_map,
    #                    cell_map)

    #fsm_constitutive_update.eps_p_eq().get_old_values();
    #print(fsm_constitutive_update.eps_p_eq().old_data())

    # Write output to files from last submesh 
    
    file1_u = XDMFFile("output/output_u" + format(submesh_number, '04') + ".xdmf")
    
    
    '''file1.parameters['functions_share_mesh'] = False
    file1.parameters['rewrite_function_mesh'] = False
    file1.parameters["flush_output"] = False'''
    
    #u_array = u.vector()
    #print (u_array.size())
    
    file1_u.write(u, t=float(submesh_number));
    file1_u.close()
    
    
    
    assign( eps_p_eq_project, project(eps_p_eq, V_eps_p_eq_project, solver_type='gmres',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )
    
    file1_eqv_eps_p = XDMFFile("output/output_eqv_eps_p" + format(submesh_number, '04') + ".xdmf")
    file1_eqv_eps_p.write(eps_p_eq_project, t=float(submesh_number));
    file1_eqv_eps_p.close()

    #previous subdomain solution and variables:
    
    u_old.append(u)
    mesh_old.append(mesh)
    fsm_constitutive_update_old.append(fsm_constitutive_update)
    cell_map_old.append(cell_map)
    print(type(fsm_constitutive_update_old[0]))
    
#meshinfo.close()