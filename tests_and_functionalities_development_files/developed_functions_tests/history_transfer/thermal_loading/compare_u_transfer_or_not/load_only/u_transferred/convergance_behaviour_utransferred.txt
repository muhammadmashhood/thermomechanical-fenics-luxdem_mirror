Newton iteration 0: r (abs) = 8.406e+00 (tol = 1.000e-15) r (rel) = 1.000e+00 (tol = 1.000e-06)
Newton iteration 1: r (abs) = 2.080e-01 (tol = 1.000e-15) r (rel) = 2.474e-02 (tol = 1.000e-06)
Newton iteration 2: r (abs) = 8.453e-03 (tol = 1.000e-15) r (rel) = 1.006e-03 (tol = 1.000e-06)
Newton iteration 3: r (abs) = 5.269e-04 (tol = 1.000e-15) r (rel) = 6.268e-05 (tol = 1.000e-06)
Newton iteration 4: r (abs) = 1.804e-05 (tol = 1.000e-15) r (rel) = 2.146e-06 (tol = 1.000e-06)
Newton iteration 5: r (abs) = 8.782e-08 (tol = 1.000e-15) r (rel) = 1.045e-08 (tol = 1.000e-06)
Newton solver finished in 6 iterations and 6 linear solver iterations.


Newton iteration 0: r (abs) = 8.275e+00 (tol = 1.000e-15) r (rel) = 1.000e+00 (tol = 1.000e-06)
Newton iteration 1: r (abs) = 8.092e-02 (tol = 1.000e-15) r (rel) = 9.779e-03 (tol = 1.000e-06)
Newton iteration 2: r (abs) = 2.748e-03 (tol = 1.000e-15) r (rel) = 3.321e-04 (tol = 1.000e-06)
Newton iteration 3: r (abs) = 6.378e-05 (tol = 1.000e-15) r (rel) = 7.708e-06 (tol = 1.000e-06)
Newton iteration 4: r (abs) = 8.601e-08 (tol = 1.000e-15) r (rel) = 1.039e-08 (tol = 1.000e-06)
Newton solver finished in 5 iterations and 5 linear solver iterations.


Newton iteration 0: r (abs) = 8.329e+00 (tol = 1.000e-15) r (rel) = 1.000e+00 (tol = 1.000e-06)
Newton iteration 1: r (abs) = 2.982e-02 (tol = 1.000e-15) r (rel) = 3.580e-03 (tol = 1.000e-06)
Newton iteration 2: r (abs) = 6.367e-04 (tol = 1.000e-15) r (rel) = 7.644e-05 (tol = 1.000e-06)
Newton iteration 3: r (abs) = 4.653e-06 (tol = 1.000e-15) r (rel) = 5.586e-07 (tol = 1.000e-06)
Newton solver finished in 4 iterations and 4 linear solver iterations.
