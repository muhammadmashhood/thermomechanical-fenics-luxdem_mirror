# modified to work with the docker image's fsm
from dolfin import *
import fsm
import sys

#set_log_level(10)

Time = 0.


class DirichletBoundaryXY_Top(SubDomain):
    def inside(self, x, on_boundary):
        return abs(x[2] - 1) < DOLFIN_EPS
class DirichletBoundaryYZ_Left(SubDomain):
    def inside(self, x, on_boundary):
        return abs(x[0] - 1) < DOLFIN_EPS
class DirichletBoundaryZX_front(SubDomain):
    def inside(self, x, on_boundary):
        return abs(x[1] - 1) < DOLFIN_EPS

class DirichletBoundaryX(SubDomain):
    def inside(self, x, on_boundary):
        return (x[0] < DOLFIN_EPS)
class DirichletBoundaryY(SubDomain):
    def inside(self, x, on_boundary):
        return (x[1] < DOLFIN_EPS)
class DirichletBoundaryZ(SubDomain):
    def inside(self, x, on_boundary):
        return (x[2] < DOLFIN_EPS)


class HalfDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (0, 0.5))
        
#mesh = UnitCubeMesh(4,4,4);
mesh_Complete = Mesh(sys.argv[1])
E = 200.0;
nu = 0.3;




# marking the domain for half cube
mark_sub_domain = MeshFunction("size_t", mesh_Complete, mesh_Complete.topology().dim(), 0)
mark_sub_domain.set_all(0)
mark_domain = HalfDomain()
print(type(mark_domain))
mark_domain.mark(mark_sub_domain, 1)
sub_mesh = MeshView.create(mark_sub_domain, 1)
print('HalfDomain')

mesh = sub_mesh

nTimeSteps = 4

scheme = "default"
degree = 3
dx = Measure("dx")
dx = dx(degree=degree, scheme=scheme)

V  = VectorFunctionSpace(mesh, "Lagrange", 2)
element_t = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=36, quad_scheme=scheme)
Vt = FunctionSpace(mesh, element_t)
element_s = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
Vs = FunctionSpace(mesh, element_s)

T  = FunctionSpace(mesh, "Lagrange", 1)
zero = Constant(0.0)


bc0 = DirichletBC(V.sub(0), zero, DirichletBoundaryY(), method="pointwise")
bc1 = DirichletBC(V.sub(1), zero, DirichletBoundaryY(), method="pointwise")
bc2 = DirichletBC(V.sub(2), zero, DirichletBoundaryY(), method="pointwise")
#bc3 = DirichletBC(V.sub(2), zero, DirichletBoundaryXY_Top(), method="pointwise")
#bc4 = DirichletBC(V.sub(0), zero, DirichletBoundaryYZ_Left(), method="pointwise")
#bc5 = DirichletBC(V.sub(1), zero, DirichletBoundaryZX_front(), method="pointwise")

bcs = [bc0, bc1, bc2]
#bcs = [bc0, bc1, bc2, bc3, bc4, bc5]

E_t = 0.3*E
hardening_parameter = E_t/(1.0 - E_t/E)
yield_stress = 9.0

u = Function(V, name="u")
#u_to_check = Function(V, name="u to check")
t = Function(T, name="T")

temp_exp_x = Expression(" x[0]*x[0]*x[0]*t2",t2=0,degree=2)
temp_exp_y = Expression(" x[1]*x[1]*x[1]*t2",t2=0,degree=2)
temp_exp_z = Expression(" x[2]*x[2]*x[2]*t2",t2=0,degree=2)
#temp_exp = Expression(" 500*x[1]*x[1]*x[1]*t1",t1=0,degree=2)

#t = temp_exp



def mech_eps(u):
    return as_vector([u[i].dx(i) for i in range(3)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1), (0, 2), (1, 2)]])

def thermal_eps(t):
    expansion_coeff = Constant(10.E-4)#Constant(10.E-6)
    return expansion_coeff * as_vector([1., 1., 1., 0., 0., 0.]) * t

def eps(u, t):
    return mech_eps(u) - thermal_eps(t)

def sigma(s):
    #s = ss.function_space()
    return as_matrix([[s[0], s[3], s[4]], [s[3], s[1], s[5]], [s[4], s[5], s[2]]])

def tangent(t):
    #t = tt.function_space()
    return as_matrix([[t[i*6 + j] for j in range(6)] for i in range(6)])

J2 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter)
Qdef = fsm.UFLQuadratureFunction(eps(u, t), element_s, mesh)
fsm_constitutive_update = fsm.ConstitutiveUpdate(Qdef, J2)
#fsm_tangent = QuadratureFunction(mesh, Vt.element(), fsm_constitutive_update, fsm_constitutive_update.w_tangent())
#fsm_stress = QuadratureFunction(mesh, Vs.element(), fsm_constitutive_update.w_stress())
fsm_tangent = fsm.QuadratureFunction(Vt, fsm_constitutive_update.w_tangent(), fsm_constitutive_update)
fsm_stress  = fsm.QuadratureFunction(Vs, fsm_constitutive_update.w_stress())


v = TestFunction(V)
uTrial = TrialFunction(V)

a = inner(mech_eps(v), dot(tangent(fsm_tangent), mech_eps(uTrial)) )*dx
L = inner(grad(v), sigma(fsm_stress))*dx

nonlinear_problem = fsm.PlasticityProblem(a, L, u, fsm_tangent, fsm_stress, bcs)

nonlinear_solver = NewtonSolver()
nonlinear_solver.parameters["convergence_criterion"] = "incremental";
nonlinear_solver.parameters["maximum_iterations"]    = 50;
nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;

# File names for output

eps_p_eq = fsm_constitutive_update.eps_p_eq()
#fsm_constitutive_update.eps_p_eq().compute_mean(eps_eq);

file1 = XDMFFile('output/output.xdmf')
file1.parameters['functions_share_mesh'] = True
file1.parameters['rewrite_function_mesh'] = False
file1.parameters["flush_output"] = True
#File("variable_temp_250_x_4_elem/my_mesh.xml") << mesh

element_eps_p_eq_project = FiniteElement("DG", mesh.ufl_cell(), degree=1)
V_eps_p_eq_project = FunctionSpace(mesh, element_eps_p_eq_project)
eps_p_eq_project = Function(V_eps_p_eq_project, name="eps_p_eq")



step = 0
for step in range(nTimeSteps):
    print("step begin: ", step)
    
    # temperature profile    
    if (step == 0):
        temp_exp_x.t2 = 1000
        t.assign(interpolate(temp_exp_x,T))
    elif (step == 1):
        temp_exp_y.t2 = 1000
        t.assign(interpolate(temp_exp_y,T))
    elif (step == 2):
        temp_exp_z.t2 = 1000
        t.assign(interpolate(temp_exp_z,T))
    else:
        temp_exp_y.t2 = 0
        t.assign(interpolate(temp_exp_y,T))
    
    #################################################################################
    # try to check if eps_eq is still same as that of the transferred from last state
    u_to_check = u
    # Write output to files from last submesh
    xdmff = XDMFFile("output/u_to_check" + format(step, '04') + ".xdmf")    
    xdmff.write(u_to_check);
    xdmff.close()
    #################################################################################
    
    # Solve non-linear problem
    nonlinear_solver.solve(nonlinear_problem.cpp_object(), u.vector());

    # Update variables
    fsm_constitutive_update.update_history();

    # Write output to files
    file1.write(u, t=float(step));
    eps_p_eq_project.vector()[:] = project(eps_p_eq, V_eps_p_eq_project, solver_type='gmres',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ).vector()
    file1.write(eps_p_eq_project, t=float(step));
    file1.write(t, t=float(step));
    

