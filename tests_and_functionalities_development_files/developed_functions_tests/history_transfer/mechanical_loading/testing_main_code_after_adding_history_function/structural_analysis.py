# modified to work with the docker image's fsm
from dolfin import *
import fsm
import sys
#set_log_level(10)

################ things for transfer history function #########################
import types
import os
import dolfin.cpp as cpp
from dolfin import compile_cpp_code
from dolfin import *

pwd = os.path.dirname(os.path.abspath(__file__))
with open(pwd + "/history_transfer/history_transfer.cpp", "r") as f:
    cpp_code_transfer_history = f.read()

#path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
#module_cpp = compile_cpp_code(cpp_code_transfer_history,include_dir=[path,INCLUDEofFSM]) #fsm.get_include()

path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
#module_cpp = compile_cpp_code(cpp_code_transfer_history,include_dirs=['/home/fenics/shared/fenics-solid-mechanics/fsm/src/']) 
module_cpp = compile_cpp_code(cpp_code_transfer_history, include_dirs=['/usr/local/lib/python3.6/dist-packages/fsm-0.0.2-py3.6.egg/']) 

#fsm.get_include()

def transferHistory(CU1, CU2, map1, map2):
#def transferHistory(J,CU1):
   module_cpp.transferHistory(CU1, CU2, map1, map2)   # can it be called now as cpp_object etc.?
   #module_cpp.transferHistory(J,CU1)
################################################################################

# BC defination

class DirichletBoundaryX(SubDomain):
    def inside(self, x, on_boundary):
        return (x[0] < DOLFIN_EPS)
class DirichletBoundaryY(SubDomain):
    def inside(self, x, on_boundary):
        return (x[1] < DOLFIN_EPS) 
class DirichletBoundaryZ(SubDomain):
    def inside(self, x, on_boundary):
        return (x[2] < DOLFIN_EPS)

class PrescribedDisplacementY(SubDomain):
    def inside(self, x, on_boundary):
        return between( x[1], (1-DOLFIN_EPS, 1) )

        


def solution_transfer_function(mesh_Complete, half_cube_mesh, quarter_cube_mesh, u_half_cube, u_quarter_cube):
    # Builds cells mapping between parent meshes
    half = half_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()
    quarter = quarter_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()

    # Builds cells mapping betwen childs
    map = [j for i, c in enumerate(half) for j, d in enumerate(quarter) if c==d]
    
    # functional spaces (must be same as in the actual problem)
    V_half_cube = VectorFunctionSpace(half_cube_mesh, "Lagrange", 2)
    V_quarter_cube = VectorFunctionSpace(quarter_cube_mesh, "Lagrange", 2)
    
    # Get cell dofmaps
    half_dofmap = V_half_cube.dofmap()
    quarter_dofmap = V_quarter_cube.dofmap()

    # Assign values to functions for testint
    #u_half_cube.vector()[:] = np.random.rand(u_half_cube.vector()[:].size, )

    # Assing dofs to quarter
    for c in cells(half_cube_mesh):
      u_quarter_cube.vector()[quarter_dofmap.cell_dofs(map[c.index()])] = u_half_cube.vector()[half_dofmap.cell_dofs(c.index())]
    return u_quarter_cube
    
    
meshComplete = Mesh(sys.argv[1]) # in future it can be read via mesh_list[len(mesh_list)]
mesh = Mesh()

########################## Loading the mesh, meshfunction and temperature solution from layers geometry ################
meshinfo = HDF5File(mesh.mpi_comm(),'tempProfile/x_dir_evolving.h5', 'r')

'''
test = []
test_mesh_func = []
meshinfo.read(mesh,"/submesh/{}_part".format(3),False)
print(mesh.topology().dim())
mark_cd = MeshFunction("size_t", meshComplete, meshComplete.topology().dim(),0)
#mark_cd.set_all(0)
meshinfo.read(mark_cd,"/meshfunction/{}_part".format(3)) # cell marker

test.append(mesh)
test_mesh_func.append(mark_cd)

print(len(test))

meshinfo_to_write= HDF5File(meshComplete.mpi_comm(),'tempProfile/layersgeom_re_written.h5', 'w')
meshinfo_to_write.write(test_mesh_func[0],"/meshfunction/{}_part".format(3))
meshinfo_to_write.close()
'''
########################## function which collects all mesh and marks data from thermal analysis ################

numberoflayer = 3
def loadMesh(meshinfo,numberoflayer):
    mesh_list=[]
    markers_list=[]
    for i in range(numberoflayer):
        meshinfo.read(mesh,"/submesh/{}_part".format(i),False)  #  because i starts from zero and 0 not present in submesh list 
        mark_cd = MeshFunction("size_t", meshComplete, meshComplete.topology().dim(),0)
        meshinfo.read(mark_cd,"/meshfunction/{}_part".format(i)) # cell marker
        mesh_list.append(mesh)
        markers_list.append(mark_cd)
        if (i==100):
            mxf = XDMFFile("tempProfile/mesh_list_at_reading"+format(i)+".xdmf")
            mxf.write(mesh_list[i])
            mxf.close()
    return mesh_list,markers_list
    #return mesh_list
    

#mesh_list, markers_list = loadMesh(meshinfo,numberoflayer)
#print (len(mesh_list))
#print (len(markers_list))




# defining the arrays of previous solution record
u_old = []
mesh_old = []
fsm_constitutive_update_old = []
cell_map_old = []

submesh_number = 0
# importing solution and solving for each layer or subdomain 
for i in range(numberoflayer): 
    
    # reading mesh, mesh marker and thermal solution associated
    #meshinfo.read(mesh,"/submesh/{}_part".format(i+1),False)  #  because i starts from zero and 0 not present in submesh list 
    mark_cd = MeshFunction("size_t", meshComplete, meshComplete.topology().dim(),0)
    #meshinfo.read(mark_cd,"/meshfunction/{}_part".format(i+1)) # cell marker
    meshinfo.read(mark_cd,"/meshfunction/{}_part".format(i)) # cell marker being imported instead off making ourself, thats it rest is same 
    
    # writing back the read mesh in fsm to visualize if correct mesh being read
    #mxf = XDMFFile("tempProfile/fsm_read_mesh/"+format(i+1)+".xdmf")
    #mxf.write(mesh)
    #mxf.close()
    '''
        if (submesh_number == 0) :
        mark_sub_domain.set_all(0)
        mark_domain = HalfDomain()
        mark_domain.mark(mark_sub_domain, 1)
        sub_mesh = MeshView.create(mark_sub_domain, 1)
        print('HalfDomain')
        
    elif (submesh_number == 1) :
        mark_sub_domain.set_all(0)
        mark_domain = QuarterDomain()
        mark_domain.mark(mark_sub_domain, 1)
        sub_mesh = MeshView.create(mark_sub_domain, 1)
        print('QuarterDomain')
        
        
    elif (submesh_number == 2) :
        mark_sub_domain.set_all(0)
        mark_domain = WholeDomain()
        mark_domain.mark(mark_sub_domain, 1)
        sub_mesh = MeshView.create(mark_sub_domain, 1)
        print('WholeDomain')'''
    
    mesh = MeshView.create(mark_cd, 1)
    
    print(type(mesh))
    print(type(meshComplete))
    
    
    cell_map = mesh.topology().mapping()[meshComplete.id()].cell_map()
    #print (cell_map)
    print (len(cell_map))
    
    E = 20000.0;
    nu = 0.3;

    scheme = "default"
    degree = 3
    
    dx = Measure("dx")
    dx = dx(degree=degree, scheme=scheme)
    
    V  = VectorFunctionSpace(mesh, "Lagrange", 2)
    element_t = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=36, quad_scheme=scheme)
    Vt = FunctionSpace(mesh, element_t)
    element_s = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
    Vs = FunctionSpace(mesh, element_s)
    
    T  = FunctionSpace(mesh, "Lagrange", 1)
    
    zero = Constant(0.0)
    prescribed_displacement = Expression(" 5e-3*t1",t1=0,degree=2)
    
    
    bc0 = DirichletBC(V.sub(0), zero, DirichletBoundaryY(), method="pointwise")
    bc1 = DirichletBC(V.sub(1), zero, DirichletBoundaryY(), method="pointwise")
    bc2 = DirichletBC(V.sub(2), zero, DirichletBoundaryY(), method="pointwise")
    bc3 = DirichletBC(V.sub(1), prescribed_displacement, PrescribedDisplacementY(), method="pointwise")
    
    bcs = [bc0, bc1, bc2, bc3]

    E_t = 0.3*E
    hardening_parameter = E_t/(1.0 - E_t/E)
    yield_stress = 9.0

    u = Function(V, name="u")
    t = Function(T, name="T")
    
    #u_to_plot = Function(V, name="u_to_plot")
    temp_exp = Expression(" 100*x[0]*x[0]*x[0]*t2",t2=0,degree=2)
    
    
    def mech_eps(u):
        return as_vector([u[i].dx(i) for i in range(3)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1), (0, 2), (1, 2)]])

    def thermal_eps(t):
        expansion_coeff = Constant(10.E-6)
        return expansion_coeff * as_vector([1., 1., 1., 0., 0., 0.]) * t

    def eps(u, t):
        return mech_eps(u) - thermal_eps(t)

    def sigma(s):
        #s = ss.function_space()
        return as_matrix([[s[0], s[3], s[4]], [s[3], s[1], s[5]], [s[4], s[5], s[2]]])

    def tangent(t):
        #t = tt.function_space()
        return as_matrix([[t[i*6 + j] for j in range(6)] for i in range(6)])

    J2 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter)
    
    Qdef = fsm.UFLQuadratureFunction(eps(u, t), element_s, mesh)
    
    fsm_constitutive_update = fsm.ConstitutiveUpdate(Qdef, J2)
    
    
     
    if (submesh_number > 0):  # because first subdomain gets everything from the start as default
        
        u = solution_transfer_function(meshComplete, mesh_old[submesh_number-1], mesh, u_old[submesh_number-1], u)
         
        '''u_to_plot = u
        # Write output to files from last submesh
        xdmff = XDMFFile("u_to_plot/u_to_plot" + format(submesh_number, '04') + ".xdmf")    
        xdmff.write(u_to_plot);
        xdmff.close()'''
        
        '''
        ################################## plotting the previous CU's eps_eq ##########################
        
        eps_p_eq_old = fsm_constitutive_update_old.eps_p_eq()
        print(eps_p_eq_old)
        element_eps_p_eq_project_old = FiniteElement("CG", mesh_old.ufl_cell(), degree=1)
        V_eps_p_eq_project_old = FunctionSpace(mesh_old, element_eps_p_eq_project_old)
        eps_p_eq_project_old = Function(V_eps_p_eq_project_old, name="eps_p_eq_old")
        
        assign( eps_p_eq_project_old, project(eps_p_eq_old, V_eps_p_eq_project_old, solver_type='gmres',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )
        file1_eqv_eps_p_old = XDMFFile("fsm_output/output_eqv_eps_p_old" + format(submesh_number-1, '04') + ".xdmf")
        file1_eqv_eps_p_old.write(eps_p_eq_project_old, t=float(submesh_number-1));
        file1_eqv_eps_p_old.close()
        
        ###############################################################################################
        '''
        
        transferHistory(fsm_constitutive_update_old[submesh_number-1].cpp_object(),
                        fsm_constitutive_update.cpp_object(),
                        cell_map_old[submesh_number-1],
                        cell_map)
                        
        fsm_constitutive_update.update_history(); # so that all currently updated one become the old variables
        
        #if (submesh_number>0):
        '''transferHistory(fsm_constitutive_update.cpp_object(),
                            fsm_constitutive_update.cpp_object(),
                            cell_map,
                            cell_map)'''
        
        
        '''cell_map_old_exp = [11, 13, 16]
        cell_map_exp = [11, 22, 36, 16, 32, 13]
        transferHistory(fsm_constitutive_update_old.cpp_object(),
                        fsm_constitutive_update.cpp_object(),
                        cell_map_old_exp,
                        cell_map_exp)'''
        '''
        ################################## plotting the transformed eps_eq on current CU ################
        
        #import pdb
        #pdb.set_trace()
        eps_p_eq_transferred = fsm_constitutive_update.eps_p_eq()
        
        element_eps_p_eq_project_transferred = FiniteElement("CG", mesh.ufl_cell(), degree=1)
        V_eps_p_eq_project_transferred = FunctionSpace(mesh, element_eps_p_eq_project_transferred)
        eps_p_eq_project_transferred = Function(V_eps_p_eq_project_transferred, name="eps_p_eq_transferred")
        
        
        
        assign( eps_p_eq_project_transferred, project(eps_p_eq_transferred, V_eps_p_eq_project_transferred, solver_type='gmres',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )
        
        
        file1_eqv_eps_p_transferred = XDMFFile("fsm_output/output_eqv_eps_p_transferred" + format(submesh_number, '04') + ".xdmf")
        file1_eqv_eps_p_transferred.write(eps_p_eq_project_transferred, t=float(submesh_number));
        file1_eqv_eps_p_transferred.close()
        
        ###############################################################################################
        '''
    
    #fsm_tangent = QuadratureFunction(mesh, Vt.element(), fsm_constitutive_update, fsm_constitutive_update.w_tangent())
    #fsm_stress = QuadratureFunction(mesh, Vs.element(), fsm_constitutive_update.w_stress())
    fsm_tangent = fsm.QuadratureFunction(Vt, fsm_constitutive_update.w_tangent(), fsm_constitutive_update)
    fsm_stress  = fsm.QuadratureFunction(Vs, fsm_constitutive_update.w_stress())

    v = TestFunction(V)
    uTrial = TrialFunction(V)

    a = inner(mech_eps(v), dot(tangent(fsm_tangent), mech_eps(uTrial)) )*dx#(1) + (1e-90)*inner(eps(v), dot(tangent(fsm_tangent), eps(uTrial)) )*dx(0)
    L = inner(grad(v), sigma(fsm_stress))*dx#(1) + (1e-90)*inner(grad(v), sigma(fsm_stress))*dx(0)

    nonlinear_problem = fsm.PlasticityProblem(a, L, u, fsm_tangent, fsm_stress, bcs) 
    
    nonlinear_solver = NewtonSolver()
    nonlinear_solver.parameters["convergence_criterion"] = "incremental";
    nonlinear_solver.parameters["maximum_iterations"]    = 50;
    nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
    nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;

    # File names for output
    
    eps_p_eq = fsm_constitutive_update.eps_p_eq()
    #fsm_constitutive_update.eps_p_eq().compute_mean(eps_eq);

    element_eps_p_eq_project = FiniteElement("DG", mesh.ufl_cell(), degree=1)
    V_eps_p_eq_project = FunctionSpace(mesh, element_eps_p_eq_project)
    eps_p_eq_project = Function(V_eps_p_eq_project, name="eps_p_eq")
    
    
    #################################################################################
    # try to check if eps_eq is still same as that of the transferred from last state
    assign( eps_p_eq_project, project(eps_p_eq, V_eps_p_eq_project, solver_type='gmres',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )
    file1_eqv_eps_p = XDMFFile("fsm_output/output_eqv_eps_p_check" + format(i, '04') + ".xdmf")
    file1_eqv_eps_p.write(eps_p_eq_project, t=float(i));
    file1_eqv_eps_p.close()
    
    '''
    # using probe tool
    #from fenicstools.Probe import *
    print("Probe result for submesh number :    ",i+1)
    if(submesh_number < 2):
        x = array([[0.5, 0, 0.5], [0.5, 0.5, 0.5], [0.5, 1, 0.5]])  # coordinates of the 3 points of probe location
    else:
        x = array([[0.75, 0, 0.5], [0.75, 0.5, 0.5], [0.75, 1, 0.5]])  # coordinates of the 3 points of probe location
    p = Probes(x.flatten(), V_eps_p_eq_project)   # here the probe location or positions are given to the probes constructor
    p(eps_p_eq_project)
    print(p.array())
    print ( p.dump("testarray") )
    print("####################################################")
	'''
    '''
    # using plot tool
    plot(mesh)
    if(submesh_number > 1):
        plot(eps_p_eq_project)
    import matplotlib.pyplot as plt
    plt.savefig("figure.png")
    plt.show()'''
    
    
    #################################################################################
    
    #################################################################################
    # try to check if eps_eq is still same as that of the transferred from last state
    u_to_check = u
    # Write output to files from last submesh
    xdmff = XDMFFile("fsm_output/u_to_check" + format(i, '04') + ".xdmf")    
    xdmff.write(u_to_check);
    xdmff.close()
    #################################################################################
    
    
    if (submesh_number == 0):
        prescribed_displacement.t1 = 0.75
    elif (submesh_number == 1):
        prescribed_displacement.t1 = 0.75
    elif (submesh_number == 2):
        prescribed_displacement.t1 = 0.75
    elif (submesh_number == 3):
        prescribed_displacement.t1 = 0.75
    else:
        prescribed_displacement.t1 = 1
    
    # temperature field assignment
    
    temp_exp.t2 = 0
    t.assign(interpolate(temp_exp,T))
    
    # Solve non-linear problem
    nonlinear_solver.solve(nonlinear_problem.cpp_object(), u.vector());
    
    #pdb.set_trace()
    
    # Update variables
    fsm_constitutive_update.update_history();
    
    #transferHistory(fsm_constitutive_update.cpp_object(),
    #                    fsm_constitutive_update.cpp_object(),
    #                    cell_map,
    #                    cell_map)

    #fsm_constitutive_update.eps_p_eq().get_old_values();
    #print(fsm_constitutive_update.eps_p_eq().old_data())

    # Write output to files from last submesh 
    
    file1_u = XDMFFile("fsm_output/output_u" + format(i, '04') + ".xdmf")
    
    '''file1.parameters['functions_share_mesh'] = False
    file1.parameters['rewrite_function_mesh'] = False
    file1.parameters["flush_output"] = False'''
    
    #u_array = u.vector()
    #print (u_array.size())
    
    file1_u.write(u, t=float(i));
    file1_u.close()
    
    file2_t = XDMFFile("fsm_output/output_t" + format(i, '04') + ".xdmf")
    file2_t.write(t, t=float(i));
    file2_t.close()
    
    assign( eps_p_eq_project, project(eps_p_eq, V_eps_p_eq_project, solver_type='gmres',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )
    
    file1_eqv_eps_p = XDMFFile("fsm_output/output_eqv_eps_p" + format(i, '04') + ".xdmf")
    file1_eqv_eps_p.write(eps_p_eq_project, t=float(i));
    file1_eqv_eps_p.close()

    #previous subdomain solution and variables:
    
    u_old.append(u)
    mesh_old.append(mesh)
    fsm_constitutive_update_old.append(fsm_constitutive_update)
    cell_map_old.append(cell_map)
    
    submesh_number = submesh_number + 1
    
    

meshinfo.close()
