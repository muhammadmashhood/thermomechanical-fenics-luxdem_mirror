import gc
import sys
import pprint
from dolfin import *


class K(UserExpression):
    def __init__(self, k_0, k_1, **kwargs):
        super().__init__(**kwargs)
        self.k_0 = k_0
        self.k_1 = k_1

    def eval(self, value, x):
        "Set value[0] to value at point x"
        tol = 1E-14
        if x[1] <= 0.5 + tol:
            value[0] = self.k_0
        else:
            value[0] = self.k_1



listObj=[]
for idx in range(0,100):
    myObj=K(idx,idx+1,degree=0)
    listObj.append(myObj)

gc.set_debug(gc.DEBUG_STATS)
del listObj
gc.collect()	

