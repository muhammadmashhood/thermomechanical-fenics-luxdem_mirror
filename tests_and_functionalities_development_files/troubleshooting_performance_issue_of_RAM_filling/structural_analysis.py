# modified to work with the docker image's fsm
#from dolfin import *
import fsm
import sys
from fenics import *
# garbage thing
#import gc
#gc.disable()

# BC defination

class DirichletBoundaryZ(SubDomain):
    def inside(self, x, on_boundary):
        return (x[2] < DOLFIN_EPS)     

class HalfDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[2], (0, 0.5))
class ThreeQuarterDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[2], (0, 0.75))   
class WholeDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[2], (0, 1)) 

#meshComplete = Mesh(gcode_conf.arg1) # in future it can be read via mesh_list[len(mesh_list)]
meshComplete = UnitCubeMesh(14,14,14)



for i in range(0, 3): # for multi material wrapper check
    print ("simulating step number:    ",i)
    print()

    mark_cd = MeshFunction("size_t", meshComplete, meshComplete.topology().dim(),0) 
    
    if(i == 0):
        mark1 = HalfDomain()
        mark1.mark(mark_cd, 1)
    
    elif(i == 1):
        mark1 = ThreeQuarterDomain()
        mark1.mark(mark_cd, 1)

    elif(i == 2):
        mark1 = WholeDomain()
        mark1.mark(mark_cd, 1)


    mesh = MeshView.create(mark_cd, 1)
    

    scheme = "default"
    degree = 3
    
    dx = Measure("dx")
    dx = dx(degree=degree, scheme=scheme)
    
    V  = VectorFunctionSpace(mesh, "Lagrange", 2)
    element_t = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=36, quad_scheme=scheme)
    Vt = FunctionSpace(mesh, element_t)
    element_s = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
    Vs = FunctionSpace(mesh, element_s)
            
    zero = Constant(0.0)
    
    bc0 = DirichletBC(V.sub(0), zero, DirichletBoundaryZ(), method="pointwise")
    bc1 = DirichletBC(V.sub(1), zero, DirichletBoundaryZ(), method="pointwise")
    bc2 = DirichletBC(V.sub(2), zero, DirichletBoundaryZ(), method="pointwise")
    
    bcs = [bc0, bc1, bc2]

    u = Function(V, name="u")

    
    def mech_eps(u):
        return as_vector([u[i].dx(i) for i in range(3)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1), (0, 2), (1, 2)]])

    
    Qdef = fsm.UFLQuadratureFunction(mech_eps(u), element_s, mesh)
        
    materials = MeshFunction("size_t", mesh, mesh.topology().dim())
    materials.set_all(0)

    # lists to be assigned to multi-material wrapper
    fsm_tangent_list = [] 
    fsm_stress_list = []
    fsm_constitutive_update_list = []
    eps_p_eq_list = []

    print()
    print("Assigning property to each cell")
    print()
    print("mesh.num_cells():",mesh.num_cells())

    for cell in cells(mesh):
        index = cell.index()
         
        materials[index] = index          # giving label to the particular cell
                
        nu = 0.33
        yield_stress = 9
        E = 2000
        
        E_t = 0.3*E
        hardening_parameter = E_t/(1.0 - E_t/E) 

        J2_1 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter) # .self ?

        fsm_constitutive_update_1 = fsm.ConstitutiveUpdate(Qdef, J2_1)

        fsm_constitutive_update_list.append(fsm_constitutive_update_1)
        
        #fsm_tangent_1 = fsm.QuadratureFunction(Vt, fsm_constitutive_update_1.w_tangent(), fsm_constitutive_update_1)
        #fsm_stress_1 = fsm.QuadratureFunction(Vs, fsm_constitutive_update_1.w_stress()) #, fsm_constitutive_update_1)?
        
        eps_p_eq_list.append( fsm_constitutive_update_list[index].eps_p_eq() )
        #fsm_tangent_list.append(fsm_tangent_1)
        #fsm_stress_list.append(fsm_stress_1)


    del fsm_constitutive_update_list

    #del fsm_tangent_list
    #del fsm_stress_list

