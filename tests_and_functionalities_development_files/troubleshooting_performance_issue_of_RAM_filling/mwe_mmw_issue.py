import fsm
import sys
from fenics import *
# garbage thing
#import gc
#gc.disable()

# BC defination

class DirichletBoundaryZ(SubDomain):
    def inside(self, x, on_boundary):
        return (x[2] < DOLFIN_EPS)     

mesh = UnitCubeMesh(13,13,13)
print()
print("Number of cells:", mesh.num_cells())
print()    

scheme = "default"
degree = 3

dx = Measure("dx")
dx = dx(degree=degree, scheme=scheme)

V  = VectorFunctionSpace(mesh, "Lagrange", 2)
element_t = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=36, quad_scheme=scheme)
Vt = FunctionSpace(mesh, element_t)
element_s = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
Vs = FunctionSpace(mesh, element_s)
    
zero = Constant(0.0)

bc0 = DirichletBC(V.sub(0), zero, DirichletBoundaryZ(), method="pointwise")
bc1 = DirichletBC(V.sub(1), zero, DirichletBoundaryZ(), method="pointwise")
bc2 = DirichletBC(V.sub(2), zero, DirichletBoundaryZ(), method="pointwise")

bcs = [bc0, bc1, bc2]

u = Function(V, name="u")


def mech_eps(u):
	return as_vector([u[i].dx(i) for i in range(3)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1), (0, 2), (1, 2)]])


Qdef = fsm.UFLQuadratureFunction(mech_eps(u), element_s, mesh)

materials = MeshFunction("size_t", mesh, mesh.topology().dim())
materials.set_all(0)

# lists to be assigned to multi-material wrapper
fsm_tangent_list = [] 
fsm_stress_list = []
fsm_constitutive_update_list = []
eps_p_eq_list = []

print()
print("Assigning property to each zone or cell")
print()


for i in range(mesh.num_cells()):

	materials[i] = i          # giving label to the particular cell
		
	nu = 0.33
	yield_stress = 9
	E = 2000

	E_t = 0.3*E
	hardening_parameter = E_t/(1.0 - E_t/E) 

	J2_1 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter) 

	fsm_constitutive_update_1 = fsm.ConstitutiveUpdate(Qdef, J2_1)
	
	fsm_constitutive_update_list.append(fsm_constitutive_update_1)

	fsm_tangent_1 = fsm.QuadratureFunction(Vt, fsm_constitutive_update_1.w_tangent(), fsm_constitutive_update_1)
	fsm_stress_1 = fsm.QuadratureFunction(Vs, fsm_constitutive_update_1.w_stress()) #, fsm_constitutive_update_1)?

	fsm_tangent_list.append(fsm_tangent_1)
	fsm_stress_list.append(fsm_stress_1)

# hadning over the list of tangent and stress tensors to multi material wrapper
#fsm_tangent = mmw.MultiMaterialWrapper(fsm_tangent_list, materials)
#fsm_stress = mmw.MultiMaterialWrapper(fsm_stress_list, materials)

#.............................
# after the solver gives the results each member of fsm_constitutive_update_list will be applied with function of <.update_history()>



