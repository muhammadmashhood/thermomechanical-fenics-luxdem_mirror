import gc
import sys
import pprint

class MyObj(object):
    def __init__(self, name):
        self.name = name
        
listObj=[]
for idx in range(0,100):
    myObj=MyObj("pippo_{0}".format(idx))
    listObj.append(myObj)

gc.set_debug(gc.DEBUG_STATS)
del listObj
gc.collect()	
#for o in gc.garbage():
#    print("{0} and {1}".format(o,id(o)))
