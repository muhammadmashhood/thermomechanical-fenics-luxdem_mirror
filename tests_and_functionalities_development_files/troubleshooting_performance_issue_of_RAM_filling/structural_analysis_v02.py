# modified to work with the docker image's fsm
#from dolfin import *
import fsm
import sys
#set_log_level(10)
from G_code_functionality import conf_g_code as gcode_conf

################ things for transfer history function #########################
import types
import os
import dolfin.cpp as cpp
import conf_geom as config_geom
#from dolfin import compile_cpp_code
#from dolfin import *
from fenics import *
from conf_struct import *
import numpy as np
# multi-material wrapper
#from multimaterial_wrapper_for_AM import multi_material_wrapper as mmw

#pwd = os.path.dirname(os.path.abspath(__file__))
#with open(pwd + "/history_transfer/history_transfer.cpp", "r") as f:
#    cpp_code_transfer_history = f.read()


#path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
#module_cpp = compile_cpp_code(cpp_code_transfer_history, include_dirs=['/usr/local/lib/python3.6/dist-packages/fsm-0.0.2-py3.6.egg/']) 

#def transferHistory(CU1, CU2, map1, map2):
#   module_cpp.transferHistory(CU1, CU2, map1, map2)   # can it be called now as cpp_object etc.?

################################################################################

# BC defination

class DirichletBoundaryZ(SubDomain):
    def inside(self, x, on_boundary):
        return (x[2] < DOLFIN_EPS)     
    

meshComplete = Mesh(gcode_conf.arg1) # in future it can be read via mesh_list[len(mesh_list)]
                                # for input the mesh for thermal analysis and structural solver should be same
#meshComplete = Mesh(sys.argv[1]) # for multi material wrapper check
mesh = Mesh()

########################## Loading the mesh, meshfunction and temperature solution from layers geometry ################
meshinfo = HDF5File(mesh.mpi_comm(),'tempProfile/layersgeom_for_history.h5', 'r')
#meshinfo = HDF5File(MPI.comm_world,'tempProfile/layersgeom_for_history.h5', 'r')

########################## function which collects all mesh and marks data from thermal analysis ################

num_steps_data_row = np.genfromtxt(config_geom.num_steps_file_path, delimiter = ',')
numberoflayer = int(num_steps_data_row[0])
numberoflayer = numberoflayer + 1


submesh_number = 0
print("Number of layers: ",numberoflayer)
print()
# importing solution and solving for each layer or subdomain # i determines from which layer simulation should start
#for i in range(0, 3): # for multi material wrapper check
for i in range(n_initial, numberoflayer): 
    print ("simulating layer number:    ",i)
    print()

    mark_cd = MeshFunction("size_t", meshComplete, meshComplete.topology().dim(),0)
    meshinfo.read(mark_cd,"/meshfunction/{}_part".format(i)) # cell marker being imported instead off making ourself, thats it rest is same 
    
    mesh = MeshView.create(mark_cd, 1)
    

    scheme = "default"
    degree = 3
    
    dx = Measure("dx")
    dx = dx(degree=degree, scheme=scheme)
    
    V  = VectorFunctionSpace(mesh, "Lagrange", 2)
    element_t = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=36, quad_scheme=scheme)
    Vt = FunctionSpace(mesh, element_t)
    element_s = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
    Vs = FunctionSpace(mesh, element_s)
    
    T  = FunctionSpace(mesh, "Lagrange", 1)
        
    zero = Constant(0.0)
    
    bc0 = DirichletBC(V.sub(0), zero, DirichletBoundaryZ(), method="pointwise")
    bc1 = DirichletBC(V.sub(1), zero, DirichletBoundaryZ(), method="pointwise")
    bc2 = DirichletBC(V.sub(2), zero, DirichletBoundaryZ(), method="pointwise")
    
    
    bcs = [bc0, bc1, bc2]

    u = Function(V, name="u")

    # temperature defination and importing temperature solution
    t = Function(T, name="T")
    
    meshinfo.read(t,"/solution/{}_part".format(i))
        
    #u_to_plot = Function(V, name="u_to_plot")
    
    temp_ref = Function(T, name="T_ref")   # reference temperature
    temp_ref = Constant(reference_temperature) # assigning reference temperature
    
    def mech_eps(u):
        return as_vector([u[i].dx(i) for i in range(3)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1), (0, 2), (1, 2)]])

    def thermal_eps(t):
        #expansion_coeff = Constant(expansion_coeff_value)
        expansion_coeff = expansion_coefficient_coeff_a * t + expansion_coefficient_coeff_b  #  temperature dependent value     
        return expansion_coeff * as_vector([1., 1., 1., 0., 0., 0.]) * ( t - temp_ref )

    def eps(u, t):
        return mech_eps(u) - thermal_eps(t)
    
    Qdef = fsm.UFLQuadratureFunction(eps(u, t), element_s, mesh)
    
    # multi-material wrapper

    # description of marker based upon the temperature or temperature range
    
    materials = MeshFunction("size_t", mesh, mesh.topology().dim())
    materials.set_all(0)

    # assigning the markers 

    # writting the DG temperature field
    #ile_t = XDMFFile('fsm_output/t' + format(i, '04') + '.xdmf')
    #file_t.parameters['functions_share_mesh'] = True
    #file_t.parameters['rewrite_function_mesh'] = False
    #file_t.parameters["flush_output"] = True
    #file_t.write(t, t=float(i));

    T_dg = FunctionSpace(mesh, "DG", 0)
    t_dg = project(t, T_dg)   # to make averaging on the cell and to keep basically no. temp field entities = no. mesh cell, projecting the CG temperature field upon the DG 0 and each cell suppose to have mark based on own temperature value 

    # writting the DG temperature field
    #file_t_dg = XDMFFile('fsm_output/t_dg' + format(i, '04') + '.xdmf')
    #file_t_dg.parameters['functions_share_mesh'] = True
    #file_t_dg.parameters['rewrite_function_mesh'] = False
    #file_t_dg.parameters["flush_output"] = True
    #file_t_dg.write(t_dg, t=float(i));


    # lists to be assigned to multi-material wrapper
    fsm_tangent_list = [] 
    fsm_stress_list = []
    fsm_constitutive_update_list = []
    eps_p_eq_list = []

    if len(t_dg.vector()) != mesh.num_cells():
        raise ValueError("make sure that the number of entities for cell temperature field projected on DG0 is equal to number of cells in current mesh")
    
    print()
    print("Assigning material properties to each cell as per its averaged temperature")
    print()
    print("mesh.num_cells():",mesh.num_cells())
    for cell in cells(mesh):
        index = cell.index()
        ##print("working for index:",index)#for debug
        #print(index)
        #print(len(t_dg.vector()))
        #print(t_dg.vector()[index])
        #if t_dg.vector()[index] > 49.8:   # is the location of value of concerned cell temperature same as the location of t[cell_index]?
        ##print("assigning material label = index of cell")#for debug
        materials[index] = index          # giving label to the particular cell
        
        # temperature dependent material properties expression
        ##print("evaluating the property value based upon temperature value: ") #for debug
        nu = nu_coeff_a * t_dg.vector()[index] + nu_coeff_b #0.33 + 0.0001 * t_dg.vector()[index] #0.3 interpolation {{0,0.33},{500,0.38}} x-axis is temperature
        yield_stress = yield_coeff_a * t_dg.vector()[index] + yield_coeff_b #9 - 0.0162 * t_dg.vector()[index] #9   interpolation {{0,50000},{500,25000}} x-axis is temperature
        E = E_coeff_a * t_dg.vector()[index] + E_coeff_b #50000 - 50 * t_dg.vector()[index] #E = 20000   interpolation {{0,9},{500,0.9}} x-axis is temperature
        
        E_t = 0.3*E
        hardening_parameter = E_t/(1.0 - E_t/E) 
        
        ##print("making J2 ") #for debug
        # making the fsm components for each mesh cell or other words each temp cell region
        J2_1 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter) # .self ?
        ##print("making fsm_const_update: ") #for debug
        fsm_constitutive_update_1 = fsm.ConstitutiveUpdate(Qdef, J2_1)
        # updating the list for each cell based upon its DG averaged temperature value of material property 
        ##print("appending fsm_const update: ") #for debug
        fsm_constitutive_update_list.append(fsm_constitutive_update_1)
        ##print() #for debug
        
        #fsm_tangent_1 = fsm.QuadratureFunction(Vt, fsm_constitutive_update_1.w_tangent(), fsm_constitutive_update_1)
        #fsm_stress_1 = fsm.QuadratureFunction(Vs, fsm_constitutive_update_1.w_stress()) #, fsm_constitutive_update_1)?
        
        #print("appending eps_p_eq():", cell, mesh.num_cells())
        #eps_p_eq_list.append( fsm_constitutive_update_list[index].eps_p_eq() )
        #fsm_tangent_list.append(fsm_tangent_1)
        #fsm_stress_list.append(fsm_stress_1)
    fsm_constitutive_update_list = []
     

    del fsm_tangent_list
    del fsm_stress_list

    
    
    submesh_number = submesh_number + 1
    
    

meshinfo.close()
