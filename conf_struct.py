# material properties ( chosen around 800 deg C for temp range of 25 - 1644 deg C )

#THERMO-MECHANICAL MODEL DEVELOPMENT AND EXPERIMENTAL VALIDATION FOR METALLIC PARTS IN ADDITIVE MANUFACTURING
# temperature dep material prop expression polynomial coeff_a x Temperature + ceoff_b

#E = 65.25E9; 
#E_t = 0.3*E
#hardening_parameter = E_t/(1.0 - E_t/E)
#yield_stress = 438E6 

#ref: Process Modeling and Validation of Powder Bed Metal Additive Manufacturing {{0,0.33},{1000,0.42}}
nu_coeff_a = 0.00009
nu_coeff_b = 0.33

#ref: THERMO-MECHANICAL MODEL DEVELOPMENT AND EXPERIMENTAL VALIDATION FOR METALLIC PARTS IN ADDITIVE MANUFACTURING {{0,768e6},{1000,417e6}}
yield_coeff_a = -351000
yield_coeff_b = 768000000

#ref: Process Modeling and Validation of Powder Bed Metal Additive Manufacturing {{0,107000e6},{1000,16290e6}}
E_coeff_a = -90710000
E_coeff_b = 107000000000 

#ref: THERMO-MECHANICAL MODEL DEVELOPMENT AND EXPERIMENTAL VALIDATION FOR METALLIC PARTS IN ADDITIVE MANUFACTURING {{0, 8.60e-6},{1000, 9.70e-6}} 
expansion_coefficient_coeff_a = 1.1e-9
expansion_coefficient_coeff_b = 8.6e-6

#expansion_coeff_value = 10e-6#9.7E-6  

#Process Modeling and Validation of Powder Bed Metal Additive Manufacturing
#nu = 0.3#0.41;  

reference_temperature = 1000#100#1644.5946044921875#681.0755615234375
n_initial = 19#35 # initial geometry to read from G_code mesh

# 29.9mm, 13.9mm/m  all vary

# Flag for iterative solver if false then by default UMFPACK is used
iterative_solver = True
