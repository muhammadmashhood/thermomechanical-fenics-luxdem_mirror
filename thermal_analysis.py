"""
FEniCS tutorial demo program: Heat equation with Dirichlet conditions.
Test problem is chosen to give an exact solution at all nodes of the mesh.

  u'= Laplace(u) + f  in the unit square
  u = u_D             on the boundary
  u = u_0             at t = 0

"""
# a
from __future__ import print_function
from fenics import *
#from dolfin import *
import logging
import sys
set_log_level(logging.WARNING)
#set_log_level(10)
import numpy as np
#import matplotlib.pyplot as plt
import socket
print("Machine:", socket.gethostname())
print("2D-heat-DED version 1.0")
from G_code_functionality import conf_g_code as gcode_conf
#import G-code_functionality/conf_g_code as gcode_conf

# Build geometry
from conf_geom import *
from conf_proc import *


Td = xhi/velbeam      # time per layer, deposition only
Tl = Td + pause       # time per layer, adding pause

#num_steps = int(Tl/dt+.5)   # number of time steps

# reading the number of steps from file (same as the number of mesh files)
num_steps_data_row = np.genfromtxt(num_steps_file_path, delimiter = ',')
num_steps = int(num_steps_data_row[0])
#num_steps = 400

from conf_mat import *
sigm = 5.67e-08    # SB constant


yhi = yhi_init

class WholeDomain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[2], (0, 0.0005))
        
# initial temperature of first element
tinit_active = Constant(tinit_active_element)

# Define boundary condition
# Dirichlet
u_D = Constant(tinit)

# Neumann
upx = Constant(-10);upy = Constant(-10)

# Gaussian
s4 = Expression(' -alpha*pow3d/(2*pi*pow(beamrad,2)) * exp(-0.5 * pow( (x[0]-velbeam*t-lah)/beamrad , 2)) ',
                degree=2, pow3d=pow3d, beamrad=beamrad, velbeam=velbeam, lah=lah, t=0, alpha=alpha)


print("reading mesh from file", gcode_conf.arg1)
mesh_final = Mesh(gcode_conf.arg1)
#mesh_final = Mesh(sys.argv[1])
print("mesh read")
#mxf = XDMFFile("thermal_output/mesh.xdmf")
#mxf.write(mesh_final)
#mxf.close()

Vf = FunctionSpace(mesh_final, 'P', 1)
uf = Function(Vf)
u_f_array = uf.vector().get_local()
u_f_array[:] = tinit_active_element # Ti6Al4V melting point 
print("mesh read2")
'''
class CurrentDomainInitial(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[1], (0, yhi))

mark_cd = MeshFunction("size_t", mesh_final, mesh_final.topology().dim(), 0)
mark1 = CurrentDomainInitial()
mark1.mark(mark_cd, 1)
#mesh = SubMesh(mesh_final, mark_cd, 1)
print(type(mesh)) # <class 'dolfin.cpp.mesh.SubMesh'>
mesh = MeshView.create(mark_cd, 1)'''

# reading the mesh marking from the G-code mesh maker
mesh = Mesh()
meshinfo_from_g_code = HDF5File(mesh.mpi_comm(),'G_code_functionality/mesh_h5_from_G_code/G_code_layersgeom.h5', 'r')


# making the mesh function for marking and reading initial mesh mark for initial mesh

mark_cd = MeshFunction("size_t", mesh_final, mesh_final.topology().dim(), 0)
mark_cd.set_all(0)
meshinfo_from_g_code.read(mark_cd,"/meshfunction/{}_part".format(n_initial)) # cell marker being imported instead off making ourself, thats it rest is same    

#mark_domain = WholeDomain()
#mark_domain.mark(mark_cd, 1)

mesh = MeshView.create(mark_cd, 1)
print("mesh read3")

V = FunctionSpace(mesh, 'P', 1)

# Define initial value
#u_n = interpolate(u_D, V)
# temperature defination
#temp_exp = Expression(" 1000*x[2]*x[2]*t1 * 4e6",t1=0,degree=2)
#temp_exp = Expression(" 500 *t1",t1=0,degree=2)
#temp_exp.t1 = 1
#u_n = interpolate(temp_exp,V)
u_n = interpolate(tinit_active, V)
#u_n = project(u_D, V)

# Define variational problem
u = Function(V)
#u.interpolate(u_D)
u.interpolate(tinit_active)
#u.assign(interpolate(temp_exp,V))
v = TestFunction(V)
f = Constant(0)


tcd = tcd_coeff_a * u + tcd_coeff_b
print(type(tcd))
# Robin
# 1: left+right, 2: top
conv = convh#/tcd   # convection parameter (h/k)
s1 = Constant(tglov);s2 = Constant(tglov);r1 = conv ;r2 = conv

# radiation on Top
rad = eps*sigm#/tcd # radiation parameter (eps*sig/k)
s3 = Constant(tglov);r3 = rad

####################### write checkpoint and mesh info for mesh and temperature solution (h5) #######################
#TempuratureSolution = XDMFFile("tempProfile/temp.xdmf")
#TempuratureSolution.parameters["functions_share_mesh"] = False # since mesh is changing

meshinfo= HDF5File(mesh_final.mpi_comm(),'tempProfile/layersgeom_for_history_1500_cooling_steps.h5', 'w')  

print("mesh read4")



# List
boundary_conditions = {0: {'Robin':  (r1,s1)},
                       1: {'Radiation':  (r3,s3)},
                       2: {'Dirichlet':  u_D},
                       3: {'GaussRobRadiation': (r2,s2,r3,s3,s4)},
                       4: {'RRadiation':  (r1,s1,r3,s3)}}

# Define boundary subdomains
tol = 1e-14
class BoundaryX0(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0, tol)
class BoundaryX1(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], xhi, tol)
class BoundaryZ0(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[2], 0, tol)
class BoundaryZ1(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[2], 0.0001, tol)
'''class BoundaryZ1(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary''' 
class BoundaryY1(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], yhi, tol)
class BoundaryAl(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


def collect_bnd_int_and_f(mesh):
    tdiff = tdiff_coeff_a * u + tdiff_coeff_b
    # Mark boundaries
    boundary_markers = MeshFunction("size_t", mesh, mesh.topology().dim()-1, 9999)
    #bxa = BoundaryZ1(); bxa.mark(boundary_markers, 4)
    bx0 = BoundaryAl(); bx0.mark(boundary_markers, 4)
    #bx1 = BoundaryAl(); bx1.mark(boundary_markers, 2)# temporary stoping to verify convection first 
    by0 = BoundaryZ0(); by0.mark(boundary_markers, 2)
    #by1 = BoundaryAl(); by1.mark(boundary_markers, 3)

    # Collect Dirichlet conditions
    bcs = []
    for i in boundary_conditions:
        if 'Dirichlet' in boundary_conditions[i]:
            bc_cur = DirichletBC(V, boundary_conditions[i]['Dirichlet'],
                                 boundary_markers, i)
            bcs.append(bc_cur)

    # redefine ds to allow multiple Neumann BC regions
    ds = Measure('ds', domain = mesh, subdomain_data = boundary_markers)

    integrals_N = []
    for i in boundary_conditions:
        if 'Neumann' in boundary_conditions[i]:
            if boundary_conditions[i]['Neumann'] != 0:
                g = boundary_conditions[i]['Neumann']
                integrals_N.append(dt*tdiff*g*v*ds(i))

    integrals_R_a = []
    integrals_R_L = []
    for i in boundary_conditions:
        if 'Robin' in boundary_conditions[i]:
            if boundary_conditions[i]['Robin'] != 0:
                r, s = boundary_conditions[i]['Robin']
                integrals_R_a.append(dt*tdiff*r*u*v*ds(i))
                integrals_R_L.append(dt*tdiff*r*s*v*ds(i))
        if 'Radiation' in boundary_conditions[i]:
            if boundary_conditions[i]['Radiation'] != 0:
                r, s = boundary_conditions[i]['Radiation']
                integrals_R_a.append(dt*tdiff*r*pow(u,4)*v*ds(i))
                integrals_R_L.append(dt*tdiff*r*pow(s,4)*v*ds(i))
        if 'RRadiation' in boundary_conditions[i]:
            if boundary_conditions[i]['RRadiation'] != 0:
                print("so the i in RRadiation is:   ",i)
                r, s, rp, sp = boundary_conditions[i]['RRadiation']  # remember that the division with tcd or tdiff for convh and rad is done here to avoid the 
                integrals_R_a.append(dt*tdiff*(r/tdiff)*u*v*ds(i))   # Error in building the mapping (105, 21) :Index not found.
                integrals_R_L.append(dt*tdiff*(r/tdiff)*s*v*ds(i))   # Here only one RRadiation BC is assumed which is on the whole surface
                integrals_R_a.append(dt*tdiff*(rp/tdiff)*pow(u,4)*v*ds(i)) # So keep in mind there might need some modification when more than one RRadiation BCs are used 
                integrals_R_L.append(dt*tdiff*(rp/tdiff)*pow(sp,4)*v*ds(i))
        if 'GaussRobRadiation' in boundary_conditions[i]:
            if boundary_conditions[i]['GaussRobRadiation'] != 0:
                r, s, rp, sp, g = boundary_conditions[i]['GaussRobRadiation']
                integrals_N.append(dt*tdiff*g*v*ds(i))
                integrals_R_a.append(dt*tdiff*r*u*v*ds(i))
                integrals_R_L.append(dt*tdiff*r*s*v*ds(i))
                integrals_R_a.append(dt*tdiff*rp*pow(u,4)*v*ds(i))
                integrals_R_L.append(dt*tdiff*rp*pow(sp,4)*v*ds(i))

    # needs u
    #cpv = conditional(gt(abs(u-tm), tr), cp, cp + lat/tr * (1-(abs(u-tm)/tr)) )
    rho = rho_coeff_a * u + rho_coeff_b
    cp = cp_coeff_a * u + cp_coeff_b
    cpv = cp 
    cpvrho = cpv*rho

    F = cpvrho*u*v*dx + tdiff*dt*dot(grad(u), grad(v))*dx - cpvrho*(u_n + dt*f)*v*dx + sum(integrals_N)
    F += sum(integrals_R_a) - sum(integrals_R_L)
    return F, bcs

# Time-stepping
t = 0
istep = n_initial
ostep = 0
########################################################################
# First time step simulation
n=0
t += dt
u_D.t = t

s1.t = t; s2.t = t; s3.t = t; s4.t = t
print("step:", istep, t, "n:", n)

F, bcs = collect_bnd_int_and_f(mesh)

# Compute solution
solve(F == 0, u, bcs)
'''
J = derivative(F, u)
problem = NonlinearVariationalProblem(F, u, bcs, J)
solver = NonlinearVariationalSolver(problem)
solver.parameters["newton_solver"]["absolute_tolerance"]= 1E-8
solver.parameters["newton_solver"]["relative_tolerance"] = 1.0E-8
solver.parameters["newton_solver"]["krylov_solver"]["absolute_tolerance"]= 1E-5
solver.parameters["newton_solver"]["krylov_solver"]["relative_tolerance"] = 1E-5
solver = NonlinearVariationalSolver(problem)
solver.solve()
'''
if n == 0:
    # Solver parameters
    prm=parameters
    info(prm, True)
    
cmin=u.vector().get_local().min()
cmax=u.vector().get_local().max()
print('minmax', cmin, cmax, istep, t)

# check for negative temperature overshoot
if cmin < tinit:
    raise ValueError("cmin < tinit temperature overshoot in -ve direction")
if cmax > tinit_active_element:
    raise ValueError("cmin > tinit_active_element temperature overshoot in +ve direction")

# Update previous solution
u_n.assign(u)

# Plot solution
if (istep % oplot == 0):
    ostep = istep
    xdmff = XDMFFile("thermal_output/forttemp" + format(ostep, '04') + ".xdmf")
    u.rename("u","temp")
    xdmff.write(u)
    print("mesh read",n)
    xdmff.close()
    
    meshinfo.write(mesh,"/submesh/{}_part".format(ostep))
    meshinfo.write(mark_cd,"/meshfunction/{}_part".format(ostep)) # cell marker
    meshinfo.write(u, "/solution/{}_part".format(ostep))
    

########################################################################

# functional space and mesh from first time step 
Vp = V
meshp = mesh
u_np = u_n
up = u

#step_number_to_read = [143, 179]
#num_steps+200
for n in range(n_initial + 1, num_steps + steps_for_cooling):
#for n in range(n_initial + 1, 2):#num_steps): 
    #mesh = SubMesh(mesh_final, mark_cd, 1)
    mark_cd = MeshFunction("size_t", mesh_final, mesh_final.topology().dim(), 0)
    #if (n < 2):
    #    meshinfo_from_g_code.read(mark_cd,"/meshfunction/{}_part".format( step_number_to_read[n] ) )
    #else:
    #    meshinfo_from_g_code.read(mark_cd,"/meshfunction/{}_part".format( 179 )   )  # simulate cooling down too by keep on reading final mesh file     

    #meshinfo_from_g_code.read(mark_cd,"/meshfunction/{}_part".format( step_number_to_read ) )  # simulate cooling down too
    #meshinfo_from_g_code.read(mark_cd,"/meshfunction/{}_part".format( n ) ) # cell marker being imported instead off making ourself, thats it rest is same    
    
    if (n <= num_steps):
        meshinfo_from_g_code.read(mark_cd,"/meshfunction/{}_part".format( n ) ) # cell marker being imported instead off making ourself, thats it rest is same    
    
    elif (n > num_steps):
        meshinfo_from_g_code.read(mark_cd,"/meshfunction/{}_part".format( num_steps ) )
    '''if (n <= num_steps):
        meshinfo_from_g_code.read(mark_cd,"/meshfunction/{}_part".format( 0 ) ) # cell marker being imported instead off making ourself, thats it rest is same    
    
    elif (n > num_steps):
        meshinfo_from_g_code.read(mark_cd,"/meshfunction/{}_part".format( 0 ) )'''
    
    mesh = MeshView.create(mark_cd, 1)
    #mark1.mark(mark_cd, ostep+1)
    #mesh = SubMesh(mesh_final, mark_cd, ostep+1)

    V = FunctionSpace(mesh, 'P', 1)

    # Assign previous solution of parent domain to current domain
    u_n = Function(V)
    u_n_array = u_n.vector().get_local()  
    u_np_array = u_np.vector().get_local()  # previous step and parent domain's solution

    dofp_vp = np.array(dof_to_vertex_map(Vp), dtype=int)   # which dof is associated to which vertex in parent domain
    
    #vp_vf = meshp.data().array("parent_vertex_indices", 0)  # getting the vertex indices of parent domain
    
    #### Setup for mesh view
    vp_vf_lst = meshp.topology().mapping()[mesh_final.id()].vertex_map()
    #print(vp_vf.tolist())
    #print(vp_vf_lst)
    #print(type(vp_vf_lst))
    vp_vf = np.array(vp_vf_lst) # Meshview objects give vertex_map as list so converting to the numpy.ndarray
    #print(vp_vf.tolist())
    #print(type(vp_vf))
    ###
    vf_doff = np.array(vertex_to_dof_map(Vf), dtype=int) # which dof is associated to which vertex in full domain

    u_f_array[vf_doff[vp_vf[dofp_vp]]] = u_np_array  # assigning the previous solution of parent domain
                                                     # upon the full domain (which had initialy the initial temp only)        
                                                     # using the technique: parent vertix index to full domain vertex index
                                                     # to dof of this index vertex (start from inner most bracket)
    
    dofc_vc = np.array(dof_to_vertex_map(V), dtype=int) # which dof is associated to which vertex in current domain
    
    #vc_vf = mesh.data().array("parent_vertex_indices", 0) # getting the vertex indices of current domain 
    
    #### Setup for mesh view
    vc_vf_lst = mesh.topology().mapping()[mesh_final.id()].vertex_map()
    vc_vf = np.array(vc_vf_lst) # Meshview objects give vertex_map as list so converting to the numpy.ndarray
    ###
    
    vf_dof = np.array(vertex_to_dof_map(Vf), dtype=int) # which dof is associated to which vertex in full domain

    u_n_array = u_f_array[vf_dof[vc_vf[dofc_vc]]] # from the full domain projecting the solution now to current domain
    u_n.vector()[:]=u_n_array

    # Assign current solution of parent domain to current domain
    u = Function(V)
    u_array = u.vector().get_local()
    up_array = up.vector().get_local()  # the current solution from parent domain

    u_f_array[vf_doff[vp_vf[dofp_vp]]] = up_array

    u_array = u_f_array[vf_dof[vc_vf[dofc_vc]]]
    u.vector()[:]=u_array

    # Define variational problem
    v = TestFunction(V)
    f = Constant(0)

    # Update current time
    t += dt
    u_D.t = t

    s1.t = t; s2.t = t; s3.t = t; s4.t += dt
    s4.lah =  lah
 
    #istep += 1
    istep = n
    print("step:", istep, t, "n:", n)

    F, bcs = collect_bnd_int_and_f(mesh)
    print("done establishing the F and bcs")

    # Compute solution
    solve(F == 0, u, bcs)
    print("done solving")
    '''
    J = derivative(F, u)
    problem = NonlinearVariationalProblem(F, u, bcs, J)
    solver = NonlinearVariationalSolver(problem)
    solver.parameters["newton_solver"]["absolute_tolerance"]= 1E-8
    solver.parameters["newton_solver"]["relative_tolerance"] = 1.0E-8
    solver.parameters["newton_solver"]["krylov_solver"]["absolute_tolerance"]= 1E-5
    solver.parameters["newton_solver"]["krylov_solver"]["relative_tolerance"] = 1E-5
    solver = NonlinearVariationalSolver(problem)
    solver.solve()
    '''
    cmin=u.vector().get_local().min()
    cmax=u.vector().get_local().max()
    print('minmax', cmin, cmax, istep, t)
    
    # check for negative temperature overshoot
    if cmin < tinit:
        raise ValueError("cmin < tinit temperature overshoot in -ve direction")
    # check for negative temperature overshoot
    if cmax > tinit_active_element:
        raise ValueError("cmin > tinit_active_element temperature overshoot in +ve direction")
    
    
    # Update previous solution
    u_n.assign(u)
    
    # Plot solution
    if (istep % oplot == 0):
        ostep = istep
        xdmff = XDMFFile("thermal_output/forttemp" + format(ostep, '04') + ".xdmf")
        u.rename("u","temp")
        xdmff.write(u)
        xdmff.close()

        #TempuratureSolution.write_checkpoint(u,"temperature",float(ostep),append=True)

        meshinfo.write(mesh,"/submesh/{}_part".format(ostep))
        meshinfo.write(mark_cd,"/meshfunction/{}_part".format(ostep)) # cell marker
        meshinfo.write(u, "/solution/{}_part".format(ostep))


    Vp = V
    meshp = mesh
    u_np = u_n
    up = u

cmin=u.vector().get_local().min()
cmax=u.vector().get_local().max()
print('minmax after deposition and before cooling start', cmin, cmax, istep, t)

'''
## Next solver loop is for the cooling down simulation 
cooling_steps = 200
for cool in range(cooling_steps):
    istep += 1
    t += dt
    u_D.t = t

    s1.t = t; s2.t = t; s3.t = t; s4.t = t
    print("cooling step:", istep, t)

    F, bcs = collect_bnd_int_and_f(mesh)

    # Compute solution
    solve(F == 0, u, bcs)
    
    #J = derivative(F, u)
    #problem = NonlinearVariationalProblem(F, u, bcs, J)
    #solver = NonlinearVariationalSolver(problem)
    #solver.parameters["newton_solver"]["absolute_tolerance"]= 1E-8
    #solver.parameters["newton_solver"]["relative_tolerance"] = 1.0E-8
    #solver.parameters["newton_solver"]["krylov_solver"]["absolute_tolerance"]= 1E-5
    #solver.parameters["newton_solver"]["krylov_solver"]["relative_tolerance"] = 1E-5
    #solver = NonlinearVariationalSolver(problem)
    #solver.solve()
    
    cmin=u.vector().get_local().min()
    cmax=u.vector().get_local().max()
    print('minmax', cmin, cmax, istep, t)

    # check for negative temperature overshoot
    if cmin < tinit:
        raise ValueError("cmin < tinit temperature overshoot in -ve direction")
    if cmax > tinit_active_element:
        raise ValueError("cmin > tinit_active_element temperature overshoot in +ve direction")

    # Update previous solution
    u_n.assign(u)

    # Plot solution
    if (istep % oplot == 0):
        ostep = istep
        xdmff = XDMFFile("thermal_output/forttemp" + format(ostep, '04') + ".xdmf")
        u.rename("u","temp")
        xdmff.write(u)
        xdmff.close()
        
        meshinfo.write(mesh,"/submesh/{}_part".format(ostep))
        meshinfo.write(mark_cd,"/meshfunction/{}_part".format(ostep)) # cell marker
        meshinfo.write(u, "/solution/{}_part".format(ostep))
    
##########
'''

#TempuratureSolution.close()
meshinfo.close()
meshinfo_from_g_code.close()

ostep = istep
xdmff = XDMFFile("thermal_output/forttemp" + format(ostep, '04') + ".xdmf")
u.rename("u","temp")
xdmff.write(u)
xdmff.close()

