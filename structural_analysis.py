# modified to work with the docker image's fsm
from dolfin import *
import fsm
import sys
#set_log_level(10)
from G_code_functionality import conf_g_code as gcode_conf

################ things for transfer history function #########################
import types
import os
import dolfin.cpp as cpp
import conf_geom as config_geom
from dolfin import compile_cpp_code
#from dolfin import *
from fenics import *
from conf_struct import *
import numpy as np
# multi-material wrapper
from multimaterial_wrapper_for_AM import multi_material_wrapper as mmw

pwd = os.path.dirname(os.path.abspath(__file__))
with open(pwd + "/history_transfer/history_transfer.cpp", "r") as f:
    cpp_code_transfer_history = f.read()


path = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
module_cpp = compile_cpp_code(cpp_code_transfer_history, include_dirs=['/usr/local/lib/python3.6/dist-packages/fsm-0.0.2-py3.6.egg/']) 

def transferHistory(CU1, CU2, map1, map2):
   module_cpp.transferHistory(CU1, CU2, map1, map2)   # can it be called now as cpp_object etc.?

################################################################################

# BC defination

class DirichletBoundaryX(SubDomain):
    def inside(self, x, on_boundary):
        return (x[0] < DOLFIN_EPS)
class DirichletBoundaryY(SubDomain):
    def inside(self, x, on_boundary):
        return (x[1] < DOLFIN_EPS) 
class DirichletBoundaryZ(SubDomain):
    def inside(self, x, on_boundary):
        return (x[2] < DOLFIN_EPS)

class PrescribedDisplacementY(SubDomain):
    def inside(self, x, on_boundary):
        return between( x[1], (1-DOLFIN_EPS, 1) )

        


def solution_transfer_function(mesh_Complete, half_cube_mesh, quarter_cube_mesh, u_half_cube, u_quarter_cube):
    # Builds cells mapping between parent meshes
    half = half_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()
    quarter = quarter_cube_mesh.topology().mapping()[mesh_Complete.id()].cell_map()

    # Builds cells mapping betwen childs
    map = [j for i, c in enumerate(half) for j, d in enumerate(quarter) if c==d]
    
    # functional spaces (must be same as in the actual problem)
    V_half_cube = VectorFunctionSpace(half_cube_mesh, "Lagrange", 2)
    V_quarter_cube = VectorFunctionSpace(quarter_cube_mesh, "Lagrange", 2)
    
    # Get cell dofmaps
    half_dofmap = V_half_cube.dofmap()
    quarter_dofmap = V_quarter_cube.dofmap()

    # Assign values to functions for testint
    #u_half_cube.vector()[:] = np.random.rand(u_half_cube.vector()[:].size, )

    # Assing dofs to quarter
    for c in cells(half_cube_mesh):
      u_quarter_cube.vector()[quarter_dofmap.cell_dofs(map[c.index()])] = u_half_cube.vector()[half_dofmap.cell_dofs(c.index())]
    return u_quarter_cube
    
# map maker between two cells which tells that which cell of previous mesh belongs to which one of current mesh
def map_maker(current_map, previous_map):
    # Builds cells mapping betwen childs
    map = [j for i, c in enumerate(previous_map) for j, d in enumerate(current_map) if c==d]
    return map
    
meshComplete = Mesh(gcode_conf.arg1) # in future it can be read via mesh_list[len(mesh_list)]
                                # for input the mesh for thermal analysis and structural solver should be same
#meshComplete = Mesh(sys.argv[1]) # for multi material wrapper check
mesh = Mesh()

########################## Loading the mesh, meshfunction and temperature solution from layers geometry ################
meshinfo = HDF5File(mesh.mpi_comm(),'tempProfile/layersgeom_for_history.h5', 'r')
#meshinfo = HDF5File(MPI.comm_world,'tempProfile/layersgeom_for_history.h5', 'r')

########################## function which collects all mesh and marks data from thermal analysis ################

num_steps_data_row = np.genfromtxt(config_geom.num_steps_file_path, delimiter = ',')
numberoflayer = int(num_steps_data_row[0])
numberoflayer = numberoflayer + 1

# defining the arrays of previous solution record
u_old = []
mesh_old = []
fsm_constitutive_update_old = []
cell_map_old = []
submesh_number = 0
print("Number of layers: ",numberoflayer)
print()
# importing solution and solving for each layer or subdomain # i determines from which layer simulation should start
#for i in range(0, 3): # for multi material wrapper check
for i in range(n_initial, numberoflayer): 
    print ("simulating layer number:    ",i)
    print()
    # reading mesh, mesh marker and thermal solution associated
    
    #meshinfo.read(mesh_from_h5,"/submesh/{}_part".format(i),False)  #  because i starts from zero and 0 not present in submesh list 
    #meshinfo.read(mesh,"/submesh/{}_part".format(i),False)
    mark_cd = MeshFunction("size_t", meshComplete, meshComplete.topology().dim(),0)
    meshinfo.read(mark_cd,"/meshfunction/{}_part".format(i)) # cell marker being imported instead off making ourself, thats it rest is same 
    
    # writing back the read mesh in fsm to visualize if correct mesh being read
    #mxf = XDMFFile("tempProfile/fsm_read_mesh/"+format(i+1)+".xdmf")
    #mxf.write(mesh)
    #mxf.close()
    
    mesh = MeshView.create(mark_cd, 1)
    
    #print(type(mesh)) # <class 'dolfin.cpp.mesh.Mesh'>
    #print(type(meshComplete)) # <class 'dolfin.cpp.mesh.Mesh'>
    
    
    cell_map = mesh.topology().mapping()[meshComplete.id()].cell_map()
    
    #print (cell_map)
    #print (len(cell_map))
    
    scheme = "default"
    degree = 3
    
    dx = Measure("dx")
    dx = dx(degree=degree, scheme=scheme)
    
    V  = VectorFunctionSpace(mesh, "Lagrange", 2)
    element_t = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=36, quad_scheme=scheme)
    Vt = FunctionSpace(mesh, element_t)
    element_s = VectorElement("Quadrature", mesh.ufl_cell(), degree=3, dim=6, quad_scheme=scheme)
    Vs = FunctionSpace(mesh, element_s)
    
    T  = FunctionSpace(mesh, "Lagrange", 1)
        
    zero = Constant(0.0)
    
    
    
    bc0 = DirichletBC(V.sub(0), zero, DirichletBoundaryZ(), method="pointwise")
    bc1 = DirichletBC(V.sub(1), zero, DirichletBoundaryZ(), method="pointwise")
    bc2 = DirichletBC(V.sub(2), zero, DirichletBoundaryZ(), method="pointwise")
    
    
    bcs = [bc0, bc1, bc2]

    u = Function(V, name="u")

    # temperature defination and importing temperature solution
    t = Function(T, name="T")
    
    meshinfo.read(t,"/solution/{}_part".format(i))
    
    mxf = XDMFFile("fsm_output/t_read "+format(i)+".xdmf")
    mxf.write(t)
    mxf.close()
    
    #u_to_plot = Function(V, name="u_to_plot")
    
    temp_ref = Function(T, name="T_ref")   # reference temperature
    temp_ref = Constant(reference_temperature) # assigning reference temperature
    
    def mech_eps(u):
        return as_vector([u[i].dx(i) for i in range(3)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1), (0, 2), (1, 2)]])

    def thermal_eps(t):
        #expansion_coeff = Constant(expansion_coeff_value)
        expansion_coeff = expansion_coefficient_coeff_a * t + expansion_coefficient_coeff_b  #  temperature dependent value     
        return expansion_coeff * as_vector([1., 1., 1., 0., 0., 0.]) * ( t - temp_ref )

    def eps(u, t):
        return mech_eps(u) - thermal_eps(t)

    def sigma(s):
        #s = ss.function_space()
        return as_matrix([[s[0], s[3], s[4]], [s[3], s[1], s[5]], [s[4], s[5], s[2]]])

    def tangent(t):
        #t = tt.function_space()
        return as_matrix([[t[i*6 + j] for j in range(6)] for i in range(6)])
    
    Qdef = fsm.UFLQuadratureFunction(eps(u, t), element_s, mesh)
    
    # multi-material wrapper

    # description of marker based upon the temperature or temperature range
    
    materials = MeshFunction("size_t", mesh, mesh.topology().dim())
    materials.set_all(0)

    # assigning the markers 

    # writting the DG temperature field
    file_t = XDMFFile('fsm_output/t' + format(i, '04') + '.xdmf')
    file_t.parameters['functions_share_mesh'] = True
    file_t.parameters['rewrite_function_mesh'] = False
    file_t.parameters["flush_output"] = True
    file_t.write(t, t=float(i));

    T_dg = FunctionSpace(mesh, "DG", 0)
    t_dg = project(t, T_dg)   # to make averaging on the cell and to keep basically no. temp field entities = no. mesh cell, projecting the CG temperature field upon the DG 0 and each cell suppose to have mark based on own temperature value 

    # writting the DG temperature field
    file_t_dg = XDMFFile('fsm_output/t_dg' + format(i, '04') + '.xdmf')
    file_t_dg.parameters['functions_share_mesh'] = True
    file_t_dg.parameters['rewrite_function_mesh'] = False
    file_t_dg.parameters["flush_output"] = True
    file_t_dg.write(t_dg, t=float(i));


    # lists to be assigned to multi-material wrapper
    fsm_tangent_list = [] 
    fsm_stress_list = []
    fsm_constitutive_update_list = []
    eps_p_eq_list = []

    if len(t_dg.vector()) != mesh.num_cells():
        raise ValueError("make sure that the number of entities for cell temperature field projected on DG0 is equal to number of cells in current mesh")
    
    print()
    print("Assigning material properties to each cell as per its averaged temperature")
    print()
    
    for cell in cells(mesh):
        index = cell.index()
        #print(index)
        #print(len(t_dg.vector()))
        #print(t_dg.vector()[index])
        #if t_dg.vector()[index] > 49.8:   # is the location of value of concerned cell temperature same as the location of t[cell_index]?
        materials[index] = index          # giving label to the particular cell
        
        # temperature dependent material properties expression
        nu = nu_coeff_a * t_dg.vector()[index] + nu_coeff_b #0.33 + 0.0001 * t_dg.vector()[index] #0.3 interpolation {{0,0.33},{500,0.38}} x-axis is temperature
        yield_stress = yield_coeff_a * t_dg.vector()[index] + yield_coeff_b #9 - 0.0162 * t_dg.vector()[index] #9   interpolation {{0,50000},{500,25000}} x-axis is temperature
        E = E_coeff_a * t_dg.vector()[index] + E_coeff_b #50000 - 50 * t_dg.vector()[index] #E = 20000   interpolation {{0,9},{500,0.9}} x-axis is temperature
        
        E_t = 0.3*E
        hardening_parameter = E_t/(1.0 - E_t/E) 
        
        # making the fsm components for each mesh cell or other words each temp cell region
        J2_1 = fsm.python.cpp.plasticity_model.VonMises(E, nu, yield_stress, hardening_parameter) # .self ?
        fsm_constitutive_update_1 = fsm.ConstitutiveUpdate(Qdef, J2_1)
        # updating the list for each cell based upon its DG averaged temperature value of material property 
        fsm_constitutive_update_list.append(fsm_constitutive_update_1)
 
     
    if (submesh_number > 0):  # because first subdomain gets everything from the start as default
        print("Transferring the load history from previous load step")
        print()
        #u = solution_transfer_function(meshComplete, mesh_old, mesh, u_old, u)
         
        '''u_to_plot = u
        # Write output to files from last submesh
        xdmff = XDMFFile("u_to_plot/u_to_plot" + format(submesh_number, '04') + ".xdmf")    
        xdmff.write(u_to_plot);
        xdmff.close()'''
        
        '''
        ################################## plotting the previous CU's eps_eq ##########################
        
        eps_p_eq_old = fsm_constitutive_update_old.eps_p_eq()
        print(eps_p_eq_old)
        element_eps_p_eq_project_old = FiniteElement("CG", mesh_old.ufl_cell(), degree=1)
        V_eps_p_eq_project_old = FunctionSpace(mesh_old, element_eps_p_eq_project_old)
        eps_p_eq_project_old = Function(V_eps_p_eq_project_old, name="eps_p_eq_old")
        
        assign( eps_p_eq_project_old, project(eps_p_eq_old, V_eps_p_eq_project_old, solver_type='umfpack',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )
        file1_eqv_eps_p_old = XDMFFile("fsm_output/output_eqv_eps_p_old" + format(submesh_number-1, '04') + ".xdmf")
        file1_eqv_eps_p_old.write(eps_p_eq_project_old, t=float(submesh_number-1));
        file1_eqv_eps_p_old.close()
        
        ###############################################################################################
        '''
        mapping = map_maker(cell_map, cell_map_old) # getting the mapper to identify which CU of current mesh's cell belongs to which one of the previous cell to transfer CU1 to correct or in other words between same cell or location CU2
        #print(mesh_old.num_cells())
        
        #for cell in cells(mesh_old): # change to old mesh cell numbers time loop
        for l in range(len(mapping)):
            cell_index1 = l
            cell_index2 = mapping[l]
            #print(cell.label())
            #if index < mesh_old.num_cells() :
            transferHistory(fsm_constitutive_update_old[cell_index1].cpp_object(),
                            fsm_constitutive_update_list[cell_index2].cpp_object(),
                            cell_map_old,
                            cell_map)
            '''else:
            transferHistory(fsm_constitutive_update_list[index].cpp_object(),
                            fsm_constitutive_update_list[index].cpp_object(),
                            cell_map_old,
                            cell_map)'''
            
            fsm_constitutive_update_list[cell_index2].update_history(); # so that all currently updated one become the old variables
        
        #print(mapping)
        #if (submesh_number>0):
        '''transferHistory(fsm_constitutive_update.cpp_object(),
                            fsm_constitutive_update.cpp_object(),
                            cell_map,
                            cell_map)'''
        
        
        '''cell_map_old_exp = [11, 13, 16]
        cell_map_exp = [11, 22, 36, 16, 32, 13]
        transferHistory(fsm_constitutive_update_old.cpp_object(),
                        fsm_constitutive_update.cpp_object(),
                        cell_map_old_exp,
                        cell_map_exp)'''
        '''
        ################################## plotting the transformed eps_eq on current CU ################
        
        #import pdb
        #pdb.set_trace()
        eps_p_eq_transferred = fsm_constitutive_update.eps_p_eq()
        
        element_eps_p_eq_project_transferred = FiniteElement("CG", mesh.ufl_cell(), degree=1)
        V_eps_p_eq_project_transferred = FunctionSpace(mesh, element_eps_p_eq_project_transferred)
        eps_p_eq_project_transferred = Function(V_eps_p_eq_project_transferred, name="eps_p_eq_transferred")
        
        
        
        assign( eps_p_eq_project_transferred, project(eps_p_eq_transferred, V_eps_p_eq_project_transferred, solver_type='umfpack',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )
        
        
        file1_eqv_eps_p_transferred = XDMFFile("fsm_output/output_eqv_eps_p_transferred" + format(submesh_number, '04') + ".xdmf")
        file1_eqv_eps_p_transferred.write(eps_p_eq_project_transferred, t=float(submesh_number));
        file1_eqv_eps_p_transferred.close()
        
        ###############################################################################################
        '''
        del fsm_constitutive_update_old
    print("Forming the tangent and stress matrices for each cell as per its material properties")
    print()
    for cell in cells(mesh): 
        index = cell.index()
        fsm_tangent_1 = fsm.QuadratureFunction(Vt, fsm_constitutive_update_list[index].w_tangent(), fsm_constitutive_update_list[index])
        fsm_stress_1 = fsm.QuadratureFunction(Vs, fsm_constitutive_update_list[index].w_stress()) #, fsm_constitutive_update_1)?
        
        #print("appending eps_p_eq():", cell, mesh.num_cells())
        #eps_p_eq_list.append( fsm_constitutive_update_list[index].eps_p_eq() )
        fsm_tangent_list.append(fsm_tangent_1)
        fsm_stress_list.append(fsm_stress_1)

    # writing the markers
    material_marks_file = File("fsm_output/material_marks" + format(i,'04') + ".pvd")
    material_marks_file << materials
    
    fsm_tangent = mmw.MultiMaterialWrapper(fsm_tangent_list, materials)
    fsm_stress = mmw.MultiMaterialWrapper(fsm_stress_list, materials)
    
    #eps_p_eq = mmw.MultiMaterialWrapper(eps_p_eq_list, materials)
    #fsm_constitutive_update.eps_p_eq().compute_mean(eps_eq);

    v = TestFunction(V)
    uTrial = TrialFunction(V)

    a = inner(mech_eps(v), dot(tangent(fsm_tangent), mech_eps(uTrial)) )*dx#(1) + (1e-90)*inner(eps(v), dot(tangent(fsm_tangent), eps(uTrial)) )*dx(0)
    L = inner(grad(v), sigma(fsm_stress))*dx#(1) + (1e-90)*inner(grad(v), sigma(fsm_stress))*dx(0)
    
    # a and L correspond to the LHS and RHS respectively as mentioned in the book 
    #"Automated Solution of Differential Equations by the Finite Element Method" eq 26.51 and eq 26.52
    #where this variational form equation is linearized for Newton solver which processes global Newton iterations
    #in which for each global Newton iteration the RMA is solved with its own Newton algorithm to given
    # sigma(n+1), eps_p(n+1) and the tangent matrix C(n+1)
    #the non linear problem is formed below using this linearized variational form, bcs, fsm_tangent which 
    # physically speaking is the stiffness matrix for each global Newton iteration and the 
    # fsm_stress which actually is the analogical to internal forces of material to be balanced 
    # by external load in global Newton iterations 
    # PlasticityProblem class handles the assembly over cells and looping over quadrature points 
    # and variable update etc.     
    nonlinear_problem = fsm.PlasticityProblem(a, L, u, fsm_tangent_list[0], fsm_stress_list[0], bcs)
    
    nonlinear_solver = NewtonSolver()
    nonlinear_solver.parameters["convergence_criterion"] = "incremental";
    nonlinear_solver.parameters["maximum_iterations"]    = 50;
    nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
    nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;
    # linear solver parameters if iterative chosen
    if iterative_solver:
        nonlinear_solver.parameters["linear_solver"] = 'cg'
        nonlinear_solver.parameters["preconditioner"] = 'ilu'
        #nonlinear_solver.parameters['krylov_solver']["absolute_tolerance"] = 1e-15
        #nonlinear_solver.parameters['krylov_solver']["relative_tolerance"] = 1e-7
        #nonlinear_solver.parameters['krylov_solver']["maximum_iterations"] = 1000

    # File names for output
    
    


    element_eps_p_eq_project = FiniteElement("DG", mesh.ufl_cell(), degree=1)
    V_eps_p_eq_project = FunctionSpace(mesh, element_eps_p_eq_project)
    eps_p_eq_project = Function(V_eps_p_eq_project, name="eps_p_eq")
    
    
    #################################################################################
    # try to check if eps_eq is still same as that of the transferred from last state
    eps_p_eq_project.vector()[:] = 0 # initially zero before filling eps_p_eq of each cell
    eps_p_eq_project_old_np_array = np.zeros(len(eps_p_eq_project.vector()[:])) # also zero initially
    #print(eps_p_eq_project_old.vector()[:])
    #print(len(eps_p_eq_project_old.vector()[:]))
    #print()
    print("Writing the equivalent plastic strain of previous domain to current one in cell by cell manner")
    print()
    # Write output to files
    '''for l in range( mesh.num_cells()): #- (mesh.num_cells()-1) ):
        eps_p_eq = fsm_constitutive_update_list[l].eps_p_eq()
        eps_p_eq_project.vector()[:] = project(eps_p_eq, V_eps_p_eq_project, solver_type='umfpack',
                                            form_compiler_parameters={
                                                "representation": parameters["form_compiler"]["representation"],
                                                "quadrature_scheme": scheme,
                                                "quadrature_degree": degree
                                            }
                                        ).vector()
        eps_p_eq_project.vector()[:] = np.add(eps_p_eq_project.vector()[:], eps_p_eq_project_old_np_array)
        eps_p_eq_project_old_np_array = eps_p_eq_project.vector()[:] # update for next iteration
    del eps_p_eq_project_old_np_array'''
    
    '''assign( eps_p_eq_project, project(eps_p_eq, V_eps_p_eq_project, solver_type='umfpack',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )'''
    file1_eqv_eps_p = XDMFFile("fsm_output/output_eqv_eps_p_check" + format(i, '04') + ".xdmf")
    file1_eqv_eps_p.write(eps_p_eq_project, t=float(i));
    file1_eqv_eps_p.close()
    
    '''
    # using probe tool
    #from fenicstools.Probe import *
    print("Probe result for submesh number :    ",i+1)
    if(submesh_number < 2):
        x = array([[0.5, 0, 0.5], [0.5, 0.5, 0.5], [0.5, 1, 0.5]])  # coordinates of the 3 points of probe location
    else:
        x = array([[0.75, 0, 0.5], [0.75, 0.5, 0.5], [0.75, 1, 0.5]])  # coordinates of the 3 points of probe location
    p = Probes(x.flatten(), V_eps_p_eq_project)   # here the probe location or positions are given to the probes constructor
    p(eps_p_eq_project)
    print(p.array())
    print ( p.dump("testarray") )
    print("####################################################")
	'''
    '''
    # using plot tool
    plot(mesh)
    if(submesh_number > 1):
        plot(eps_p_eq_project)
    import matplotlib.pyplot as plt
    plt.savefig("figure.png")
    plt.show()'''
    
    
    #################################################################################
    
    #################################################################################
    # try to check if eps_eq is still same as that of the transferred from last state
    u_to_check = u
    # Write output to files from last submesh
    xdmff = XDMFFile("fsm_output/u_to_check" + format(i, '04') + ".xdmf")    
    xdmff.write(u_to_check);
    xdmff.close()
    #################################################################################
    
    
    # Solve non-linear problem
    nonlinear_solver.solve(nonlinear_problem.cpp_object(), u.vector());
    
    #pdb.set_trace()
    
    # Update variables
    # As the update of fsm_constitutive_update makes all current plastified quadrature points, eps_p_eqv and eps_p as the old one for next load step so it is necessary to update it for each constitutive update for correct and realist next loading or inloading step
    
    for k in range(len(fsm_constitutive_update_list)):
    #for k in range(mesh.num_cells()):
        fsm_constitutive_update_list[k].update_history();
    
    # deleting the variables to possibly free up memory
    del fsm_tangent_list
    del fsm_stress_list
    
    #transferHistory(fsm_constitutive_update.cpp_object(),
    #                    fsm_constitutive_update.cpp_object(),
    #                    cell_map,
    #                    cell_map)

    #fsm_constitutive_update.eps_p_eq().get_old_values();
    #print(fsm_constitutive_update.eps_p_eq().old_data())

    # Write output to files from last submesh 
    
    file1_u = XDMFFile("fsm_output/output_u" + format(i, '04') + ".xdmf")
    
    '''file1.parameters['functions_share_mesh'] = False
    file1.parameters['rewrite_function_mesh'] = False
    file1.parameters["flush_output"] = False'''
    
    #u_array = u.vector()
    #print (u_array.size())
    
    file1_u.write(u, t=float(i));
    file1_u.close()
    
    file2_t = XDMFFile("fsm_output/output_t" + format(i, '04') + ".xdmf")
    file2_t.write(t, t=float(i));
    file2_t.close()
    
    eps_p_eq_project.vector()[:] = 0 # initially zero before filling eps_p_eq of each cell
    eps_p_eq_project_old_np_array = np.zeros(len(eps_p_eq_project.vector()[:])) # also zero initially
    #print(eps_p_eq_project_old.vector()[:])
    #print(len(eps_p_eq_project_old.vector()[:]))
    
    print()
    print("Writing the equivalent plastic strain in cell by cell manner")
    print()
    
    # Write output to files
    '''for l in range( mesh.num_cells()): #- (mesh.num_cells()-1) ):
        eps_p_eq = fsm_constitutive_update_list[l].eps_p_eq()
        eps_p_eq_project.vector()[:] = project(eps_p_eq, V_eps_p_eq_project, solver_type='umfpack',
                                            form_compiler_parameters={
                                                "representation": parameters["form_compiler"]["representation"],
                                                "quadrature_scheme": scheme,
                                                "quadrature_degree": degree
                                            }
                                        ).vector()
        eps_p_eq_project.vector()[:] = np.add(eps_p_eq_project.vector()[:], eps_p_eq_project_old_np_array)
        eps_p_eq_project_old_np_array = eps_p_eq_project.vector()[:] # update for next iteration
    del eps_p_eq_project_old_np_array'''
    
    '''assign( eps_p_eq_project, project(eps_p_eq, V_eps_p_eq_project, solver_type='umfpack',
                                        form_compiler_parameters={
                                            "representation": parameters["form_compiler"]["representation"],
                                            "quadrature_scheme": scheme,
                                            "quadrature_degree": degree
                                        }
                                    ) )'''
    
    file1_eqv_eps_p = XDMFFile("fsm_output/output_eqv_eps_p" + format(i, '04') + ".xdmf")
    file1_eqv_eps_p.write(eps_p_eq_project, t=float(i));
    file1_eqv_eps_p.close()

    #previous subdomain solution and variables:
    print()
    print("Replacing the old variable values of mesh, displacement, constitutive update and cell map with current values")
    print()
    u_old = u
    mesh_old = mesh
    fsm_constitutive_update_old = fsm_constitutive_update_list
    cell_map_old = cell_map
    
     
    # deleting the variables to possibly free up memory
    del fsm_constitutive_update_list
    
    
    submesh_number = submesh_number + 1
    
    

meshinfo.close()
