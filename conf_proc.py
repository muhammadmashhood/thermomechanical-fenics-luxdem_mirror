
tinit = 0#1e-12          # bottom Dirichlet temperature
tinit_active_element = 1000#1000  # Initial temperature of element being activated
tglov = 0#400#303          # Glove comp. temp for Robin

steps_for_cooling = 1000 # steps to cool down after completing geometry
nhepa = 1
beamrad = 0.000025#0.00025


pow3d = 0#1000

alpha = 0.0028826225158256505
lah = -2.0*beamrad
velbeam = 2#50*0.016933333

pause = 0.0

